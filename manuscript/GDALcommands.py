'''
Commandline commands for getting figures and zonal stat results for 2010 2015 continental products
Update: Dan wrote some to do this much better
https://bitbucket.org/au-eoed/livingearth_australia/src/master/post-processing/
'''

# extract out first band (level 3 classes) from data file (i.e. lccs_2010_L4_v-1.0.0.tif)
gdal_translate -b 1 -of GTiff -co "COMPRESS=LZW" -co "TILED=YES" -co "BIGTIFF=YES" lccs_2010_L4_v-1.0.0_250m.tif lccs_2010_L4_v-1.0.0_250m_b1.tif

# clip to extent of Aust mainland and islands (basically get rid of ocean)
gdalwarp -cutline aust_mainland_islands_dissolve.gpkg -crop_to_cutline -dstalpha lccs_2010_L4_v-1.0.0_250m_b1.tif lccs_2010_L4_v-1.0.0_250m_b1_clip.tif

# get zonal statistics for level 3 classes using the QGIS function
# 'Raster layer unique values report' and output as html
