# script to get random stratified sampling points ready for accuracy assessment
# initial points generated using AcATaMa plugin in QGIS

import os, shutil, ogr

from rsgislib import vectorutils
from rsgislib import zonalstats

import pandas as pd
import geopandas as gpd

# temp files directory (deleted at the end)
os.makedirs('./temp', exist_ok=True)
alberstile = '9_-40'
# extract LCCS pixel values to points
inputimage = './2012_'+str(alberstile)+'.tif'
inputvector = './'+str(alberstile)+'_stratified_random_sampling.shp'
outputvector = './temp/extracttopoints_zonalstats.shp'
removeExistingVector = False
useBandNames = True
zonalstats.pointValue2SHP(inputimage, inputvector, outputvector, removeExistingVector, useBandNames)

# get required level3 column as list
vectorFile = './temp/extracttopoints_zonalstats.shp'
vectorLayer = 'extracttopoints_zonalstats'
colName = 'level3'
level3 = vectorutils.readVecColumn(vectorFile, vectorLayer, colName)

# write classify column to vector file as datatype string
vectorFile = './temp/extracttopoints_zonalstats.shp'
vectorLayer = 'extracttopoints_zonalstats'
colName = 'classified'
colData = level3
vectorutils.writeVecColumn(vectorFile, vectorLayer, colName, ogr.OFTString, colData)

# add column to vector file as datatype string and copy level3 data
vectorFile = './temp/extracttopoints_zonalstats.shp'
vectorLayer = 'extracttopoints_zonalstats'
colName = 'output'
colData = level3
vectorutils.writeVecColumn(vectorFile, vectorLayer, colName, ogr.OFTString, colData)

# add column to vector file as datatype string and write as all zeros
vectorFile = './temp/extracttopoints_zonalstats.shp'
vectorLayer = 'extracttopoints_zonalstats'
colName = 'processed'
colData = [i * 0 for i in level3]
vectorutils.writeVecColumn(vectorFile, vectorLayer, colName, ogr.OFTString, colData)

# open with geopandas into dataframe
df = gpd.read_file('./temp/extracttopoints_zonalstats.shp')

Class_0 = df[(df['level3'] == 0)]
Class_111 = df[(df['level3'] == 111)]
Class_112 = df[(df['level3'] == 112)]
Class_123 = df[(df['level3'] == 123)]
Class_124 = df[(df['level3'] == 124)]
Class_215 = df[(df['level3'] == 215)]
Class_216 = df[(df['level3'] == 216)]
Class_220 = df[(df['level3'] == 220)]

# reorder and export
reorder = gpd.GeoDataFrame(pd.concat([Class_0, Class_111, Class_112, Class_123, Class_124, Class_215, Class_216, Class_220]))
reorder.crs = {'init': 'epsg:3577'}
reorder.to_file(str(alberstile)+'_accuracy_assessment.shp')

# remove temp vector files
shutil.rmtree("./temp")
