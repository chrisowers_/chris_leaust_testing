'''
geomedians for LCCS accuracy assessment

script exports geomedians as geotiff for relevant year
based on input yaml with extents

- user should specify year for geomedian, default is 2015
- only loads rgb for geomedian, comment out below if all bands required
- can scale the data (values 0-1) if preferred, default is not to (values 0-10000)
'''

import os
import datacube
import yaml
from datacube.storage import masking
from datacube.helpers import write_geotiff
from odc.algo import to_f32

def get_geomedian_geotiff(site, year):
    """
    Function to run the load and export geomedian as geotiff.
    Gets site bounds from yaml file
    
    """
    # Read in config file with site bounds
    with open("au_validation_sites.yaml", "r") as f:
        config = yaml.safe_load(f)
    # Get bounds
    x = (config[site]["min_x"],config[site]["max_x"])
    y = (config[site]["max_y"],config[site]["min_y"])
    
    # set up datacube query
    query = {'time': ('{}-01-01'.format(year), '{}-12-31'.format(year))}
    query['x'] = x
    query['y'] = y
    query['crs'] = 'EPSG:3577'
    query['resolution'] = (-25, 25)
    query['measurements'] = ['red', 'green', 'blue']

    dc = datacube.Datacube(app = 'geomedian')
    # pick sensor based on input year
    if year <= 2011:
        geomedian_data = dc.load(product='ls5_nbart_geomedian_annual', dask_chunks={'x' : 1000, 'y' : 1000}, **query)
    elif year == 2012:
        geomedian_data = dc.load(product='ls7_nbart_geomedian_annual', dask_chunks={'x' : 1000, 'y' : 1000}, **query)
    else:
        geomedian_data = dc.load(product='ls8_nbart_geomedian_annual', dask_chunks={'x' : 1000, 'y' : 1000}, **query)
    geomedian_masked = masking.mask_invalid_data(geomedian_data).squeeze().drop('time')
    
    if scaled == True:
        sr_max_value = 10000                   # maximum SR value in the loaded product
        scale, offset = (1 / sr_max_value, 0)  # differs per product, aim for 0-1 values in float32
        geomedian_scaled = to_f32(geomedian_masked, scale=scale, offset=offset)
        # write out geotiff of scaled data (0-1)
        write_geotiff(str(directory)+'/'+str(year)+'_'+str(site)+'_geomedian.tif', geomedian_scaled)    
    
    else:
        # write out geotiff of unscaled data (0-10000)
        write_geotiff(str(directory)+'/'+str(year)+'_'+str(site)+'_geomedian.tif', geomedian_masked)
        print('exported '+str(year)+'_'+str(site)+'_geomedian.tif')

# specify year
year = 2015

# scale data to 0-1 (slightly larger file size, but potentially more stability in actual returned values)
# default is not to scale (False)
scaled = False

directory = './'+str(year)+'_geomedians'
os.makedirs(directory, exist_ok=True)
# Read in config file with site bounds
with open("au_validation_sites.yaml", "r") as f:
    config = yaml.safe_load(f)
# Loop over all sites in yaml
for site in config:
    get_geomedian_geotiff(site, year)

