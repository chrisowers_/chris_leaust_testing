# script to get random stratified sampling points ready for accuracy assessment
# initial points generated using AcATaMa plugin in QGIS

import os, shutil, ogr

import pandas as pd
import geopandas as gpd

# merge using geopandas
tile1 = gpd.read_file('./8_-20_accuracy_assessment.shp')
tile2 = gpd.read_file('./8_-28_accuracy_assessment.shp')
tile3 = gpd.read_file('./8_-29_accuracy_assessment.shp')
tile4 = gpd.read_file('./9_-40_accuracy_assessment.shp')
tile5 = gpd.read_file('./9_-42_accuracy_assessment.shp')


# reorder
reorder = gpd.GeoDataFrame(pd.concat([tile1, tile2, tile3, tile4, tile5]))
reorder.crs = {'init': 'epsg:3577'}
reorder.to_file('2012_accuracy_assessment.shp')

