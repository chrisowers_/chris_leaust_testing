'''


Script is intended to be called by kmeans_artsurface_workflow.py
Takes tif images with identical extents and runs binned histogram (i.e. zonal statistics like) based on AOI artsurfacecover
Examples would include indices derived from DEA (i.e. NDBI, MADs)
Generates a pdf figure with model showing relationship

Chris Owers, Pete Bunting June 2019
Aberystwyth University

'''

import pandas as pd
import numpy as np
import georasters as gr
import matplotlib.pyplot as plt
import random

def histogram(AOI, SpectralClasses, InputImageName, artsurface, variable):

    ########## Read data into Pandas dataframe ############
    artsurface = gr.from_file(artsurface)
    variable = gr.from_file(variable)

    # Convert to Pandas DataFrame
    artsurface_data = artsurface.to_pandas()
    variable_data = variable.to_pandas()

    # Merge datasets on coordinates
    dataValsFrame = pd.merge(variable_data, artsurface_data, on=['x', 'y'])


    ########## Split data to train and validate ############
    ids = dataValsFrame.index.values.astype(int)
    random.shuffle(ids)

    n = int(len(ids)/2)
    ids1, ids2 = ids[:n], ids[n:]

    dfTrain = dataValsFrame[dataValsFrame.index.isin(ids1)]
    print('Train: ', dfTrain.shape)
    dfValid = dataValsFrame[dataValsFrame.index.isin(ids2)]
    print('Validate: ', dfValid.shape)


    ########## Bin data to generate relationship ############

    # value_x is first dataset input into dataValsFrame (i.e. artsurface_data)
    aggCCDF = dfTrain.groupby('value_x').agg({'value_y': ['mean','std']})
    xVals = aggCCDF.index.values
    yVals = aggCCDF['value_y']['mean'].values
    errVals = aggCCDF['value_y']['std'].values
    errVals[np.isnan(errVals)] = 0
    print('Mean Bin Std Dev: ', np.mean(errVals))


    ########## Plot result ############
    ax = plt.subplot(111)
    ax.scatter(xVals, yVals, color='#808080', zorder=10)
    ax.errorbar(xVals,yVals,yerr=errVals, color='#E0E0E0', linestyle="None", zorder=1)
    # ax.set_title('Kmeans classes vs Binned artificial surface cover (%)')
    ax.set_xlabel(AOI+' '+str(InputImageName)+' '+'kmeans classes')
    ax.set_ylabel('Artificial surface cover (%)')
    ax.set_ylim(0,100)
    plt.savefig('./'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_ASC_v_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'.pdf')

