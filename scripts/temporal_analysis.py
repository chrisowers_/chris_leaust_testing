import sys
import datacube
from datacube.storage import masking
from datacube.helpers import write_geotiff
from datacube.utils.geometry import CRS
dc = datacube.Datacube(app="temporal analysis")
sys.path.append('/g/data/r78/LCCS_Aberystwyth/co6850/dea-notebooks/10_Scripts/')
import DEADataHandling

# AOI = 'Coorong'
# x = (600000, 700000)
# y = (-3950000, -3850000)

# AOI = 'Hobart'
# x = (1200000, 1300000)
# y = (-4800000, -4700000)

AOI = 'Perth'
x = (-1550000, -1450000)
y = (-3650000, -3550000)

res = (-25, 25)
time = ('2015-01-01', '2015-12-31')
crs = 'EPSG:3577'

query = {'x': x,
         'y': y,
         'resolution': res,
         'time': time,
         'crs': crs}  
mask_dict = {'cloud_acca': 'no_cloud',
             'cloud_fmask': 'no_cloud',
             'contiguous': True}

##### NDBI #####
# ds = DEADataHandling.load_clearlandsat(dc=dc, query=query, 
#                                        sensors=['ls5','ls7','ls8'],
#                                        bands_of_interest=['nir', 'swir1'],
#                                        masked_prop=0.1,
#                                        mask_dict=mask_dict)

# NDBI = ((ds.swir1 - ds.nir)/(ds.swir1 + ds.nir))
 
# NDBI_median = NDBI.groupby('time.year').median(dim = 'time').squeeze().drop('year')
# NDBI_median = NDBI_median.to_dataset(name='array')
# NDBI_median.attrs['crs'] = CRS('EPSG:3577')
# write_geotiff(filename='./Perth_NDBI_median_2015.tif', dataset=NDBI_median)
# print(str(AOI)+'_NDBI_median_2015.tif')

# NDBI_stddev = NDBI.groupby('time.year').std(dim = 'time').squeeze().drop('year')
# NDBI_stddev = NDBI_stddev.to_dataset(name='array')
# NDBI_stddev.attrs['crs'] = CRS('EPSG:3577')
# write_geotiff(filename=str(AOI)+'_NDBI_stddev_2015.tif', dataset=NDBI_stddev)
# print(str(AOI)+'_NDBI_stddev_2015.tif')


##### FC - BS #####
ds = DEADataHandling.load_clearlandsat(dc=dc, query=query, 
                                       sensors=['ls5','ls7','ls8'],
                                       product = 'fc',
                                       masked_prop=0.1,
                                       mask_dict=mask_dict)

# BS_stddev = ds['BS'].groupby('time.year').std(dim = 'time').squeeze().drop('year')
# BS_stddev = BS_stddev.to_dataset(name='array')
# BS_stddev.attrs['crs'] = CRS('EPSG:3577')
# write_geotiff(filename=str(AOI)+'_BS_stddev_2015.tif', dataset=BS_stddev)
# print(str(AOI)+'_BS_stddev_2015.tif')

# BS_min = ds['BS'].groupby('time.year').min(dim = 'time').squeeze().drop('year')
# BS_min = BS_min.to_dataset(name='array')
# BS_min.attrs['crs'] = CRS('EPSG:3577')
# write_geotiff(filename=str(AOI)+'_BS_min_2015.tif', dataset=BS_min)
# print(str(AOI)+'_BS_min_2015.tif')

# ds_mask = ds['BS'] > (ds['PV'] + ds['NPV'])
# ds_bare = ds_mask.where(ds['BS'] > 0)
# ds_summary = ds_bare.mean(dim = 'time')
# ds_summary = ds_summary.to_dataset(name='array')
# ds_summary.attrs['crs'] = CRS('EPSG:3577')
# write_geotiff(filename=str(AOI)+'_BS_summary_2015.tif', dataset=ds_summary)
# print(str(AOI)+'_ds_summary_2015.tif')

ds_mask = ds['BS'] > ds['PV']
ds_bare = ds_mask.where(ds['BS'] > 0)
ds_summary = ds_bare.mean(dim = 'time')
ds_summary = ds_summary.to_dataset(name='array')
ds_summary.attrs['crs'] = CRS('EPSG:3577')
write_geotiff(filename=str(AOI)+'_BS_PVsummary_2015.tif', dataset=ds_summary)
print(str(AOI)+'_ds_summary_2015.tif')