import sys
import xarray
import datacube
from datacube.storage import masking
from datacube.helpers import write_geotiff
from datacube.utils.geometry import CRS

# import DEADataHandling for clear Fractional Cover Scenes
sys.path.append('/g/data/r78/LCCS_Aberystwyth/co6850/dea-notebooks/10_Scripts/')
import DEADataHandling

res = (-25, 25)
time = ('2015-01-01', '2015-12-31')
crs = 'EPSG:3577'

Ayr = {'AOI': 'Ayr', 'query': {'x':(1500000, 1600000), 'y':(-2200000, -2100000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1500000, 'target_max_x': 1600000, 'target_min_y':-2200000, 'target_max_y': -2100000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Diamantina = {'AOI': 'Diamantina', 'query': {'x':(800000, 900000), 'y':(-2800000, -2700000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 800000, 'target_max_x': 900000, 'target_min_y':-2800000, 'target_max_y': -2700000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Gwydir = {'AOI': 'Gwydir', 'query': {'x':(1600000, 1700000), 'y':(-3400000, -3300000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1600000, 'target_max_x': 1700000, 'target_min_y':-3400000, 'target_max_y': -3300000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Leichhardt = {'AOI': 'Leichhardt', 'query': {'x':(800000, 900000), 'y':(-2000000, -1900000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 800000, 'target_max_x': 900000, 'target_min_y':-2000000, 'target_max_y': -1900000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Kakadu = {'AOI': 'Kakadu', 'query': {'x':(0, 100000), 'y':(-1350000, -1250000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 0, 'target_max_x': 100000, 'target_min_y':-1350000, 'target_max_y': -1250000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Hobart = {'AOI': 'Hobart', 'query': {'x':(1200000, 1300000), 'y':(-4800000, -4700000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1200000, 'target_max_x': 1300000, 'target_min_y':-4800000, 'target_max_y': -4700000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Perth = {'AOI': 'Perth', 'query': {'x':(-1550000, -1450000), 'y':(-3650000, -3550000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': -1550000, 'target_max_x': -1450000, 'target_min_y':-3650000, 'target_max_y': -3550000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Murray_Valley = {'AOI': 'Murray_Valley', 'query': {'x':(1100000, 1200000), 'y':(-4000000, -3900000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1100000, 'target_max_x': 1200000, 'target_min_y':-4000000, 'target_max_y': -3900000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Adelaide = {'AOI': 'Adelaide', 'query': {'x':(550000, 650000), 'y':(-3850000, -3750000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 550000, 'target_max_x': 650000, 'target_min_y':-3850000, 'target_max_y': -3750000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Lake_Eyre = {'AOI': 'Lake_Eyre', 'query': {'x':(500000, 600000), 'y':(-3000000, -2900000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 500000, 'target_max_x': 600000, 'target_min_y':-3000000, 'target_max_y': -2900000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Blue_Mtns = {'AOI': 'Blue_Mtns', 'query': {'x':(1600000, 1700000), 'y':(-3900000, -3800000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1600000, 'target_max_x': 1700000, 'target_min_y':-3900000, 'target_max_y': -3800000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Aust_Alps = {'AOI': 'Aust_Alps', 'query': {'x':(1400000, 1500000), 'y':(-4100000, -4000000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1400000, 'target_max_x': 1500000, 'target_min_y':-4100000, 'target_max_y': -4000000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Collier_Range = {'AOI': 'Collier_Range', 'query': {'x':(-1300000, -1200000), 'y':(-2700000, -2600000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': -1300000, 'target_max_x': -1200000, 'target_min_y':-2700000, 'target_max_y': -2600000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Coorong = {'AOI': 'Coorong', 'query': {'x':(600000, 700000), 'y':(-3950000, -3850000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 600000, 'target_max_x': 700000, 'target_min_y':-3950000, 'target_max_y': -3850000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Brisbane = {'AOI': 'Brisbane', 'query': {'x':(2000000, 2100000), 'y':(-3200000, -3100000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x':2000000, 'target_max_x': 2100000, 'target_min_y':-3200000, 'target_max_y': -3100000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Mt_Ney = {'AOI': 'Mt_Ney', 'query': {'x':(-1000000, -900000), 'y':(-3650000, -3550000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x':-1000000, 'target_max_x': -900000, 'target_min_y':-3650000, 'target_max_y': -3550000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

# sites = [Ayr, Diamantina, Gwydir, Leichhardt, Kakadu, Hobart, Perth, Murray_Valley, Adelaide, Lake_Eyre, Blue_Mtns, Aust_Alps, Collier_Range, Coorong, Brisbane, Mt_Ney]

sites = [Coorong]


for site in sites:
    
    mask_dict = {'cloud_acca': 'no_cloud',
                 'cloud_fmask': 'no_cloud',
                 'contiguous': True}
    
    dc = datacube.Datacube(app="veg_mask")
    
    FC = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                           sensors=['ls5','ls7','ls8'],
                                           product = 'fc',
                                           masked_prop=0.1,
                                           mask_dict=mask_dict)


    tv_mask = FC['BS'] < FC['PV']
    tv = tv_mask.where(FC['BS'] > 0)
    tv_summary = tv.mean(dim='time')
    tv_thres = 1-(tv_summary > 0.167)
    vegetat_veg_cat_ds = tv_thres.to_dataset(name="vegetat_veg_cat").squeeze()
    

##### Just WOfS #####
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                         **site['query'])
    wofs_ann = masking.mask_invalid_data(wofs_ann)
    aquatic_wat = 1-(wofs_ann["frequency"] >= 0.2)

    
# ##### WOfS, ITEM, Mangrove #####
# cant get this to work at the moment due to env problem, though seems to work for level3.py

#     dc = datacube.Datacube(env='lccs_dev', app='veg_mask')

#     wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
#                          **site['query'])
#     wofs_ann = masking.mask_invalid_data(wofs_ann)    
#     item = dc.load(product="item_v2", measurements=["relative"], **site['query'])
#     item = masking.mask_invalid_data(item)
#     item = item.squeeze().drop('time')
#     mangrove = dc.load(product="mangrove_extent_cover_albers", measurements=["extent"],**site['query'])
#     mangrove = masking.mask_invalid_data(mangrove)
#     aquatic_wat = 1-(((wofs_ann["frequency"] >= 0.2) | ((item["relative"] >= 2)
#                                                      & (item["relative"] <= 8)) | (mangrove["extent"] == 1)))

#     dc = datacube.Datacube(app='veg_mask')

    aquatic_wat_cat_ds= aquatic_wat.to_dataset(name="aquatic_wat_cat").squeeze().drop('time')
    mask = vegetat_veg_cat_ds["vegetat_veg_cat"] * aquatic_wat_cat_ds["aquatic_wat_cat"]
    
    # output veg mask
    mask = mask.to_dataset(name='array')
    mask.attrs['crs'] = CRS('EPSG:3577')
    mask = mask.astype(dtype=float)
    write_geotiff(filename=str(site['AOI'])+'_veg_mask_2015.tif', dataset=mask)
    print(str(site['AOI'])+'_veg_mask_2015.tif')    

