'''

Workflow follows on from OSM.py to generate binned histogram
Script is intended to be used with OSM.py to get AOI_artifical surface.tif
Takes tif images with identical extents and runs binned histogram (i.e. zonal statistics like) based on AOI_artsurfacecover.tif
Examples would include indices derived from DEA (i.e. NDBI, MADs)
Generates a pdf figure with model showing relationship

Chris Owers, Pete Bunting June 2019


'''

import pandas as pd
import numpy as np
import georasters as gr
import matplotlib.pyplot as plt
import random
import sklearn.linear_model
import sklearn.pipeline

######## Required inputs ##########

# Input artsurfacecover.tif created from OSM.py
# artsurface = './AOI_artsurfacecover.tif'
artsurface = './artsurfacecover_masked.tif'
# Input other datasets to compare to
edev_MAD = './MAD_edev_normalised.tif'
sdev_MAD = './MAD_edev_normalised.tif'
bcdev_MAD = './MAD_edev_normalised.tif'
NDBI = './NDBI.tif'
MNDWI = './MNDWI.tif'
BRGI = './BRGI.tif'
TCB = './TCB.tif'
kmeans = './mads_kmeans_masked.tif'

# Set variable from input above for generating relationship to artificial surface cover
var = artsurface
varname = "artsurface"


######## Read data into Pandas dataframe############
kmeans = gr.from_file(kmeans)
var = gr.from_file(var)

# Convert to Pandas DataFrame
artsurface_data = kmeans.to_pandas()
var_data = var.to_pandas()

# Merge datasets on coordinates
dataValsFrame = pd.merge(artsurface_data, var_data, on=['x', 'y'])


########## Split data to train and validate ############
ids = dataValsFrame.index.values.astype(int)
random.shuffle(ids)

n = int(len(ids)/2)
ids1, ids2 = ids[:n], ids[n:]

dfTrain = dataValsFrame[dataValsFrame.index.isin(ids1)]
print('Train: ', dfTrain.shape)
dfValid = dataValsFrame[dataValsFrame.index.isin(ids2)]
print('Validate: ', dfValid.shape)


######## Bin data to generate relationship ############

# value_x is first dataset input into dataValsFrame (i.e. artsurface_data)
aggCCDF = dfTrain.groupby('value_x').agg({'value_y': ['mean','std']})
xVals = aggCCDF.index.values
yVals = aggCCDF['value_y']['mean'].values
errVals = aggCCDF['value_y']['std'].values
errVals[np.isnan(errVals)] = 0
print('Mean Bin Std Dev: ', np.mean(errVals))


# ######## Plot result ############ - commented out as plotting below after fit model
# ax = plt.subplot(111)
# ax.scatter(xVals, yVals, color='#808080', zorder=10)
# ax.errorbar(xVals,yVals,yerr=errVals, color='#E0E0E0', linestyle="None", zorder=1)
# ax.set_title('Artificial surface cover vs Binned '+str(varname))
# ax.set_xlabel('Cover (%)')
# ax.set_ylabel(varname)
# ax.set_xlim(0,100)
# # ax.set_ylim()

# plt.savefig('Cover_v_Bin'+str(varname)+'_errbars.pdf')

######### Fit model ############
model = sklearn.pipeline.Pipeline([('poly', sklearn.preprocessing.PolynomialFeatures(degree=2)), ('linear', sklearn.linear_model.LinearRegression(fit_intercept=False))])
model.fit(xVals.reshape(-1, 1), yVals)
rsq = model.score(xVals.reshape(-1, 1), yVals)
print('r sq = ', rsq)
pvStep = np.arange(0, 100, 1)
ccSteps = model.predict(pvStep.reshape(-1, 1))


######### Plot model result ############
ax = plt.subplot(111)
ax.plot(pvStep, ccSteps, color='black', zorder=20)
ax.scatter(xVals, yVals, color='#808080', zorder=10)
ax.errorbar(xVals,yVals,yerr=errVals, color='#E0E0E0', linestyle="None", zorder=1)

ax.set_title('Kmeans vs Binned '+str(varname))
ax.set_xlabel('Kmeans Class')
ax.set_ylabel(varname)
ax.set_xlim(0,50)
ax.set_ylim(0,100)
ax.text(0.1, 0.9, r'$r^2$='+str(round(rsq,4)), ha='center', va='center', transform=ax.transAxes, fontsize=10)
plt.savefig('Cover_v_Bin'+str(varname)+'_relationship_errbars.pdf')

