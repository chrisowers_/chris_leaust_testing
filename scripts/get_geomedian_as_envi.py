import os
import gdal
import numpy as np

import datacube
from datacube.storage import masking
from datacube.helpers import write_geotiff
from datacube.utils.geometry import CRS

dc = datacube.Datacube(app="geomedian")

# # Perth
# x = (-1550000, -1450000)
# y = (-3650000, -3550000)

# Kakadu
x = (0, 100000)
y = (-1350000, -1250000)

# # Kalgoorlie
# x = (-1002450,  -972375)
# y = (-3402100, -3366125)
                                         
res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

# Save DEA data as tif and img
geomedian = dc.load(product=sensor + "_nbart_geomedian_annual", time=time, **query)
geomedian = masking.mask_invalid_data(geomedian).squeeze().drop('time')
# geomedian *= 1.0/geomedian.max()
geomedian = geomedian.astype(np.float32)
geomedian.attrs['crs'] = CRS('EPSG:3577')

write_geotiff(filename='Kalgoorlie_2010.tif', dataset=geomedian)

# ds = gdal.Open('Kalgoorlie_2010.tif')
# ds = gdal.Translate('Kalgoorlie_2010.img', ds, format="ENVI")
# ds = None

# Save DEA data as tif and img - seperate files for each band
# data = dc.load(product=sensor + "_nbart_geomedian_annual", time=time, **query)
# location = "kakadu"
# year = "2015"
# suffix = '.tif'
# for var in data.data_vars:
#     out = data[var]
#     out = out.to_dataset(name=var)
#     out *= 1.0/10000
#     out = out.astype(np.float32)
#     out.attrs['crs']=CRS(data.crs)
#     out = out.drop('time').isel(time=0)
#     name = f'{location}_{year}_{var}{suffix}'
#     write_geotiff(name, out)
#     name_out = f'{location}_{year}_{var}.img'
#     envi = gdal.Open(name)
#     gdal.Translate(name_out, envi, format="ENVI")
