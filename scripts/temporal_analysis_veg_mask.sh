#!/bin/bash

#PBS -N temporal_analysis_veg_mask
#PBS -P r78
#PBS -q express
#PBS -M cho18@aber.ac.uk
#PBS -m abe
#PBS -l walltime=00:15:00
#PBS -l mem=64GB
#PBS -l ncpus=4
#PBS -l wd

module use /g/data/v10/public/modules/modulefiles
module load dea
python temporal_analysis_veg_mask.py