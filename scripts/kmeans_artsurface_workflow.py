'''

Workflow is designed to follow on from create_artsurface_fromOSM.py
This script runs kmeans with k classes on an input variable (i.e. MADs) and uses artsurfacecover (%)
to assign classes to artificial surface or bare areas

Using this script is likely to involve and iterative process of how many SpectralClasses to use
as well as the threshold to use for deciding if a kmeans class should be classifed as artificial surface.
For this purpose a histogram (.pdf) is generated to view the k means classes and artificial surface cover.

Output file name = AOI_dataset_kmeans_spectralclasses_binaryoutput(artificial surface = 1) i.e. Perth_mads_kmeans_c10_output_binary.tif

Chris Owers July 2019
Aberystwyth University

'''

import os
import gdal
from rios import rat
from rsgislib import classification, segmentation, rastergis, imageutils

########## Required Inputs ##########

AOI = 'Adelaide'

# input image for kmeans (i.e. MADs dataset)
InputImageName = 'mads'
InputImage = '/Users/chrisowers/Desktop/temp/artsurface/mads_normalised_outs/'+str(AOI)+'_mads_normalised.tif'

# input image for artificial surface cover (%) - run create_artsurface_fromOSM.py to generate this file
artsurface = './'+str(AOI)+'_artsurfacecover.tif'

# number of classes for kmeans
SpectralClasses = 5

# delete temporary files when finished?
RemoveTemp = True

os.makedirs('./'+str(AOI)+'_kmeans', exist_ok=True)
os.makedirs('./'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses), exist_ok=True)


########## kmeans classification #########

from unsupervised_kmeans_classification import ClassifyImage
OutputImage = './'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'.kea'
GDALformat = 'KEA'
SampleSize = 0.01
ClassifyImage(InputImage, OutputImage, GDALformat, SpectralClasses, SampleSize)


########## plot histogram of kmeans classes vs artificial surface cover (%) to see distribution within each kmeans class ##########

from binned_histogram import histogram

# Set variable from OutputImage above for generating relationship to artificial surface cover
variable = './'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'.kea'

histogram(AOI, SpectralClasses, InputImageName, artsurface, variable)


########## Perform zonal statistics and populate RAT of kmeans classified image with % artificial surface cover ##########

# clump kmeans image to create RAT
inputimage = './'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'.kea'
outputimage = './'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'_clump.kea'
gdalformat = 'KEA'
segmentation.clump(inputimage, outputimage, gdalformat, True, 0, True)

# collapse on class PixelVal to get number of kmeans classes as clumps
inputimage = './'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'_clump.kea'
outputimage = './'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'_output.kea'
classification.collapseClasses(inputimage, outputimage, gdalformat, 'PixelVal', 'PixelVal')

# repopulate stats (histogram required for following function populateRATWithStats)
clumps = './'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'_output.kea'
rastergis.populateStats(clumps, True, True, True)

# populate kmeans with artificial surface cover stats
input = './'+str(AOI)+'_artsurfacecover.tif'
clumps = './'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'_output.kea'
bs = []
bs.append(rastergis.BandAttStats(band=1, minField='b1Min', maxField='b1Max', meanField='b1Mean', sumField='b1Sum', stdDevField='b1StdDev'))
rastergis.populateRATWithStats(input, clumps, bs)


########## Add column to RAT specifying threshold of mean artificial surface cover (%) to classify as artificial surface

# add column to kea specifying n > % threshold for artificial surface
im = gdal.Open('./'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'_output.kea', 1)
data = rat.readColumn(im, 'b1Mean')

outclass = []
for n in data:
    if n > 10:
        outclass.append(1)
    else:
        outclass.append(0)
print(outclass)
rat.writeColumn(im, 'outclass', outclass)
del data, im

# collapse on class outclass to get binary map of artificial surface (1) and bare areas (0)
inputimage = './'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'_output.kea'
outputimage = './'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'_output_binary.tif'
gdalformat = 'GTiff'
classification.collapseClasses(inputimage, outputimage, gdalformat, 'outclass', 'outclass')

# making 0 no data value - might not want this but using it at the moment for display
from rsgislib import imageutils
inputImage = './'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'_output_binary.tif'
imageutils.popImageStats(inputImage,True,0,True)


########## remove temporary files ##########

if RemoveTemp is True:
    os.remove('./'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'.kea')
    os.remove('./'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'_clump.kea')
    os.remove('./'+str(AOI)+'_kmeans/temp_c'+str(SpectralClasses)+'/'+str(AOI)+'_'+str(InputImageName)+'_kmeans_c'+str(SpectralClasses)+'_output.kea')
    print('Generated kmeans using '+str(SpectralClasses)+' classes on '+str(InputImageName)+' for Artificial Surface Cover (%) for ' +str(AOI))
else:
    print('Generated kmeans using '+str(SpectralClasses)+' classes on '+str(InputImageName)+' for Artificial Surface Cover (%) for ' +str(AOI))
