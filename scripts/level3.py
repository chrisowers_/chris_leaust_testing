'''

current running script of level3 (copy of notebook)

'''

import sys, os
import numpy
import xarray
import rasterio
from matplotlib import pyplot
import datacube
from datacube.storage import masking
from datacube.helpers import write_geotiff
from datacube.utils.geometry import CRS

import scipy
from scipy import stats
from skimage.segmentation import quickshift
from sklearn.impute import SimpleImputer

# must specific lccs_dev environment for MADs and mangrove datasets
dc = datacube.Datacube(env='lccs_dev', app='level3')

# import DEADataHandling for clear Fractional Cover Scenes
sys.path.append('../../../dea-notebooks/10_Scripts')
import DEADataHandling
import BandIndices
import TasseledCapTools


# import le_lccs modules
sys.path.append('../../../livingearth_lccs')
from le_lccs.le_ingest import gridded_ingest
from le_lccs.le_classification import lccs_l3

os.makedirs('./level3', exist_ok=True)

res = (-25, 25)
time = ('2015-01-01', '2015-12-31')
crs = 'EPSG:3577'
sensor = 'ls8'

Ayr = {'AOI': 'Ayr', 'query': {'x':(1500000, 1600000), 'y':(-2200000, -2100000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1500000, 'target_max_x': 1600000, 'target_min_y':-2200000, 'target_max_y': -2100000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Diamantina = {'AOI': 'Diamantina', 'query': {'x':(800000, 900000), 'y':(-2800000, -2700000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 800000, 'target_max_x': 900000, 'target_min_y':-2800000, 'target_max_y': -2700000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Gwydir = {'AOI': 'Gwydir', 'query': {'x':(1600000, 1700000), 'y':(-3400000, -3300000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1600000, 'target_max_x': 1700000, 'target_min_y':-3400000, 'target_max_y': -3300000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Leichhardt = {'AOI': 'Leichhardt', 'query': {'x':(800000, 900000), 'y':(-2000000, -1900000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 800000, 'target_max_x': 900000, 'target_min_y':-2000000, 'target_max_y': -1900000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Kakadu = {'AOI': 'Kakadu', 'query': {'x':(0, 100000), 'y':(-1350000, -1250000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 0, 'target_max_x': 100000, 'target_min_y':-1350000, 'target_max_y': -1250000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Hobart = {'AOI': 'Hobart', 'query': {'x':(1200000, 1300000), 'y':(-4800000, -4700000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1200000, 'target_max_x': 1300000, 'target_min_y':-4800000, 'target_max_y': -4700000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Perth = {'AOI': 'Perth', 'query': {'x':(-1550000, -1450000), 'y':(-3650000, -3550000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': -1550000, 'target_max_x': -1450000, 'target_min_y':-3650000, 'target_max_y': -3550000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Murray_Valley = {'AOI': 'Murray_Valley', 'query': {'x':(1100000, 1200000), 'y':(-4000000, -3900000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1100000, 'target_max_x': 1200000, 'target_min_y':-4000000, 'target_max_y': -3900000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Adelaide = {'AOI': 'Adelaide', 'query': {'x':(550000, 650000), 'y':(-3850000, -3750000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 550000, 'target_max_x': 650000, 'target_min_y':-3850000, 'target_max_y': -3750000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Lake_Eyre = {'AOI': 'Lake_Eyre', 'query': {'x':(500000, 600000), 'y':(-3000000, -2900000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 500000, 'target_max_x': 600000, 'target_min_y':-3000000, 'target_max_y': -2900000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Blue_Mtns = {'AOI': 'Blue_Mtns', 'query': {'x':(1600000, 1700000), 'y':(-3900000, -3800000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1600000, 'target_max_x': 1700000, 'target_min_y':-3900000, 'target_max_y': -3800000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Aust_Alps = {'AOI': 'Aust_Alps', 'query': {'x':(1400000, 1500000), 'y':(-4100000, -4000000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1400000, 'target_max_x': 1500000, 'target_min_y':-4100000, 'target_max_y': -4000000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Collier_Range = {'AOI': 'Collier_Range', 'query': {'x':(-1300000, -1200000), 'y':(-2700000, -2600000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': -1300000, 'target_max_x': -1200000, 'target_min_y':-2700000, 'target_max_y': -2600000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Coorong = {'AOI': 'Coorong', 'query': {'x':(600000, 700000), 'y':(-3950000, -3850000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 600000, 'target_max_x': 700000, 'target_min_y':-3950000, 'target_max_y': -3850000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Brisbane = {'AOI': 'Brisbane', 'query': {'x':(2000000, 2100000), 'y':(-3200000, -3100000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x':2000000, 'target_max_x': 2100000, 'target_min_y':-3200000, 'target_max_y': -3100000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Mt_Ney = {'AOI': 'Mt_Ney', 'query': {'x':(-1000000, -900000), 'y':(-3650000, -3550000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x':-1000000, 'target_max_x': -900000, 'target_min_y':-3650000, 'target_max_y': -3550000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}


###################################################################################################################    

# Normal
sites = [Murray_Valley, Blue_Mtns, Aust_Alps]


for site in sites:
    ########## veg/non veg ##########
   
    # need to specify as pq not indexed in lccs_dev
    dc = datacube.Datacube(env='datacube', app="level3")
    
    mask_dict = {'cloud_acca': 'no_cloud',
                 'cloud_fmask': 'no_cloud',
                 'contiguous': True}
    # New vegetation classification
    FC = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                           sensors=['ls5','ls7','ls8'],
                                           product = 'fc',
                                           masked_prop=0.1,
                                           mask_dict=mask_dict)

    # Create a mask to show areas where total vegetation is greater than the baresoil fraction of a pixel for each scene
    tv_mask = FC['BS'] < (FC['PV'] + (FC['NPV'] < 90))
    tv = tv_mask.where(FC['BS'] > 0)
    # Calculate proportion of time where total vegetation is greater than the bare soil fraction of a pixel for the input year
    tv_summary = tv.mean(dim='time')
    tv_thres = tv_summary > 0.167
    
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], **site['query'])
    wofs_ann = masking.mask_invalid_data(wofs_ann)    
    aquatic_wat = 1-((wofs_ann["frequency"] >= 0.9))
    tv_thres = tv_thres * aquatic_wat

    vegetat_veg_cat_ds = tv_thres.to_dataset(name="vegetat_veg_cat").squeeze()
    
    # back to lccs_dev
    dc = datacube.Datacube(env='lccs_dev', app="level3")

    
    ########## aquatic/terrestrial ##########
    # Load data from datacube
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], **site['query'])
    wofs_ann = masking.mask_invalid_data(wofs_ann)

#     item = dc.load(product="item_v2", measurements=["relative"], 
#                          **site['query'])
#     item = masking.mask_invalid_data(item)
#     item = item.squeeze().drop('time')

#     mangrove = dc.load(product="mangrove_extent_cover_albers", measurements=["extent"], 
#                          **site['query'])
#     mangrove = masking.mask_invalid_data(mangrove)

    # Create binary layer representing aquatic (1) and terrestrial (0)

    aquatic_wat = ((wofs_ann["frequency"] >= 0.2))

#     For coastal landscapes use the following
#     aquatic_wat = ((wofs_ann["frequency"] >= 0.2) | ((item["relative"] >= 2) & (item["relative"] <= 8)) | (mangrove["extent"] == 1))

    # Convert to Dataset and add name
    aquatic_wat_cat_ds = aquatic_wat.to_dataset(name="aquatic_wat_cat").squeeze().drop('time')             

    
    ########## cultman/natural ##########
#     # Load data from datacube
#     mads = dc.load(product=sensor +"_nbart_tmad_annual", **site['query'])
#     mads = masking.mask_invalid_data(mads)

#     # Normalise measurements using log function
#     log_edev = numpy.log(1/mads["edev"])
#     log_sdev = numpy.log(1/mads["sdev"])
#     log_bcdev = numpy.log(1/mads["bcdev"])

#     # Create binary layer representing cultivated (1) and natural (0)
#     cultman = ((log_edev <= 2) & (log_bcdev <= 1.75)) | (log_sdev <= 1.3)
#     cultman  = 1-cultman

#     # Convert to Dataset and add name
#     cultman_agr_cat_ds = cultman.to_dataset(name="cultman_agr_cat").squeeze().drop('time')
    
    # Sean's cultivated
    tmad = dc.load(product=sensor +"_nbart_tmad_annual", **site['query'])
    tmad = tmad.isel(time=0)
    tmad = tmad.drop('time')
    
    # Impute missing values (0 and NaN)
    imp_0 = SimpleImputer(missing_values=0, strategy='mean')
    container = {}
    for key in tmad.data_vars:
        d = tmad[key].data.squeeze()
        d = numpy.nan_to_num(d)
        d = numpy.where(d < 0, 0, d)
        d = numpy.where(d == 1, 0, d)
        imp_0.fit(d)
        d = imp_0.transform(d)
        d = -numpy.log(d)
        container.update({key: d})
    tmad['edev'].data = container['edev']
    tmad['sdev'].data = container['sdev']
    tmad['bcdev'].data = container['bcdev']
    
    # Calculate the mean of all the tmad inputs
    tmad_mean = numpy.mean(numpy.stack([tmad.edev.data, tmad.sdev.data, tmad.bcdev.data], axis=-1), axis=-1)
    # Convert type to float64 (required for quickshift)
    tmad_mean = numpy.float64(tmad_mean)
    # Segment
    tmad_seg = quickshift(tmad_mean, kernel_size=5, convert2lab=False, max_dist=500, ratio=0.5)
    # Calculate the median for each segment
    tmad_median_seg = scipy.ndimage.median(input=tmad_mean, labels=tmad_seg, index=tmad_seg)
    # Set threshold as 10th percentile of mean TMAD
    thresh = numpy.percentile(tmad_mean.ravel(), 20)
    # Create boolean layer using threshold
    tmad_thresh = tmad_median_seg < thresh
    cultman_agr_cat_ds = xarray.Dataset({'cultman_agr_cat': (tmad.dims, tmad_thresh)}, coords=tmad.coords)
    
    ########## artsurface/natural ##########
    dc = datacube.Datacube(env='datacube', app="level3")  
    LS = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                       sensors=['ls5','ls7','ls8'],
                                       masked_prop=0.1,
                                       mask_dict=mask_dict)
    
    TCe = TasseledCapTools.pct_exceedance_tasseled_cap(LS, tc_bands=['greenness','brightness', 'wetness'],
                                               greenness_threshold=-500, 
                                               brightness_threshold=5000, 
                                               wetness_threshold=-2500, 
                                               drop=True, drop_tc_bands=True)
    TCe_thresG = TCe.greenness_pct_exceedance < 0.1
    TCe_thresB = TCe.brightness_pct_exceedance < 0.1
    TCe_thresW = TCe.wetness_pct_exceedance > 0.9
    TCE = TCe_thresG | TCe_thresB & TCe_thresW
    urban  = TCE > 0
    artific_urb_cat_ds = urban.to_dataset(name="artific_urb_cat").squeeze()
    

    ########## artwater/natural water ##########
    # Load data
    var_file = "/g/data/u46/wofs/confidence/geoFabric/geofabric.tif"
    var_name = "features"

    import_obj = gridded_ingest.LEIngestGDAL(**site['query_geofabric'])
    geofabric = import_obj.read_to_xarray(var_file, var_name)

    # Create binary layer representing artificial water (1) and natural water (0)
    artwatr_wat_cat = ((geofabric["features"] == 1) | (geofabric["features"] == 8))

    # Convert to Dataset and add name
    artwatr_wat_cat_ds = artwatr_wat_cat.to_dataset(name="artwatr_wat_cat")
    
    ########## LCCS classification ##########
    variables_xarray_list = []
    variables_xarray_list.append(vegetat_veg_cat_ds)
    variables_xarray_list.append(aquatic_wat_cat_ds)
    variables_xarray_list.append(cultman_agr_cat_ds)
    variables_xarray_list.append(artific_urb_cat_ds)
    variables_xarray_list.append(artwatr_wat_cat_ds)

    # Merge to a single dataframe
    classification_data = xarray.merge(variables_xarray_list)

    # Apply Level 3 classification using separate function. Works through in three stages
    level1, level2, level3 = lccs_l3.classify_lccs_level3(classification_data)

    # Save classification values back to xarray
    out_class_xarray = xarray.Dataset(
        {"level1" : (classification_data["vegetat_veg_cat"].dims, level1),
         "level2" : (classification_data["vegetat_veg_cat"].dims, level2),
         "level3" : (classification_data["vegetat_veg_cat"].dims, level3)})
    classification_data = xarray.merge([classification_data, out_class_xarray])


    # Write out 
    output_rgb_file_name = 'level3/'+str(site['AOI'])+'_l3_layers_'+str(time[0])+'.tif'
    out_file_transform = [site['query_geofabric']['target_pixel_size_x'], 0, site['query_geofabric']['target_min_x'], 0, site['query_geofabric']['target_pixel_size_y'], site['query_geofabric']['target_max_y']]
    output_x_size = int((site['query_geofabric']['target_max_x'] - site['query_geofabric']['target_min_x'])/site['query_geofabric']['target_pixel_size_x'])
    output_y_size = int((site['query_geofabric']['target_min_y'] - site['query_geofabric']['target_max_y'])/site['query_geofabric']['target_pixel_size_y'])

    # Write
    rgb_dataset = rasterio.open(output_rgb_file_name, 'w', driver='GTiff',
                                height=output_y_size, width=output_x_size,
                                count=5, dtype=level3.dtype,
                                crs=crs, transform=out_file_transform)
    rgb_dataset.write(classification_data.vegetat_veg_cat.values.astype(numpy.uint8), 1)
    rgb_dataset.write(classification_data.aquatic_wat_cat.values.astype(numpy.uint8), 2)
    rgb_dataset.write(classification_data.cultman_agr_cat.values.astype(numpy.uint8), 3)
    rgb_dataset.write(classification_data.artific_urb_cat.values.astype(numpy.uint8), 4)
    rgb_dataset.write(classification_data.artwatr_wat_cat.values.astype(numpy.uint8), 5)
    rgb_dataset.close()

    red, green, blue, alpha = lccs_l3.colour_lccs_level3(level3)
    
    # Write out
    output_rgb_file_name = 'level3/'+str(site['AOI'])+'_l3_'+str(time[0])+'.tif'
    out_file_transform = [site['query_geofabric']['target_pixel_size_x'], 0, site['query_geofabric']['target_min_x'], 0, site['query_geofabric']['target_pixel_size_y'], site['query_geofabric']['target_max_y']]
    output_x_size = int((site['query_geofabric']['target_max_x'] - site['query_geofabric']['target_min_x'])/site['query_geofabric']['target_pixel_size_x'])
    output_y_size = int((site['query_geofabric']['target_min_y'] - site['query_geofabric']['target_max_y'])/site['query_geofabric']['target_pixel_size_y'])

    # Write RGB colour scheme out
    rgb_dataset = rasterio.open(output_rgb_file_name, 'w', driver='GTiff',
                                height=output_y_size, width=output_x_size,
                                count=3, dtype=level3.dtype,
                                crs=crs, transform=out_file_transform)
    rgb_dataset.write(red, 1)
    rgb_dataset.write(green, 2)
    rgb_dataset.write(blue, 3)
    rgb_dataset.close()
    
###################################################################################################################    
    
    
# Coastal with mangroves
sites = [Ayr, Adelaide, Brisbane]

for site in sites:
    ########## veg/non veg ##########
   
    # need to specify as pq not indexed in lccs_dev
    dc = datacube.Datacube(env='datacube', app="level3")
    
    mask_dict = {'cloud_acca': 'no_cloud',
                 'cloud_fmask': 'no_cloud',
                 'contiguous': True}
    # New vegetation classification
    FC = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                           sensors=['ls5','ls7','ls8'],
                                           product = 'fc',
                                           masked_prop=0.1,
                                           mask_dict=mask_dict)

    # Create a mask to show areas where total vegetation is greater than the baresoil fraction of a pixel for each scene
    tv_mask = FC['BS'] < (FC['PV'] + (FC['NPV'] < 90))
    tv = tv_mask.where(FC['BS'] > 0)
    # Calculate proportion of time where total vegetation is greater than the bare soil fraction of a pixel for the input year
    tv_summary = tv.mean(dim='time')
    tv_thres = tv_summary > 0.167
    
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], **site['query'])
    wofs_ann = masking.mask_invalid_data(wofs_ann)    
    aquatic_wat = 1-((wofs_ann["frequency"] >= 0.9))
    tv_thres = tv_thres * aquatic_wat

    vegetat_veg_cat_ds = tv_thres.to_dataset(name="vegetat_veg_cat").squeeze()
    
    # back to lccs_dev
    dc = datacube.Datacube(env='lccs_dev', app="level3")

    
    ########## aquatic/terrestrial ##########
    # Load data from datacube
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], **site['query'])
    wofs_ann = masking.mask_invalid_data(wofs_ann)

    item = dc.load(product="item_v2", measurements=["relative"], 
                         **site['query'])
    item = masking.mask_invalid_data(item)
    item = item.squeeze().drop('time')

    mangrove = dc.load(product="mangrove_extent_cover_albers", measurements=["extent"], 
                         **site['query'])
    mangrove = masking.mask_invalid_data(mangrove)

#     Create binary layer representing aquatic (1) and terrestrial (0)

#     aquatic_wat = ((wofs_ann["frequency"] >= 0.2))

#     For coastal landscapes use the following
    aquatic_wat = ((wofs_ann["frequency"] >= 0.2) | ((item["relative"] >= 2) & (item["relative"] <= 8)) | (mangrove["extent"] == 1))

    # Convert to Dataset and add name
    aquatic_wat_cat_ds = aquatic_wat.to_dataset(name="aquatic_wat_cat").squeeze().drop('time')             

    
    ########## cultman/natural ##########
#     # Load data from datacube
#     mads = dc.load(product=sensor +"_nbart_tmad_annual", **site['query'])
#     mads = masking.mask_invalid_data(mads)

#     # Normalise measurements using log function
#     log_edev = numpy.log(1/mads["edev"])
#     log_sdev = numpy.log(1/mads["sdev"])
#     log_bcdev = numpy.log(1/mads["bcdev"])

#     # Create binary layer representing cultivated (1) and natural (0)
#     cultman = ((log_edev <= 2) & (log_bcdev <= 1.75)) | (log_sdev <= 1.3)
#     cultman  = 1-cultman

#     # Convert to Dataset and add name
#     cultman_agr_cat_ds = cultman.to_dataset(name="cultman_agr_cat").squeeze().drop('time')
    
    # Sean's cultivated
    tmad = dc.load(product=sensor +"_nbart_tmad_annual", **site['query'])
    tmad = tmad.isel(time=0)
    tmad = tmad.drop('time')
    
    # Impute missing values (0 and NaN)
    imp_0 = SimpleImputer(missing_values=0, strategy='mean')
    container = {}
    for key in tmad.data_vars:
        d = tmad[key].data.squeeze()
        d = numpy.nan_to_num(d)
        d = numpy.where(d < 0, 0, d)
        d = numpy.where(d == 1, 0, d)
        imp_0.fit(d)
        d = imp_0.transform(d)
        d = -numpy.log(d)
        container.update({key: d})
    tmad['edev'].data = container['edev']
    tmad['sdev'].data = container['sdev']
    tmad['bcdev'].data = container['bcdev']
    
    # Calculate the mean of all the tmad inputs
    tmad_mean = numpy.mean(numpy.stack([tmad.edev.data, tmad.sdev.data, tmad.bcdev.data], axis=-1), axis=-1)
    # Convert type to float64 (required for quickshift)
    tmad_mean = numpy.float64(tmad_mean)
    # Segment
    tmad_seg = quickshift(tmad_mean, kernel_size=5, convert2lab=False, max_dist=500, ratio=0.5)
    # Calculate the median for each segment
    tmad_median_seg = scipy.ndimage.median(input=tmad_mean, labels=tmad_seg, index=tmad_seg)
    # Set threshold as 10th percentile of mean TMAD
    thresh = numpy.percentile(tmad_mean.ravel(), 20)
    # Create boolean layer using threshold
    tmad_thresh = tmad_median_seg < thresh
    cultman_agr_cat_ds = xarray.Dataset({'cultman_agr_cat': (tmad.dims, tmad_thresh)}, coords=tmad.coords)
    
    ########## artsurface/natural ##########
    dc = datacube.Datacube(env='datacube', app="level3")  
    LS = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                       sensors=['ls5','ls7','ls8'],
                                       masked_prop=0.1,
                                       mask_dict=mask_dict)
    
    TCe = TasseledCapTools.pct_exceedance_tasseled_cap(LS, tc_bands=['greenness','brightness', 'wetness'],
                                               greenness_threshold=-500, 
                                               brightness_threshold=5000, 
                                               wetness_threshold=-2500, 
                                               drop=True, drop_tc_bands=True)
    TCe_thresG = TCe.greenness_pct_exceedance < 0.1
    TCe_thresB = TCe.brightness_pct_exceedance < 0.1
    TCe_thresW = TCe.wetness_pct_exceedance > 0.9
    TCE = TCe_thresG | TCe_thresB & TCe_thresW
    urban  = TCE > 0
    artific_urb_cat_ds = urban.to_dataset(name="artific_urb_cat").squeeze()
    
    

    ########## artwater/natural water ##########
    # Load data
    var_file = "/g/data/u46/wofs/confidence/geoFabric/geofabric.tif"
    var_name = "features"

    import_obj = gridded_ingest.LEIngestGDAL(**site['query_geofabric'])
    geofabric = import_obj.read_to_xarray(var_file, var_name)

    # Create binary layer representing artificial water (1) and natural water (0)
    artwatr_wat_cat = ((geofabric["features"] == 1) | (geofabric["features"] == 8))

    # Convert to Dataset and add name
    artwatr_wat_cat_ds = artwatr_wat_cat.to_dataset(name="artwatr_wat_cat")
    
    ########## LCCS classification ##########
    variables_xarray_list = []
    variables_xarray_list.append(vegetat_veg_cat_ds)
    variables_xarray_list.append(aquatic_wat_cat_ds)
    variables_xarray_list.append(cultman_agr_cat_ds)
    variables_xarray_list.append(artific_urb_cat_ds)
    variables_xarray_list.append(artwatr_wat_cat_ds)

    # Merge to a single dataframe
    classification_data = xarray.merge(variables_xarray_list)

    # Apply Level 3 classification using separate function. Works through in three stages
    level1, level2, level3 = lccs_l3.classify_lccs_level3(classification_data)

    # Save classification values back to xarray
    out_class_xarray = xarray.Dataset(
        {"level1" : (classification_data["vegetat_veg_cat"].dims, level1),
         "level2" : (classification_data["vegetat_veg_cat"].dims, level2),
         "level3" : (classification_data["vegetat_veg_cat"].dims, level3)})
    classification_data = xarray.merge([classification_data, out_class_xarray])


    # Write out 
    output_rgb_file_name = 'level3/'+str(site['AOI'])+'_l3_layers_'+str(time[0])+'.tif'
    out_file_transform = [site['query_geofabric']['target_pixel_size_x'], 0, site['query_geofabric']['target_min_x'], 0, site['query_geofabric']['target_pixel_size_y'], site['query_geofabric']['target_max_y']]
    output_x_size = int((site['query_geofabric']['target_max_x'] - site['query_geofabric']['target_min_x'])/site['query_geofabric']['target_pixel_size_x'])
    output_y_size = int((site['query_geofabric']['target_min_y'] - site['query_geofabric']['target_max_y'])/site['query_geofabric']['target_pixel_size_y'])

    # Write
    rgb_dataset = rasterio.open(output_rgb_file_name, 'w', driver='GTiff',
                                height=output_y_size, width=output_x_size,
                                count=5, dtype=level3.dtype,
                                crs=crs, transform=out_file_transform)
    rgb_dataset.write(classification_data.vegetat_veg_cat.values.astype(numpy.uint8), 1)
    rgb_dataset.write(classification_data.aquatic_wat_cat.values.astype(numpy.uint8), 2)
    rgb_dataset.write(classification_data.cultman_agr_cat.values.astype(numpy.uint8), 3)
    rgb_dataset.write(classification_data.artific_urb_cat.values.astype(numpy.uint8), 4)
    rgb_dataset.write(classification_data.artwatr_wat_cat.values.astype(numpy.uint8), 5)
    rgb_dataset.close()

    red, green, blue, alpha = lccs_l3.colour_lccs_level3(level3)
    
    # Write out
    output_rgb_file_name = 'level3/'+str(site['AOI'])+'_l3_'+str(time[0])+'.tif'
    out_file_transform = [site['query_geofabric']['target_pixel_size_x'], 0, site['query_geofabric']['target_min_x'], 0, site['query_geofabric']['target_pixel_size_y'], site['query_geofabric']['target_max_y']]
    output_x_size = int((site['query_geofabric']['target_max_x'] - site['query_geofabric']['target_min_x'])/site['query_geofabric']['target_pixel_size_x'])
    output_y_size = int((site['query_geofabric']['target_min_y'] - site['query_geofabric']['target_max_y'])/site['query_geofabric']['target_pixel_size_y'])

    # Write RGB colour scheme out
    rgb_dataset = rasterio.open(output_rgb_file_name, 'w', driver='GTiff',
                                height=output_y_size, width=output_x_size,
                                count=3, dtype=level3.dtype,
                                crs=crs, transform=out_file_transform)
    rgb_dataset.write(red, 1)
    rgb_dataset.write(green, 2)
    rgb_dataset.write(blue, 3)
    rgb_dataset.close()
    
###################################################################################################################    

# Coastal with mangroves and no artificial
sites = [Leichhardt, Kakadu]


for site in sites:
    ########## veg/non veg ##########
   
    # need to specify as pq not indexed in lccs_dev
    dc = datacube.Datacube(env='datacube', app="level3")
    
    mask_dict = {'cloud_acca': 'no_cloud',
                 'cloud_fmask': 'no_cloud',
                 'contiguous': True}
    # New vegetation classification
    FC = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                           sensors=['ls5','ls7','ls8'],
                                           product = 'fc',
                                           masked_prop=0.1,
                                           mask_dict=mask_dict)

    # Create a mask to show areas where total vegetation is greater than the baresoil fraction of a pixel for each scene
    tv_mask = FC['BS'] < (FC['PV'] + (FC['NPV'] < 90))
    tv = tv_mask.where(FC['BS'] > 0)
    # Calculate proportion of time where total vegetation is greater than the bare soil fraction of a pixel for the input year
    tv_summary = tv.mean(dim='time')
    tv_thres = tv_summary > 0.167
    
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], **site['query'])
    wofs_ann = masking.mask_invalid_data(wofs_ann)    
    aquatic_wat = 1-((wofs_ann["frequency"] >= 0.9))
    tv_thres = tv_thres * aquatic_wat

    vegetat_veg_cat_ds = tv_thres.to_dataset(name="vegetat_veg_cat").squeeze()
    
    # back to lccs_dev
    dc = datacube.Datacube(env='lccs_dev', app="level3")

    
    ########## aquatic/terrestrial ##########
    # Load data from datacube
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], **site['query'])
    wofs_ann = masking.mask_invalid_data(wofs_ann)

    item = dc.load(product="item_v2", measurements=["relative"], 
                         **site['query'])
    item = masking.mask_invalid_data(item)
    item = item.squeeze().drop('time')

    mangrove = dc.load(product="mangrove_extent_cover_albers", measurements=["extent"], 
                         **site['query'])
    mangrove = masking.mask_invalid_data(mangrove)

#     Create binary layer representing aquatic (1) and terrestrial (0)

#     aquatic_wat = ((wofs_ann["frequency"] >= 0.2))

#     For coastal landscapes use the following
    aquatic_wat = ((wofs_ann["frequency"] >= 0.2) | ((item["relative"] >= 2) & (item["relative"] <= 8)) | (mangrove["extent"] == 1))

    # Convert to Dataset and add name
    aquatic_wat_cat_ds = aquatic_wat.to_dataset(name="aquatic_wat_cat").squeeze().drop('time')             

    
    ########## cultman/natural ##########
#     # Load data from datacube
#     mads = dc.load(product=sensor +"_nbart_tmad_annual", **site['query'])
#     mads = masking.mask_invalid_data(mads)

#     # Normalise measurements using log function
#     log_edev = numpy.log(1/mads["edev"])
#     log_sdev = numpy.log(1/mads["sdev"])
#     log_bcdev = numpy.log(1/mads["bcdev"])

#     # Create binary layer representing cultivated (1) and natural (0)
#     cultman = ((log_edev <= 2) & (log_bcdev <= 1.75)) | (log_sdev <= 1.3)
#     cultman  = 1-cultman

#     # Convert to Dataset and add name
#     cultman_agr_cat_ds = cultman.to_dataset(name="cultman_agr_cat").squeeze().drop('time')
    
    # Sean's cultivated
    tmad = dc.load(product=sensor +"_nbart_tmad_annual", **site['query'])
    tmad = tmad.isel(time=0)
    tmad = tmad.drop('time')
    
    # Impute missing values (0 and NaN)
    imp_0 = SimpleImputer(missing_values=0, strategy='mean')
    container = {}
    for key in tmad.data_vars:
        d = tmad[key].data.squeeze()
        d = numpy.nan_to_num(d)
        d = numpy.where(d < 0, 0, d)
        d = numpy.where(d == 1, 0, d)
        imp_0.fit(d)
        d = imp_0.transform(d)
        d = -numpy.log(d)
        container.update({key: d})
    tmad['edev'].data = container['edev']
    tmad['sdev'].data = container['sdev']
    tmad['bcdev'].data = container['bcdev']
    
    # Calculate the mean of all the tmad inputs
    tmad_mean = numpy.mean(numpy.stack([tmad.edev.data, tmad.sdev.data, tmad.bcdev.data], axis=-1), axis=-1)
    # Convert type to float64 (required for quickshift)
    tmad_mean = numpy.float64(tmad_mean)
    # Segment
    tmad_seg = quickshift(tmad_mean, kernel_size=5, convert2lab=False, max_dist=500, ratio=0.5)
    # Calculate the median for each segment
    tmad_median_seg = scipy.ndimage.median(input=tmad_mean, labels=tmad_seg, index=tmad_seg)
    # Set threshold as 10th percentile of mean TMAD
    thresh = numpy.percentile(tmad_mean.ravel(), 45)
    # Create boolean layer using threshold
    tmad_thresh = tmad_median_seg < thresh
    cultman_agr_cat_ds = xarray.Dataset({'cultman_agr_cat': (tmad.dims, tmad_thresh)}, coords=tmad.coords)
    
    ########## artsurface/natural ##########
    dc = datacube.Datacube(env='datacube', app="level3")  
    LS = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                       sensors=['ls5','ls7','ls8'],
                                       masked_prop=0.1,
                                       mask_dict=mask_dict)
    
    TCe = TasseledCapTools.pct_exceedance_tasseled_cap(LS, tc_bands=['greenness','brightness', 'wetness'],
                                               greenness_threshold=-500, 
                                               brightness_threshold=5000, 
                                               wetness_threshold=-2500, 
                                               drop=True, drop_tc_bands=True)
    TCe_thresG = TCe.greenness_pct_exceedance < 0.1
    TCe_thresB = TCe.brightness_pct_exceedance < 0.1
    TCe_thresW = TCe.wetness_pct_exceedance > 0.9
    TCE = TCe_thresG | TCe_thresB & TCe_thresW
    urban  = TCE * 0
    artific_urb_cat_ds = urban.to_dataset(name="artific_urb_cat").squeeze()
    

    ########## artwater/natural water ##########
    # Load data
    var_file = "/g/data/u46/wofs/confidence/geoFabric/geofabric.tif"
    var_name = "features"

    import_obj = gridded_ingest.LEIngestGDAL(**site['query_geofabric'])
    geofabric = import_obj.read_to_xarray(var_file, var_name)

    # Create binary layer representing artificial water (1) and natural water (0)
    artwatr_wat_cat = ((geofabric["features"] == 1) | (geofabric["features"] == 8))

    # Convert to Dataset and add name
    artwatr_wat_cat_ds = artwatr_wat_cat.to_dataset(name="artwatr_wat_cat")
    
    ########## LCCS classification ##########
    variables_xarray_list = []
    variables_xarray_list.append(vegetat_veg_cat_ds)
    variables_xarray_list.append(aquatic_wat_cat_ds)
    variables_xarray_list.append(cultman_agr_cat_ds)
    variables_xarray_list.append(artific_urb_cat_ds)
    variables_xarray_list.append(artwatr_wat_cat_ds)

    # Merge to a single dataframe
    classification_data = xarray.merge(variables_xarray_list)

    # Apply Level 3 classification using separate function. Works through in three stages
    level1, level2, level3 = lccs_l3.classify_lccs_level3(classification_data)

    # Save classification values back to xarray
    out_class_xarray = xarray.Dataset(
        {"level1" : (classification_data["vegetat_veg_cat"].dims, level1),
         "level2" : (classification_data["vegetat_veg_cat"].dims, level2),
         "level3" : (classification_data["vegetat_veg_cat"].dims, level3)})
    classification_data = xarray.merge([classification_data, out_class_xarray])


    # Write out 
    output_rgb_file_name = 'level3/'+str(site['AOI'])+'_l3_layers_'+str(time[0])+'.tif'
    out_file_transform = [site['query_geofabric']['target_pixel_size_x'], 0, site['query_geofabric']['target_min_x'], 0, site['query_geofabric']['target_pixel_size_y'], site['query_geofabric']['target_max_y']]
    output_x_size = int((site['query_geofabric']['target_max_x'] - site['query_geofabric']['target_min_x'])/site['query_geofabric']['target_pixel_size_x'])
    output_y_size = int((site['query_geofabric']['target_min_y'] - site['query_geofabric']['target_max_y'])/site['query_geofabric']['target_pixel_size_y'])

    # Write
    rgb_dataset = rasterio.open(output_rgb_file_name, 'w', driver='GTiff',
                                height=output_y_size, width=output_x_size,
                                count=5, dtype=level3.dtype,
                                crs=crs, transform=out_file_transform)
    rgb_dataset.write(classification_data.vegetat_veg_cat.values.astype(numpy.uint8), 1)
    rgb_dataset.write(classification_data.aquatic_wat_cat.values.astype(numpy.uint8), 2)
    rgb_dataset.write(classification_data.cultman_agr_cat.values.astype(numpy.uint8), 3)
    rgb_dataset.write(classification_data.artific_urb_cat.values.astype(numpy.uint8), 4)
    rgb_dataset.write(classification_data.artwatr_wat_cat.values.astype(numpy.uint8), 5)
    rgb_dataset.close()

    red, green, blue, alpha = lccs_l3.colour_lccs_level3(level3)
    
    # Write out
    output_rgb_file_name = 'level3/'+str(site['AOI'])+'_l3_'+str(time[0])+'.tif'
    out_file_transform = [site['query_geofabric']['target_pixel_size_x'], 0, site['query_geofabric']['target_min_x'], 0, site['query_geofabric']['target_pixel_size_y'], site['query_geofabric']['target_max_y']]
    output_x_size = int((site['query_geofabric']['target_max_x'] - site['query_geofabric']['target_min_x'])/site['query_geofabric']['target_pixel_size_x'])
    output_y_size = int((site['query_geofabric']['target_min_y'] - site['query_geofabric']['target_max_y'])/site['query_geofabric']['target_pixel_size_y'])

    # Write RGB colour scheme out
    rgb_dataset = rasterio.open(output_rgb_file_name, 'w', driver='GTiff',
                                height=output_y_size, width=output_x_size,
                                count=3, dtype=level3.dtype,
                                crs=crs, transform=out_file_transform)
    rgb_dataset.write(red, 1)
    rgb_dataset.write(green, 2)
    rgb_dataset.write(blue, 3)
    rgb_dataset.close()
    
###################################################################################################################    

# Coastal without mangroves but ITEM
sites = [Hobart, Perth, Coorong]
  

for site in sites:
    ########## veg/non veg ##########
   
    # need to specify as pq not indexed in lccs_dev
    dc = datacube.Datacube(env='datacube', app="level3")
    
    mask_dict = {'cloud_acca': 'no_cloud',
                 'cloud_fmask': 'no_cloud',
                 'contiguous': True}
    # New vegetation classification
    FC = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                           sensors=['ls5','ls7','ls8'],
                                           product = 'fc',
                                           masked_prop=0.1,
                                           mask_dict=mask_dict)

    # Create a mask to show areas where total vegetation is greater than the baresoil fraction of a pixel for each scene
    tv_mask = FC['BS'] < (FC['PV'] + (FC['NPV'] < 90))
    tv = tv_mask.where(FC['BS'] > 0)
    # Calculate proportion of time where total vegetation is greater than the bare soil fraction of a pixel for the input year
    tv_summary = tv.mean(dim='time')
    tv_thres = tv_summary > 0.167
    
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], **site['query'])
    wofs_ann = masking.mask_invalid_data(wofs_ann)    
    aquatic_wat = 1-((wofs_ann["frequency"] >= 0.9))
    tv_thres = tv_thres * aquatic_wat

    vegetat_veg_cat_ds = tv_thres.to_dataset(name="vegetat_veg_cat").squeeze()
    
    # back to lccs_dev
    dc = datacube.Datacube(env='lccs_dev', app="level3")

    
    ########## aquatic/terrestrial ##########
    # Load data from datacube
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], **site['query'])
    wofs_ann = masking.mask_invalid_data(wofs_ann)

    item = dc.load(product="item_v2", measurements=["relative"], 
                         **site['query'])
    item = masking.mask_invalid_data(item)
    item = item.squeeze().drop('time')

#     mangrove = dc.load(product="mangrove_extent_cover_albers", measurements=["extent"], 
#                          **site['query'])
#     mangrove = masking.mask_invalid_data(mangrove)

#     Create binary layer representing aquatic (1) and terrestrial (0)

#     aquatic_wat = ((wofs_ann["frequency"] >= 0.2))

#     For coastal landscapes use the following
    aquatic_wat = ((wofs_ann["frequency"] >= 0.2) | ((item["relative"] >= 2) & (item["relative"] <= 8))) # | (mangrove["extent"] == 1))

    # Convert to Dataset and add name
    aquatic_wat_cat_ds = aquatic_wat.to_dataset(name="aquatic_wat_cat").squeeze().drop('time')             

    
    ########## cultman/natural ##########
#     # Load data from datacube
#     mads = dc.load(product=sensor +"_nbart_tmad_annual", **site['query'])
#     mads = masking.mask_invalid_data(mads)

#     # Normalise measurements using log function
#     log_edev = numpy.log(1/mads["edev"])
#     log_sdev = numpy.log(1/mads["sdev"])
#     log_bcdev = numpy.log(1/mads["bcdev"])

#     # Create binary layer representing cultivated (1) and natural (0)
#     cultman = ((log_edev <= 2) & (log_bcdev <= 1.75)) | (log_sdev <= 1.3)
#     cultman  = 1-cultman

#     # Convert to Dataset and add name
#     cultman_agr_cat_ds = cultman.to_dataset(name="cultman_agr_cat").squeeze().drop('time')
    
    # Sean's cultivated
    tmad = dc.load(product=sensor +"_nbart_tmad_annual", **site['query'])
    tmad = tmad.isel(time=0)
    tmad = tmad.drop('time')
    
    # Impute missing values (0 and NaN)
    imp_0 = SimpleImputer(missing_values=0, strategy='mean')
    container = {}
    for key in tmad.data_vars:
        d = tmad[key].data.squeeze()
        d = numpy.nan_to_num(d)
        d = numpy.where(d < 0, 0, d)
        d = numpy.where(d == 1, 0, d)
        imp_0.fit(d)
        d = imp_0.transform(d)
        d = -numpy.log(d)
        container.update({key: d})
    tmad['edev'].data = container['edev']
    tmad['sdev'].data = container['sdev']
    tmad['bcdev'].data = container['bcdev']
    
    # Calculate the mean of all the tmad inputs
    tmad_mean = numpy.mean(numpy.stack([tmad.edev.data, tmad.sdev.data, tmad.bcdev.data], axis=-1), axis=-1)
    # Convert type to float64 (required for quickshift)
    tmad_mean = numpy.float64(tmad_mean)
    # Segment
    tmad_seg = quickshift(tmad_mean, kernel_size=5, convert2lab=False, max_dist=500, ratio=0.5)
    # Calculate the median for each segment
    tmad_median_seg = scipy.ndimage.median(input=tmad_mean, labels=tmad_seg, index=tmad_seg)
    # Set threshold as 10th percentile of mean TMAD
    thresh = numpy.percentile(tmad_mean.ravel(), 20)
    # Create boolean layer using threshold
    tmad_thresh = tmad_median_seg < thresh
    cultman_agr_cat_ds = xarray.Dataset({'cultman_agr_cat': (tmad.dims, tmad_thresh)}, coords=tmad.coords)
    
    ########## artsurface/natural ##########
    dc = datacube.Datacube(env='datacube', app="level3")  
    LS = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                       sensors=['ls5','ls7','ls8'],
                                       masked_prop=0.1,
                                       mask_dict=mask_dict)
    
    TCe = TasseledCapTools.pct_exceedance_tasseled_cap(LS, tc_bands=['greenness','brightness', 'wetness'],
                                               greenness_threshold=-500, 
                                               brightness_threshold=5000, 
                                               wetness_threshold=-2500, 
                                               drop=True, drop_tc_bands=True)
    TCe_thresG = TCe.greenness_pct_exceedance < 0.1
    TCe_thresB = TCe.brightness_pct_exceedance < 0.1
    TCe_thresW = TCe.wetness_pct_exceedance > 0.9
    TCE = TCe_thresG | TCe_thresB & TCe_thresW
    urban  = TCE > 0
    artific_urb_cat_ds = urban.to_dataset(name="artific_urb_cat").squeeze()
    
    

    ########## artwater/natural water ##########
    # Load data
    var_file = "/g/data/u46/wofs/confidence/geoFabric/geofabric.tif"
    var_name = "features"

    import_obj = gridded_ingest.LEIngestGDAL(**site['query_geofabric'])
    geofabric = import_obj.read_to_xarray(var_file, var_name)

    # Create binary layer representing artificial water (1) and natural water (0)
    artwatr_wat_cat = ((geofabric["features"] == 1) | (geofabric["features"] == 8))

    # Convert to Dataset and add name
    artwatr_wat_cat_ds = artwatr_wat_cat.to_dataset(name="artwatr_wat_cat")
    
    ########## LCCS classification ##########
    variables_xarray_list = []
    variables_xarray_list.append(vegetat_veg_cat_ds)
    variables_xarray_list.append(aquatic_wat_cat_ds)
    variables_xarray_list.append(cultman_agr_cat_ds)
    variables_xarray_list.append(artific_urb_cat_ds)
    variables_xarray_list.append(artwatr_wat_cat_ds)

    # Merge to a single dataframe
    classification_data = xarray.merge(variables_xarray_list)

    # Apply Level 3 classification using separate function. Works through in three stages
    level1, level2, level3 = lccs_l3.classify_lccs_level3(classification_data)

    # Save classification values back to xarray
    out_class_xarray = xarray.Dataset(
        {"level1" : (classification_data["vegetat_veg_cat"].dims, level1),
         "level2" : (classification_data["vegetat_veg_cat"].dims, level2),
         "level3" : (classification_data["vegetat_veg_cat"].dims, level3)})
    classification_data = xarray.merge([classification_data, out_class_xarray])


    # Write out 
    output_rgb_file_name = 'level3/'+str(site['AOI'])+'_l3_layers_'+str(time[0])+'.tif'
    out_file_transform = [site['query_geofabric']['target_pixel_size_x'], 0, site['query_geofabric']['target_min_x'], 0, site['query_geofabric']['target_pixel_size_y'], site['query_geofabric']['target_max_y']]
    output_x_size = int((site['query_geofabric']['target_max_x'] - site['query_geofabric']['target_min_x'])/site['query_geofabric']['target_pixel_size_x'])
    output_y_size = int((site['query_geofabric']['target_min_y'] - site['query_geofabric']['target_max_y'])/site['query_geofabric']['target_pixel_size_y'])

    # Write
    rgb_dataset = rasterio.open(output_rgb_file_name, 'w', driver='GTiff',
                                height=output_y_size, width=output_x_size,
                                count=5, dtype=level3.dtype,
                                crs=crs, transform=out_file_transform)
    rgb_dataset.write(classification_data.vegetat_veg_cat.values.astype(numpy.uint8), 1)
    rgb_dataset.write(classification_data.aquatic_wat_cat.values.astype(numpy.uint8), 2)
    rgb_dataset.write(classification_data.cultman_agr_cat.values.astype(numpy.uint8), 3)
    rgb_dataset.write(classification_data.artific_urb_cat.values.astype(numpy.uint8), 4)
    rgb_dataset.write(classification_data.artwatr_wat_cat.values.astype(numpy.uint8), 5)
    rgb_dataset.close()

    red, green, blue, alpha = lccs_l3.colour_lccs_level3(level3)
    
    # Write out
    output_rgb_file_name = 'level3/'+str(site['AOI'])+'_l3_'+str(time[0])+'.tif'
    out_file_transform = [site['query_geofabric']['target_pixel_size_x'], 0, site['query_geofabric']['target_min_x'], 0, site['query_geofabric']['target_pixel_size_y'], site['query_geofabric']['target_max_y']]
    output_x_size = int((site['query_geofabric']['target_max_x'] - site['query_geofabric']['target_min_x'])/site['query_geofabric']['target_pixel_size_x'])
    output_y_size = int((site['query_geofabric']['target_min_y'] - site['query_geofabric']['target_max_y'])/site['query_geofabric']['target_pixel_size_y'])

    # Write RGB colour scheme out
    rgb_dataset = rasterio.open(output_rgb_file_name, 'w', driver='GTiff',
                                height=output_y_size, width=output_x_size,
                                count=3, dtype=level3.dtype,
                                crs=crs, transform=out_file_transform)
    rgb_dataset.write(red, 1)
    rgb_dataset.write(green, 2)
    rgb_dataset.write(blue, 3)
    rgb_dataset.close()
    
###################################################################################################################    

# No artificial
sites = [Diamantina, Gwydir, Lake_Eyre, Collier_Range, Mt_Ney]

for site in sites:
    ########## veg/non veg ##########
   
    # need to specify as pq not indexed in lccs_dev
    dc = datacube.Datacube(env='datacube', app="level3")
    
    mask_dict = {'cloud_acca': 'no_cloud',
                 'cloud_fmask': 'no_cloud',
                 'contiguous': True}
    # New vegetation classification
    FC = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                           sensors=['ls5','ls7','ls8'],
                                           product = 'fc',
                                           masked_prop=0.1,
                                           mask_dict=mask_dict)

    # Create a mask to show areas where total vegetation is greater than the baresoil fraction of a pixel for each scene
    tv_mask = FC['BS'] < (FC['PV'] + (FC['NPV'] < 90))
    tv = tv_mask.where(FC['BS'] > 0)
    # Calculate proportion of time where total vegetation is greater than the bare soil fraction of a pixel for the input year
    tv_summary = tv.mean(dim='time')
    tv_thres = tv_summary > 0.167
    
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], **site['query'])
    wofs_ann = masking.mask_invalid_data(wofs_ann)    
    aquatic_wat = 1-((wofs_ann["frequency"] >= 0.9))
    tv_thres = tv_thres * aquatic_wat

    vegetat_veg_cat_ds = tv_thres.to_dataset(name="vegetat_veg_cat").squeeze()
    
    # back to lccs_dev
    dc = datacube.Datacube(env='lccs_dev', app="level3")

    
    ########## aquatic/terrestrial ##########
    # Load data from datacube
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], **site['query'])
    wofs_ann = masking.mask_invalid_data(wofs_ann)

#     item = dc.load(product="item_v2", measurements=["relative"], 
#                          **site['query'])
#     item = masking.mask_invalid_data(item)
#     item = item.squeeze().drop('time')

#     mangrove = dc.load(product="mangrove_extent_cover_albers", measurements=["extent"], 
#                          **site['query'])
#     mangrove = masking.mask_invalid_data(mangrove)

#     Create binary layer representing aquatic (1) and terrestrial (0)

    aquatic_wat = ((wofs_ann["frequency"] >= 0.2))

#     For coastal landscapes use the following
#     aquatic_wat = ((wofs_ann["frequency"] >= 0.2) | ((item["relative"] >= 2) & (item["relative"] <= 8)) | (mangrove["extent"] == 1))

    # Convert to Dataset and add name
    aquatic_wat_cat_ds = aquatic_wat.to_dataset(name="aquatic_wat_cat").squeeze().drop('time')             

    
    ########## cultman/natural ##########
#     # Load data from datacube
#     mads = dc.load(product=sensor +"_nbart_tmad_annual", **site['query'])
#     mads = masking.mask_invalid_data(mads)

#     # Normalise measurements using log function
#     log_edev = numpy.log(1/mads["edev"])
#     log_sdev = numpy.log(1/mads["sdev"])
#     log_bcdev = numpy.log(1/mads["bcdev"])

#     # Create binary layer representing cultivated (1) and natural (0)
#     cultman = ((log_edev <= 2) & (log_bcdev <= 1.75)) | (log_sdev <= 1.3)
#     cultman  = 1-cultman

#     # Convert to Dataset and add name
#     cultman_agr_cat_ds = cultman.to_dataset(name="cultman_agr_cat").squeeze().drop('time')
    
    # Sean's cultivated
    tmad = dc.load(product=sensor +"_nbart_tmad_annual", **site['query'])
    tmad = tmad.isel(time=0)
    tmad = tmad.drop('time')
    
    # Impute missing values (0 and NaN)
    imp_0 = SimpleImputer(missing_values=0, strategy='mean')
    container = {}
    for key in tmad.data_vars:
        d = tmad[key].data.squeeze()
        d = numpy.nan_to_num(d)
        d = numpy.where(d < 0, 0, d)
        d = numpy.where(d == 1, 0, d)
        imp_0.fit(d)
        d = imp_0.transform(d)
        d = -numpy.log(d)
        container.update({key: d})
    tmad['edev'].data = container['edev']
    tmad['sdev'].data = container['sdev']
    tmad['bcdev'].data = container['bcdev']
    
    # Calculate the mean of all the tmad inputs
    tmad_mean = numpy.mean(numpy.stack([tmad.edev.data, tmad.sdev.data, tmad.bcdev.data], axis=-1), axis=-1)
    # Convert type to float64 (required for quickshift)
    tmad_mean = numpy.float64(tmad_mean)
    # Segment
    tmad_seg = quickshift(tmad_mean, kernel_size=5, convert2lab=False, max_dist=500, ratio=0.5)
    # Calculate the median for each segment
    tmad_median_seg = scipy.ndimage.median(input=tmad_mean, labels=tmad_seg, index=tmad_seg)
    # Set threshold as 10th percentile of mean TMAD
    thresh = numpy.percentile(tmad_mean.ravel(), 45)
    # Create boolean layer using threshold
    tmad_thresh = tmad_median_seg < thresh
    cultman_agr_cat_ds = xarray.Dataset({'cultman_agr_cat': (tmad.dims, tmad_thresh)}, coords=tmad.coords)
    
    ########## artsurface/natural ##########
    dc = datacube.Datacube(env='datacube', app="level3")  
    LS = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                       sensors=['ls5','ls7','ls8'],
                                       masked_prop=0.1,
                                       mask_dict=mask_dict)
    
    TCe = TasseledCapTools.pct_exceedance_tasseled_cap(LS, tc_bands=['greenness','brightness', 'wetness'],
                                               greenness_threshold=-500, 
                                               brightness_threshold=5000, 
                                               wetness_threshold=-2500, 
                                               drop=True, drop_tc_bands=True)
    TCe_thresG = TCe.greenness_pct_exceedance < 0.1
    TCe_thresB = TCe.brightness_pct_exceedance < 0.1
    TCe_thresW = TCe.wetness_pct_exceedance > 0.9
    TCE = TCe_thresG | TCe_thresB & TCe_thresW
    urban  = TCE * 0
    artific_urb_cat_ds = urban.to_dataset(name="artific_urb_cat").squeeze()
    
    

    ########## artwater/natural water ##########
    # Load data
    var_file = "/g/data/u46/wofs/confidence/geoFabric/geofabric.tif"
    var_name = "features"

    import_obj = gridded_ingest.LEIngestGDAL(**site['query_geofabric'])
    geofabric = import_obj.read_to_xarray(var_file, var_name)

    # Create binary layer representing artificial water (1) and natural water (0)
    artwatr_wat_cat = ((geofabric["features"] == 1) | (geofabric["features"] == 8))

    # Convert to Dataset and add name
    artwatr_wat_cat_ds = artwatr_wat_cat.to_dataset(name="artwatr_wat_cat")
    
    ########## LCCS classification ##########
    variables_xarray_list = []
    variables_xarray_list.append(vegetat_veg_cat_ds)
    variables_xarray_list.append(aquatic_wat_cat_ds)
    variables_xarray_list.append(cultman_agr_cat_ds)
    variables_xarray_list.append(artific_urb_cat_ds)
    variables_xarray_list.append(artwatr_wat_cat_ds)

    # Merge to a single dataframe
    classification_data = xarray.merge(variables_xarray_list)

    # Apply Level 3 classification using separate function. Works through in three stages
    level1, level2, level3 = lccs_l3.classify_lccs_level3(classification_data)

    # Save classification values back to xarray
    out_class_xarray = xarray.Dataset(
        {"level1" : (classification_data["vegetat_veg_cat"].dims, level1),
         "level2" : (classification_data["vegetat_veg_cat"].dims, level2),
         "level3" : (classification_data["vegetat_veg_cat"].dims, level3)})
    classification_data = xarray.merge([classification_data, out_class_xarray])


    # Write out 
    output_rgb_file_name = 'level3/'+str(site['AOI'])+'_l3_layers_'+str(time[0])+'.tif'
    out_file_transform = [site['query_geofabric']['target_pixel_size_x'], 0, site['query_geofabric']['target_min_x'], 0, site['query_geofabric']['target_pixel_size_y'], site['query_geofabric']['target_max_y']]
    output_x_size = int((site['query_geofabric']['target_max_x'] - site['query_geofabric']['target_min_x'])/site['query_geofabric']['target_pixel_size_x'])
    output_y_size = int((site['query_geofabric']['target_min_y'] - site['query_geofabric']['target_max_y'])/site['query_geofabric']['target_pixel_size_y'])

    # Write
    rgb_dataset = rasterio.open(output_rgb_file_name, 'w', driver='GTiff',
                                height=output_y_size, width=output_x_size,
                                count=5, dtype=level3.dtype,
                                crs=crs, transform=out_file_transform)
    rgb_dataset.write(classification_data.vegetat_veg_cat.values.astype(numpy.uint8), 1)
    rgb_dataset.write(classification_data.aquatic_wat_cat.values.astype(numpy.uint8), 2)
    rgb_dataset.write(classification_data.cultman_agr_cat.values.astype(numpy.uint8), 3)
    rgb_dataset.write(classification_data.artific_urb_cat.values.astype(numpy.uint8), 4)
    rgb_dataset.write(classification_data.artwatr_wat_cat.values.astype(numpy.uint8), 5)
    rgb_dataset.close()

    red, green, blue, alpha = lccs_l3.colour_lccs_level3(level3)
    
    # Write out
    output_rgb_file_name = 'level3/'+str(site['AOI'])+'_l3_'+str(time[0])+'.tif'
    out_file_transform = [site['query_geofabric']['target_pixel_size_x'], 0, site['query_geofabric']['target_min_x'], 0, site['query_geofabric']['target_pixel_size_y'], site['query_geofabric']['target_max_y']]
    output_x_size = int((site['query_geofabric']['target_max_x'] - site['query_geofabric']['target_min_x'])/site['query_geofabric']['target_pixel_size_x'])
    output_y_size = int((site['query_geofabric']['target_min_y'] - site['query_geofabric']['target_max_y'])/site['query_geofabric']['target_pixel_size_y'])

    # Write RGB colour scheme out
    rgb_dataset = rasterio.open(output_rgb_file_name, 'w', driver='GTiff',
                                height=output_y_size, width=output_x_size,
                                count=3, dtype=level3.dtype,
                                crs=crs, transform=out_file_transform)
    rgb_dataset.write(red, 1)
    rgb_dataset.write(green, 2)
    rgb_dataset.write(blue, 3)
    rgb_dataset.close()