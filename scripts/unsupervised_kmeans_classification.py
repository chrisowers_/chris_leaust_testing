'''

Description: A script to perform an unsupervised pixel-based classification using the sklearn MiniBatchKMeans classifier.
The image is classified block-by-block to avoid memory errors associated with very large rasters.
Training pixels are randomly selected from each block, thereby ensuring that the training data is evenly distributed throughout the scene.

Osian Roberts, Chris Owers July 2019
Aberystwyth University


'''

import sys
import numpy as np
from osgeo import gdal
from sklearn.cluster import MiniBatchKMeans

def ProgressBar(n_tasks, progress):
    '''
    A function to display a progress bar on an unix terminal.
    '''
    barLength, status = 50, ''
    progress = float(progress) / float(n_tasks)
    if progress >= 1.0:
        progress = 1
        status = 'Done. \n'
    block = round(barLength * progress)
    text = '\r{} {:.0f}% {}'.format('#' * block + '-' * (barLength - block), round(progress * 100, 0), status)
    sys.stdout.write(text)
    sys.stdout.flush()


def GetImageBlocks(xSize, ySize, xBlocksize, yBlocksize):
    ''' A function to calculate image block coordinates and dimensions.

    Input parameters:
    xSize = The number of pixels along the image x axis.
    ySize = The number of pixels along the image y axis.
    xBlocksize = Image block size along the x axis.
    yBlocksize = Image block size along the y axis.

    Returns sub-lists containing: [xBlock starting index, xBlock starting index, n_Columns within block, n_Rows within block]
    '''
    BlockInfo = []	

    for yBlock in range(0, ySize, yBlocksize):
        if yBlock + yBlocksize < ySize:
            Rows = yBlocksize
        else:
            Rows = ySize - yBlock

        for xBlock in range(0, xSize, xBlocksize):
            if xBlock + xBlocksize < xSize:
                Cols = xBlocksize
            else:
                Cols = xSize - xBlock

            BlockInfo.append([xBlock, yBlock, Cols, Rows])
    del xBlock, yBlock, Cols, Rows
    return BlockInfo


def ClassifyImage(InputImage, OutputImage, GDALformat, SpectralClasses, SampleSize):
    ''' A function to classify an image in blocks using the k-means unsupervised classifier.'''
    # Define the classifier
    clf = MiniBatchKMeans(n_clusters=SpectralClasses, init='k-means++', max_iter=20, batch_size=1000, verbose=0, compute_labels=True, random_state=None, tol=0.0, max_no_improvement=100, init_size=10000, n_init=10, reassignment_ratio=0.05)

    print('Performing K-means unsupervised classification with '+str(SpectralClasses)+' spectral classes...')
    # Read the input image:
    Image = gdal.Open(InputImage, 0)
    RasterBands = Image.RasterCount
    SRS = Image.GetProjection()
    GeoT = Image.GetGeoTransform()
    xSize, ySize = Image.RasterXSize, Image.RasterYSize
    BlockSize = Image.GetRasterBand(1).GetBlockSize()
    NoDataValue = Image.GetRasterBand(1).GetNoDataValue()

    # Create the output raster:
    Driver = gdal.GetDriverByName(GDALformat)
    if GDALformat == 'KEA':
        OutputRaster = Driver.Create(OutputImage, xSize, ySize, 1, 1)
    elif GDALformat == 'GTiff':
        OutputRaster = Driver.Create(OutputImage, xSize, ySize, 1, 1, options=['COMPRESS=DEFLATE'])
    OutputRaster.SetProjection(SRS)
    OutputRaster.SetGeoTransform(GeoT) 
    OutBand = OutputRaster.GetRasterBand(1)
    OutBand.SetNoDataValue(0)
    del SRS, GeoT

    # Get the extent of the image blocks:
    RasterBlocks = GetImageBlocks(xSize, ySize, BlockSize[0], BlockSize[1])
    n_blocks = len(RasterBlocks)

    # Perform simple classification if the image is not comprised of more than 1 raster block:
    if n_blocks <= 1:
        ImageData = []
        print('Extracting training data...')
        # iterate over each raster band:
        for Band in range(1, RasterBands+1):
            BandData = Image.GetRasterBand(Band).ReadAsArray()

            if Band == 1: # Create a binary mask for valid pixels using the first raster band.
                BinaryMask = np.ones_like(BandData, dtype='uint8')
                BinaryMask = np.where(BandData == NoDataValue, 0, BinaryMask)

            BandData = np.ma.compressed(np.ma.masked_equal(BandData, NoDataValue))
            if len(BandData) != 0:
                ImageData.append(BandData)
            del BandData

        ImageData = np.array(ImageData).T
        TrainingData = ImageData[np.random.choice(ImageData.shape[0], size=round(ImageData.shape[0]*SampleSize), replace=False), :] # Sample the pixels without replacement.

        # fit the training data
        print('Training the classifier using '+str(len(TrainingData))+' pixels...')
        clf.fit(TrainingData, y=None)
        del TrainingData

        # generate predictions
        print('Performing classification...')
        PredClass = clf.predict(ImageData) + 1 # Add 1 to avoid having a class value == 0.
        del ImageData

        # reshape the data into a 2d array and then mask the invalid pixels:
        PredClass = np.reshape(PredClass, (ySize, xSize)) * BinaryMask

        # write classification to the output raster:
        OutBand.WriteArray(PredClass)
        del PredClass, BinaryMask
        print('Done.')

    else:
        # iterate over each raster block and obtain a sample of the pixels for classifier training:
        TrainingData = []
        print('Extracting training data from each image block...')
        for idx, Block in enumerate(RasterBlocks):
            BlockData = []

            # iterate over each raster band
            for Band in range(1, RasterBands+1):
                BandData = Image.GetRasterBand(Band).ReadAsArray(Block[0], Block[1], Block[2], Block[3])
                BandData = np.ma.compressed(np.ma.masked_equal(BandData, NoDataValue))
                if BandData.any():
	                BlockData.append(BandData)
                del BandData

            if len(BlockData) != 0:
                BlockData = np.array(BlockData).T
                BlockData = BlockData[np.random.choice(BlockData.shape[0], size=round(BlockData.shape[0]*SampleSize), replace=False)] # Sample pixels without replacement.
                if BlockData.any():         
                    TrainingData.append(BlockData)
                del BlockData
            ProgressBar(n_blocks, idx+1)

        # fit the training data:
        TrainingData = np.concatenate(TrainingData)
        TrainingData = np.nan_to_num(TrainingData)

        print('Training the classifier using '+str(len(TrainingData))+' pixels...')
        clf.fit(TrainingData, y=None)
        del TrainingData

        # Read the input image in blocks and perform classification
        print('Classifying '+str(len(RasterBlocks))+' blocks...')
        for idx, Block in enumerate(RasterBlocks):
            TestData = []

            for Band in range(RasterBands):
                Band += 1
                BandData = Image.GetRasterBand(Band).ReadAsArray(int(Block[0]), int(Block[1]), int(Block[2]), int(Block[3]))

                if Band == 1: # Create a binary mask for 
                    BinaryMask = np.ones_like(BandData, dtype='uint8')
                    BinaryMask = np.where(BandData == NoDataValue, 0, BinaryMask)

                TestData.append(BandData.flatten())
                del BandData

            # Perform classification
            TestData = np.array(TestData).T
            TestData = np.nan_to_num(TestData)
            PredClass = clf.predict(TestData) + 1 # Add 1 to avoid having a class value = 0.
            del TestData

            # reshape the data into a 2d array and then mask the invalid pixels:
            PredClass = np.reshape(PredClass, (int(Block[3]), int(Block[2]))) * BinaryMask

            # write classification to the output raster
            OutBand.WriteArray(PredClass, int(Block[0]), int(Block[1]))
            del PredClass, BinaryMask

            # print progress
            ProgressBar(n_blocks, idx+1)

        del RasterBlocks

    # Close the input and output datasets:
    del Image, OutputRaster, OutBand, clf

    # Build image overviews for faster viewing in external software (e.g. QGIS or Tuiview)
    print('Generating image overviews...')
    try:
        from rsgislib import imageutils
        imageutils.popImageStats(OutputImage, True, 0, True)
    except Exception:
        im = gdal.Open(OutputImage, 1) # 0 = external overviews for geotiff, 1 = internal overviews for geotiff.
        gdal.SetConfigOption('COMPRESS_OVERVIEW', 'DEFLATE')
        im.BuildOverviews('NEAREST', [2,4,8,16,32,64])
        del im
        print('Done.')
