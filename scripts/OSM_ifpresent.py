'''

This workflow is designed to test if open street map (OSM) data is within area of interest (AOI)
All OSM data must be in correct crs as input bounding box, and in meters (i.e. EPSG:3577)
This workflow is intented to be used with a kmeans classifier if the statement is found to be true

Basic function
- checks if OSM buildings, roads and railways are in AOI
- if yes, buffers roads and railways by 3m
- get area of OSM data
- check if % OSM data in AOI is >1%
- if no, classify all LCCS non-vegetated terrestrial as natural surface
if yes, run kmeans classifier (another script)

The idea is to have a pragmatic, tile by tile, approach to classifying areas unlikely to have artificial
surface in Australia

Obvious caveats for LCCS
- OSM is an incomplete dataset
- as OSM is a static dataset, we will only be able to detect artificial surface change for tiles
  where artificial surfaces are currently in the OSM. This is probably ok at a continental scale
  as most increase in artificial surface over the last 30 years has been urban expansion.
- End result is that some artificial surface will be assumed as natural surface in areas where
  very little artificial surface is present

To consider
- how to implement this for continental scale? the final if statement to produce a binary mask for each tile?
- what threshold to set for artificial surface presence that then is considered for kmeans?

Chris Owers July 2019
Aberystwyth University

'''

import geopandas as gp
import os
from shapely.geometry import Polygon
from rsgislib import vectorutils


# AOI (set bounding box and crs that is in meters)

AOIname = 'Ayr'
xmin = 1500000
xmax = 1600000
ymin = -2200000
ymax = -2100000
EPSG = 3577

# AOIname = 'Diamantina'
# xmin = 800000
# xmax = 900000
# ymin = -2800000
# ymax = -2700000
# EPSG = 3577

# AOIname = 'Gwydir'
# xmin = 1600000
# xmax = 1700000
# ymin = -3400000
# ymax = -3300000
# EPSG = 3577

# AOIname = 'Leichhardt'
# xmin = 800000
# xmax = 900000
# ymin = -2000000
# ymax = -1900000
# EPSG = 3577

# AOIname = 'Kakadu'
# xmin = 0
# xmax = 100000
# ymin = -1350000
# ymax = -1250000
# EPSG = 3577

# AOIname = 'Hobart'
# xmin = 1200000
# xmax = 1300000
# ymin = -4800000
# ymax = -4700000
# EPSG = 3577

# AOIname = 'Perth'
# xmin = -1550000
# xmax = -1450000
# ymin = -3650000
# ymax = -3550000
# EPSG = 3577

# AOIname = 'MurrayValley'
# xmin = 1100000
# xmax = 1200000
# ymin = -4000000
# ymax = -3900000
# EPSG = 3577

# AOIname = 'Adelaide'
# xmin = 550000
# xmax = 650000
# ymin = -3850000
# ymax = -3750000
# EPSG = 3577

# AOIname = 'LakeEyre'
# xmin = 500000
# xmax = 600000
# ymin = -3000000
# ymax = -2900000
# EPSG = 3577

# AOIname = 'BlueMtns'
# xmin = 1600000
# xmax = 1700000
# ymin = -3900000
# ymax = -3800000
# EPSG = 3577

# AOIname = 'AustAlps'
# xmin = 1400000
# xmax = 1500000
# ymin = -4100000
# ymax = -4000000
# EPSG = 3577

# AOIname = 'CollierRange'
# xmin = -1300000
# xmax = -1200000
# ymin = -2700000
# ymax = -2600000
# EPSG = 3577

# AOIname = 'Coorong'
# xmin = 600000
# xmax = 700000
# ymin = -3950000
# ymax = -3850000
# EPSG = 3577

# AOIname = 'Brisbane'
# xmin = 2000000
# xmax = 2100000
# ymin = -3200000
# ymax = -3100000
# EPSG = 3577

# AOIname = 'Dundas'
# xmin = -1000000
# xmax = -900000
# ymin = -3650000
# ymax = -3550000
# EPSG = 3577

# set path to OSM data (must be in same crs as bounding box)
OSM_buildings = '/Users/chrisowers/Desktop/temp/level3_testing/OSM/OSM_EPSG3577/gis_osm_buildings_EPSG3577.shp'
OSM_roads = '/Users/chrisowers/Desktop/temp/level3_testing/OSM/OSM_EPSG3577/gis_osm_roads_EPSG3577.shp'
OSM_railways = ('/Users/chrisowers/Desktop/temp/level3_testing/OSM/OSM_EPSG3577/gis_osm_railways_EPSG3577.shp')
os.makedirs('./OSMtemp', exist_ok=True)


########## Does AOI have > 1% of OSM in tile? ##########

# bounding box
bounds = Polygon([(xmin,ymin), (xmin, ymax), (xmax, ymax), (xmax,ymin)])
print("bounds area", bounds.area)


# check if there are buildings and if so get area
OSM_buildings = gp.read_file(OSM_buildings)
if not (OSM_buildings['geometry'].intersection(bounds)).any():
    blds_area = 0
    print("No buildings present in AOI")
else:
    OSM_buildings = OSM_buildings['geometry'].intersection(bounds)
    blds_area = 0
    for buildings in OSM_buildings[OSM_buildings.geometry.area>0]:
        blds_area += buildings.area
print("buildings area", blds_area)


# check if there are roads and if so buffer 3m then get area
OSM_roads = gp.read_file(OSM_roads)
if not (OSM_roads['geometry'].intersection(bounds)).any():
    roads_area = 0
    print("No roads present in AOI")
else:
    OSM_roads = OSM_roads['geometry'].intersection(bounds)
    if  (OSM_roads[OSM_roads.geometry.length>0]).any():
        OSM_roads[OSM_roads.geometry.length>0].to_file("./OSMtemp/gis_osm_roads_EPSG3577_AOI.shp", driver='ESRI Shapefile')
        OSM_roads_input = './OSMtemp/gis_osm_roads_EPSG3577_AOI.shp'
        roads_buf3m = './OSMtemp/gis_osm_roads_EPSG3577_AOI_buf3m.shp'
        bufferDist = 3
        vectorutils.buffervector(OSM_roads_input, roads_buf3m, bufferDist, True)
        OSM_roads = gp.read_file(roads_buf3m)
        OSM_roads = OSM_roads['geometry'].intersection(bounds)
        roads_area = 0
        for roads in OSM_roads[OSM_roads.geometry.area>0]:
            roads_area += roads.area
        print("roads area", roads_area)
    else:
        roads_area = 0
        print("No roads geometry greater than 0 present in AOI")


# check if there are railways and if so buffer 3m then get area
OSM_railways = gp.read_file(OSM_railways)
if not (OSM_railways['geometry'].intersection(bounds)).any():
    rail_area = 0
    print("No railways present in AOI")
else:
    OSM_railways = OSM_railways['geometry'].intersection(bounds)
    if  (OSM_railways[OSM_railways.geometry.length>0]).any():
        OSM_railways[OSM_railways.geometry.length>0].to_file("./OSMtemp/gis_osm_railways_EPSG3577_AOI.shp", driver='ESRI Shapefile')
        OSM_railways_input = './OSMtemp/gis_osm_railways_EPSG3577_AOI.shp'
        railways_buf3m = './OSMtemp/gis_osm_railways_EPSG3577_AOI_buf3m.shp'
        bufferDist = 3
        vectorutils.buffervector(OSM_railways_input, railways_buf3m, bufferDist, True)
        OSM_railways = gp.read_file(railways_buf3m)
        OSM_railways = OSM_railways['geometry'].intersection(bounds)
        rail_area = 0
        for railways in OSM_railways[OSM_railways.geometry.area>0]:
            rail_area += railways.area
        print("railway area", rail_area)
    else:
        rail_area = 0
        print("No railway geometry greater than 0 present in AOI")

# find total area of OSM data in tile
total_area = blds_area + roads_area + rail_area
print("total artificial surface area", total_area)

percent_area = (total_area/(bounds.area))*100
print("percent artificial surface area in AOI", percent_area)

if not percent_area > 0.1:
    print('artificial surface area is less than 0.1 % in AOI, assuming all non-vegetated terrestrial areas are natural surfaces')
else:
    print('artificial surface area is greater than 0.1 %, performing k means clustering to differentiate artificial and natural surfaces')


