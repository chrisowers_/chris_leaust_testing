#!/bin/bash

#PBS -N fc_summary
#PBS -P u46
#PBS -q express
#PBS -M cho18@aber.ac.uk
#PBS -m abe
#PBS -l walltime=00:20:00
#PBS -l mem=64GB
#PBS -l ncpus=4
#PBS -l wd

module use /g/data/v10/public/modules/modulefiles
module load dea
python fc_summary.py
