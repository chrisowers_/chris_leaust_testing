location: '/g/data/r78/LCCS_Aberystwyth/co6850/datacube-2nd-order-stats/test35'

sources:
  - product: ls8_nbart_albers
    measurements: [blue, green, red, nir, swir1, swir2]
    group_by: solar_day
    masks:
      - product: ls8_pq_albers
        measurement: pixelquality
        group_by: solar_day
        fuse_func: datacube.helpers.ga_pq_fuser
        flags:
          contiguous: True
          cloud_acca: no_cloud
          cloud_fmask: no_cloud
          cloud_shadow_acca: no_cloud_shadow
          cloud_shadow_fmask: no_cloud_shadow
          blue_saturated: False
          green_saturated: False
          red_saturated: False
          nir_saturated: False
          swir1_saturated: False
          swir2_saturated: False

date_ranges:
  start_date: 2017-07-01
  end_date: 2018-01-01
  stats_duration: 6m
  step_size: 6m

storage:
  driver: GeoTIFF

  crs: EPSG:3577
  tile_size:
    x: 100000.0
    y: 100000.0
  resolution:
    x: 25
    y: -25
  chunking: # Must be divisible by 16 for GeoTIFF
    x: 256
    y: 256
    time: 1
  dimension_order: [time, y, x]

computation:
  chunking:
    x: 1000
    y: 200

output_products:
  - name: ls8_nbart_tmad_annual
    product_type: surface_reflectance_triple_mad
    statistic: external
    file_path_template: 'ls8_ternary_mad-{epoch_start:%Y%m%d}-{epoch_end:%Y%m%d}_{x}_{y}.tif'
    statistic_args:
        impl: model.TernaryMAD
        num_threads: 3
    output_params:
      zlib: True
      fletcher32: True
    metadata:
      format:
        name: GeoTIFF
      platform:
        code: LANDSAT_8
      instrument:
        name: OLI


global_attributes:
  cmi_id: "SR_TMAD_25_2.1.0"
  title: "LS8 Surface Reflectance Triple Median Absolute Deviation 2.1.0"
  summary: |
    This product provides 'second order' statistical techniques that follow from the geometric median, useful for environmental characterisation and change detection. The Median Absolute Deviation (MAD) is a measure of variance in a dataset through comparison to the median. It is similar in concept to the way that the standard deviation in statistics can be used to understand variance compared to the mean.
  institution: "Commonwealth of Australia (Geoscience Australia)"
  keywords: "AU/GA,NASA/GSFC/SED/ESD/LANDSAT,TM,EARTH SCIENCE"
  keywords_vocabulary: "GCMD"
  product_version: "2.1.0"
  publisher_email: earth.observation@ga.gov.au
  publisher_name: Section Leader, Operations Section, NEMO, Geoscience Australia
  publisher_url: http://www.ga.gov.au
  license: "CC BY Attribution 4.0 International License"
  cdm_data_type: "Grid"
  product_suite: "Surface Reflectance Triple Median Absolute Deviation"
  source: "datacube-2nd-order-stats"
  acknowledgments: |
    The high-dimensional statistics algorithms incorporated in this product is the work of Dr Dale Roberts, Australian National University.
  references: |
    Roberts, D., Dunn, B. and Mueller, N. (2018). Open Data Cube Products Using High-Dimensional Statistics of Time Series. IGARSS 2018 - 2018 IEEE International Geoscience and Remote Sensing Symposium.
