#!/bin/bash
#PBS -N 2ndOrderstats_test1
#PBS -P r78 -l ncpus=4,mem=32GB,walltime=15:00:00,jobfs=10GB,wd -q normal -M cho18@aber.ac.uk -m abe
#OLD -P r78 -l ncpus=32,mem=64GB,walltime=10:00:00,jobfs=10GB,wd -q normalbw -M cho18@aber.ac.uk -m abe

#OLD -l ncpus=3,mem=5GB,walltime=10:00:00,jobfs=1GB,wd -q express
#OLD -l ncpus=7,mem=980GB,walltime=24:00:00,jobfs=50GB,wd -q hugemem

NJOBS=16 # per node

DEAENV='dea-env'
DBCONF=${DBCONF:-'datacube.conf'}
CONFIG=${CONFIG:-'ls8_tmad_test31.yaml'}

module use /g/data/v10/public/modules/modulefiles
module load $DEAENV

echo "database: ${DBCONF}"
echo "config: ${CONFIG}"

# Fail on any errors
set -e

# Generate fast code
echo "Cythonizing code"
cythonize model/fast.pyx

echo "Compiling code"
export NCI_GXX_ABI_WARNING=1
module load gcc/6.2.0
gcc -shared -pthread -fopenmp -fPIC -O3 -fno-strict-aliasing \
	-I$(python3 -c 'import numpy; print(numpy.get_include())') \
	$(python3-config --includes) -Wno-cpp model/fast.c \
	-o model/fast.so

if [ -z "$PBS_JOBID" ]; then
	echo "Exiting as not running under PBS"
	exit 1
fi

# Check status - exit if all tiles are complete

# ./stat2 $CONFIG

INDEX=/short/r78/LCCS_Aberystwyth/$USER/$PBS_JOBID
NNODES=$(cat $PBS_NODEFILE | uniq | wc -l)
NCPUS=$(cat $PBS_NODEFILE | grep $(hostname) | wc -l)
JOBDIR=$PWD

#echo "Retiling"

mkdir -p $INDEX
function cleanup { rm -fr $INDEX; }
trap cleanup EXIT

./retile ${TILEFILE:-test_tiles} $CONFIG > $PBS_JOBFS/tilesr
NTILES=$(cat $PBS_JOBFS/tilesr | wc -l)
NSPLIT=$(( ($NTILES + $NNODES - 1)/$NNODES ))
sort -r -n $PBS_JOBFS/tilesr | split -l $NSPLIT - $INDEX/x

echo "ntiles: ${NTILES} nsplit: ${NSPLIT} nnodes: ${NNODES} njobs: ${NJOBS}/node"

echo "Distributing work"

TILES=($INDEX/x*)
for i in $(seq 1 $NNODES); do
  TILEINDEX=$(($i-1))
  TILEFILE="${TILES[$TILEINDEX]}"
  pbsdsh -n $(( $NCPUS*$i )) -- \
  bash -l -c "\
        module load parallel $DEAENV; cd $JOBDIR;\
        parallel --wc -j$NJOBS --delay 120 --linebuffer --colsep ' ' \
	--resume-failed --joblog $PBS_JOBFS/log \
        -a $TILEFILE \"datacube-stats -v -C $DBCONF \
	--tile-index {1} {2} $CONFIG\"" &
done;
wait
