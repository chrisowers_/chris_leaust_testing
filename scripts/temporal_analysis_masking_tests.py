import sys
import numpy as np
import xarray
import datacube
from datacube.storage import masking
from datacube.helpers import write_geotiff
from datacube.utils.geometry import CRS

dc = datacube.Datacube(app="level3")

# import DEADataHandling for clear Fractional Cover Scenes
sys.path.append('/g/data/r78/LCCS_Aberystwyth/co6850/dea-notebooks/10_Scripts/')
import DEADataHandling
import BandIndices

res = (-25, 25)
time = ('2015-01-01', '2015-12-31')
crs = 'EPSG:3577'

Ayr = {'AOI': 'Ayr', 'query': {'x':(1500000, 1600000), 'y':(-2200000, -2100000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1500000, 'target_max_x': 1600000, 'target_min_y':-2200000, 'target_max_y': -2100000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Diamantina = {'AOI': 'Diamantina', 'query': {'x':(800000, 900000), 'y':(-2800000, -2700000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 800000, 'target_max_x': 900000, 'target_min_y':-2800000, 'target_max_y': -2700000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Gwydir = {'AOI': 'Gwydir', 'query': {'x':(1600000, 1700000), 'y':(-3400000, -3300000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1600000, 'target_max_x': 1700000, 'target_min_y':-3400000, 'target_max_y': -3300000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Leichhardt = {'AOI': 'Leichhardt', 'query': {'x':(800000, 900000), 'y':(-2000000, -1900000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 800000, 'target_max_x': 900000, 'target_min_y':-2000000, 'target_max_y': -1900000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Kakadu = {'AOI': 'Kakadu', 'query': {'x':(0, 100000), 'y':(-1350000, -1250000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 0, 'target_max_x': 100000, 'target_min_y':-1350000, 'target_max_y': -1250000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Hobart = {'AOI': 'Hobart', 'query': {'x':(1200000, 1300000), 'y':(-4800000, -4700000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1200000, 'target_max_x': 1300000, 'target_min_y':-4800000, 'target_max_y': -4700000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Perth = {'AOI': 'Perth', 'query': {'x':(-1550000, -1450000), 'y':(-3650000, -3550000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': -1550000, 'target_max_x': -1450000, 'target_min_y':-3650000, 'target_max_y': -3550000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Murray_Valley = {'AOI': 'Murray_Valley', 'query': {'x':(1100000, 1200000), 'y':(-4000000, -3900000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1100000, 'target_max_x': 1200000, 'target_min_y':-4000000, 'target_max_y': -3900000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Adelaide = {'AOI': 'Adelaide', 'query': {'x':(550000, 650000), 'y':(-3850000, -3750000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 550000, 'target_max_x': 650000, 'target_min_y':-3850000, 'target_max_y': -3750000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Lake_Eyre = {'AOI': 'Lake_Eyre', 'query': {'x':(500000, 600000), 'y':(-3000000, -2900000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 500000, 'target_max_x': 600000, 'target_min_y':-3000000, 'target_max_y': -2900000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Blue_Mtns = {'AOI': 'Blue_Mtns', 'query': {'x':(1600000, 1700000), 'y':(-3900000, -3800000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1600000, 'target_max_x': 1700000, 'target_min_y':-3900000, 'target_max_y': -3800000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Aust_Alps = {'AOI': 'Aust_Alps', 'query': {'x':(1400000, 1500000), 'y':(-4100000, -4000000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1400000, 'target_max_x': 1500000, 'target_min_y':-4100000, 'target_max_y': -4000000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Collier_Range = {'AOI': 'Collier_Range', 'query': {'x':(-1300000, -1200000), 'y':(-2700000, -2600000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': -1300000, 'target_max_x': -1200000, 'target_min_y':-2700000, 'target_max_y': -2600000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Coorong = {'AOI': 'Coorong', 'query': {'x':(600000, 700000), 'y':(-3950000, -3850000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 600000, 'target_max_x': 700000, 'target_min_y':-3950000, 'target_max_y': -3850000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Brisbane = {'AOI': 'Brisbane', 'query': {'x':(2000000, 2100000), 'y':(-3200000, -3100000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x':2000000, 'target_max_x': 2100000, 'target_min_y':-3200000, 'target_max_y': -3100000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Mt_Ney = {'AOI': 'Mt_Ney', 'query': {'x':(-1000000, -900000), 'y':(-3650000, -3550000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x':-1000000, 'target_max_x': -900000, 'target_min_y':-3650000, 'target_max_y': -3550000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

# sites = [Ayr, Diamantina, Gwydir, Leichhardt, Kakadu, Hobart, Perth, Murray_Valley, Adelaide, Lake_Eyre, Blue_Mtns, Aust_Alps, Collier_Range, Coorong, Brisbane, Mt_Ney]

sites = [Perth]


for site in sites:
    
    mask_dict = {'cloud_acca': 'no_cloud',
                 'cloud_fmask': 'no_cloud',
                 'contiguous': True}


   
    mask = '/g/data/r78/LCCS_Aberystwyth/co6850/chris_leaust_testing/scripts/'+str(site['AOI'])+'_veg_mask_2015.tif'
    mask = xarray.open_rasterio(mask)
    mask = mask == 1
    mask = mask.isel(band=0).squeeze().drop('band')

    
##### NDBI ##### 
#     LS = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
#                                            sensors=['ls5','ls7','ls8'],
#                                            bands_of_interest=['nir', 'swir1'],
#                                            masked_prop=0.1,
#                                            mask_dict=mask_dict)
#     NDBI = (LS.swir1 - LS.nir)/(LS.swir1 + LS.nir)
#     NDBI_stddev = NDBI.groupby('time.year').std(dim = 'time').squeeze().drop('year') * mask
#     NDBI_stddev = NDBI_stddev.to_dataset(name='array')
#     NDBI_stddev.attrs['crs'] = CRS('EPSG:3577')
#     write_geotiff(filename=str(site['AOI'])+'_masked_NDBI_stddev_2015.tif', dataset=NDBI_stddev)
#     print(str(site['AOI'])+'_masked_NDBI_stddev_2015.tif')


    
# ##### Fractional cover #####
#     FC = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
#                                            product = 'fc',
#                                            masked_prop=0.1,
#                                            mask_dict=mask_dict)    
  
#     BS_stddev = FC['BS'].groupby('time.year').std(dim = 'time').squeeze().drop('year') * mask
#     BS_stddev = BS_stddev.to_dataset(name='array')
#     BS_stddev.attrs['crs'] = CRS('EPSG:3577')
#     write_geotiff(filename=str(site['AOI'])+'_masked_BS_stddev_2015.tif', dataset=BS_stddev)
#     print(str(site['AOI'])+'_masked_BS_stddev_2015.tif')

#     BS_mask = FC['BS'] > (FC['PV'] + FC['NPV'])
#     BS_bare = BS_mask.where(FC['BS'] > 0)
#     BS_summary2 = BS_bare.mean(dim = 'time') * mask    
#     BS_summary2 = BS_summary2.to_dataset(name='array')
#     BS_summary2.attrs['crs'] = CRS('EPSG:3577')
#     write_geotiff(filename=str(site['AOI'])+'_masked_BS_sum2_2015.tif', dataset=BS_summary2)
#     print(str(site['AOI'])+'_masked_BS_sum2_2015.tif')    
    
#     BS_mask = FC['NPV'] > FC['PV']
#     BS_bare = BS_mask.where(FC['BS'] > 0)
#     BS_summary12 = BS_bare.mean(dim = 'time') * mask    
#     BS_summary12 = BS_summary12.to_dataset(name='array')
#     BS_summary12.attrs['crs'] = CRS('EPSG:3577')
#     write_geotiff(filename=str(site['AOI'])+'_masked_BS_sum12_2015.tif', dataset=BS_summary12)
#     print(str(site['AOI'])+'_masked_BS_sum12_2015.tif')   
    
    
##### Tasseled cap #####
    LS = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                           sensors=['ls5','ls7','ls8'],
                                           masked_prop=0.5,
                                           mask_dict=mask_dict)    
    
#     TC = BandIndices.tasseled_cap(LS, tc_bands=['wetness'], drop=True)
#     TCW_stddev = TC['wetness'].groupby('time.year').std(dim = 'time').squeeze().drop('year') * mask
#     TCW_stddev = TCW_stddev.to_dataset(name='array')
#     TCW_stddev.attrs['crs'] = CRS('EPSG:3577')
#     write_geotiff(filename=str(site['AOI'])+'_masked_TCW_stddev_2015.tif', dataset=TCW_stddev)
#     print(str(site['AOI'])+'_masked_TCW_stddev_2015.tif')

    TC = BandIndices.tasseled_cap(LS, tc_bands=['wetness'], drop=True)
    TCW_min = TC['wetness'].groupby('time.year').min(dim = 'time').squeeze().drop('year') * mask
    TCW_min = TCW_min.to_dataset(name='array')
    TCW_min.attrs['crs'] = CRS('EPSG:3577')
    write_geotiff(filename=str(site['AOI'])+'_masked_TCW_min_2015.tif', dataset=TCW_min)
    print(str(site['AOI'])+'_masked_TCW_min_2015.tif')

#     TCW_np_max = np.max(TC['wetness'])
#     TCW_np_min = np.min(TC['wetness'])
#     TCW_normalised = (TC['wetness']-TCW_np_min)/(TCW_np_max-TCW_np_min) * mask
#     TCWn_stddev = TCW_normalised.groupby('time.year').std(dim = 'time').squeeze().drop('year') * mask
#     TCWn_stddev = TCWn_stddev.to_dataset(name='array')
#     TCWn_stddev.attrs['crs'] = CRS('EPSG:3577')
#     write_geotiff(filename=str(site['AOI'])+'_masked_TCWn_stddev_2015.tif', dataset=TCWn_stddev)
#     print(str(site['AOI'])+'_masked_TCWn_stddev_2015.tif')

