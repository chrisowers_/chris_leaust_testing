import datacube
import matplotlib.pyplot as plt
from datacube import helpers
from datacube.utils import geometry
from scipy import ndimage
import xarray as xr
import numpy as np
import sys
sys.path.append('/g/data/u46/users/sc0554/dea-notebooks/10_Scripts/')
import DEADataHandling

dc=datacube.Datacube(app="new veg")

# AOI = 'Ayr'
# x = (1500000, 1600000)
# y = (-2200000, -2100000)

# AOI = 'Diamantina'
# x = (800000, 900000)
# y = (-2800000, -2700000)

# AOI = 'Gwydir'
# x = (1600000, 1700000)
# y = (-3400000, -3300000)

# AOI = 'Leichhardt'
# x = (800000, 900000)
# y = (-2000000, -1900000)

# AOI = 'Kakadu'
# x = (0, 100000)
# y = (-1350000, -1250000)

# AOI = 'Hobart'
# x = (1200000, 1300000)
# y = (-4800000, -4700000)

AOI = 'Perth'
x = (-1550000, -1450000)
y = (-3650000, -3550000)

# AOI = 'Murray_Valley'
# x = (1100000, 1200000)
# y = (-4000000, -3900000)

# AOI = 'Adelaide'
# x = (550000, 650000)
# y = (-3850000, -3750000)

# AOI = 'Lake_Eyre'
# x = (500000, 600000)
# y = (-3000000, -2900000)

# AOI = 'Blue_mtns'
# x = (1600000, 1700000)
# y = (-3900000, -3800000)

# AOI = 'Aust_Alps'
# x = (1400000, 1500000)
# y = (-4100000, -4000000)

# AOI = 'Collier_Range'
# x = (-1300000, -1200000)
# y = (-2700000, -2600000)

# AOI = 'Coorong'
# x = (600000, 700000)
# y = (-3950000, -3850000)

# AOI = 'Brisbane'
# x = (2000000, 2100000)
# y = (-3200000, -3100000)

# AOI = 'Dundas'
# x = (-1000000, -900000)
# y = (-3650000, -3550000)

# AOI = 'Canberra'
# x = (1500000, 1600000)
# y = (-4000000, -3900000)

product = 'fc'
query = {'time': ('2015-01-01', '2015-12-31'),
        'resolution' : (-25, 25)}
query['x'] = (x[0], x[1])
query['y'] = (y[0], y[1])
query['crs'] = 'EPSG:3577'

mask_dict = {'cloud_acca': 'no_cloud',
             'cloud_fmask': 'no_cloud',
             'contiguous': True}

data = DEADataHandling.load_clearlandsat(dc=dc, query=query, product=product,
                                  masked_prop=0.1,
                                  mask_dict=mask_dict,
                                  satellite_metadata=True)
# Create a mask to show areas where total vegetation is greater than the bare-soil fraction of a pixel for each scene
# PV + NPV
tv_mask = data['BS'] < data['PV'] + data['NPV']
tv = tv_mask.where(data['BS'] > 0)
# Calculate the proportion of time where total vegetation is greater than the bare soil fraction of a pixel
# for the input year
tv_summary = tv.mean(dim='time')
# Create a boolean layer where vegetation is assigned if greater than 0.167
tv_summary_filt = tv_summary > 0.167
# Convert booleans to binary
tv_summary_filt = tv_summary_filt * 1
# Change data type to float
tv_summary_filt = tv_summary_filt.data.astype(float)

meta_d = data.isel(time=0).drop('time')
out = xr.Dataset({'fc_summary': (meta_d.dims, tv_summary_filt)}, coords=meta_d.coords, attrs=meta_d.attrs)
helpers.write_geotiff(str(AOI)+'_pv_npv_bs_fcsummary16_100.tif', out)

# Create a mask to show areas where total vegetation is greater than the bare-soil fraction of a pixel for each scene
# PV + NPV
tv_mask = data['BS'] < data['PV'] + data['NPV']
tv = tv_mask.where(data['BS'] > 0)
# Calculate the proportion of time where total vegetation is greater than the bare soil fraction of a pixel
# for the input year
tv_summary = tv.mean(dim='time')
# Create a boolean layer where vegetation is assigned if greater than 0.167
tv_summary_filt = tv_summary > 0.50
# Convert booleans to binary
tv_summary_filt = tv_summary_filt * 1
# Change data type to float
tv_summary_filt = tv_summary_filt.data.astype(float)

meta_d = data.isel(time=0).drop('time')
out = xr.Dataset({'fc_summary': (meta_d.dims, tv_summary_filt)}, coords=meta_d.coords, attrs=meta_d.attrs)
helpers.write_geotiff(str(AOI)+'_pv_npv_bs_fcsummary50_100.tif', out)

# # PV
# tv_mask = data['BS'] < data['PV']
# tv = tv_mask.where(data['BS'] > 0)
# # Calculate the proportion of time where total vegetation is greater than the bare soil fraction of a pixel
# # for the input year
# tv_summary = tv.mean(dim='time')
# # Create a boolean layer where vegetation is assigned if greater than 0.167
# tv_summary_filt = tv_summary > 0.167
# # Convert booleans to binary
# tv_summary_filt = tv_summary_filt * 1
# # Change data type to float
# tv_summary_filt = tv_summary_filt.data.astype(float)

# meta_d = data.isel(time=0).drop('time')
# out = xr.Dataset({'fc_summary': (meta_d.dims, tv_summary_filt)}, coords=meta_d.coords, attrs=meta_d.attrs)
# helpers.write_geotiff(str(AOI)+'_pv_bs_fcsummary.tif', out)

# # NPV
# tv_mask = data['BS'] < data['NPV']
# tv = tv_mask.where(data['BS'] > 0)
# # Calculate the proportion of time where total vegetation is greater than the bare soil fraction of a pixel
# # for the input year
# tv_summary = tv.mean(dim='time')
# # Create a boolean layer where vegetation is assigned if greater than 0.167
# tv_summary_filt = tv_summary > 0.167
# # Convert booleans to binary
# tv_summary_filt = tv_summary_filt * 1
# # Change data type to float
# tv_summary_filt = tv_summary_filt.data.astype(float)

# meta_d = data.isel(time=0).drop('time')
# out = xr.Dataset({'fc_summary': (meta_d.dims, tv_summary_filt)}, coords=meta_d.coords, attrs=meta_d.attrs)
# helpers.write_geotiff(str(AOI)+'_npv_bs_fcsummary.tif', out)
