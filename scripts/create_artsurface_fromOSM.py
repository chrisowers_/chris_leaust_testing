'''

This is a workflow to create a layer representing % artificial surface for an area of interest.
Based on the Open Street Map data (OSM) https://www.openstreetmap.org/

Currently requires OSM for area of interest (buldings, roads, railways) to be downloaded, reprojected to crs in meters
(i.e. EPSG:3577), and accessible as shapefiles (.shp)

Chris Owers July 2019

'''

import os, shutil
import ogr, gdal
import geopandas as gp
from shapely.geometry import Polygon
from rsgislib import vectorutils, imageutils, imagecalc

########## Required inputs ##########

# AOI (set bounding box and crs that is in meters)
AOI = 'Brisbane'
xmin = 2000000
xmax = 2100000
ymin = -3200000
ymax = -3100000
EPSG = 3577

# set path to OSM data (must be in same crs as bounding box)
OSM_buildings = './OSM_EPSG3577/gis_osm_buildings_EPSG3577.shp'
OSM_roads = './OSM_EPSG3577/gis_osm_roads_EPSG3577.shp'
OSM_railways = './OSM_EPSG3577/gis_osm_railways_EPSG3577.shp'
os.makedirs('./'+str(AOI)+'_artsurface', exist_ok=True)
os.makedirs('./'+str(AOI)+'_artsurface/temp', exist_ok=True)

# Set cell size for comparison to EO data i.e. for Landsat (25m), or Sentinel (10m)
resolution = 25

# delete temporary files when finished?
RemoveTemp = True


########## make polygon for bounding box - needed later for exporting raster images ##########
bounds = Polygon([(xmin,ymin), (xmin, ymax), (xmax, ymax), (xmax,ymin)])

# convert bounds to a shapefile with OGR
driver = ogr.GetDriverByName('Esri Shapefile')
ds = driver.CreateDataSource('./'+str(AOI)+'_artsurface/temp/'+str(AOI)+'_AOI.shp')
set_crs = ogr.osr.SpatialReference()
set_crs.ImportFromEPSG(EPSG)
layer = ds.CreateLayer('', set_crs, ogr.wkbPolygon)
set_crs = ogr.osr.SpatialReference()
set_crs.ImportFromEPSG(EPSG)

# Add one attribute
layer.CreateField(ogr.FieldDefn('id', ogr.OFTInteger))
defn = layer.GetLayerDefn()

# Create a new feature (attribute and geometry)
feat = ogr.Feature(defn)
feat.SetField('id', 1)

# Make a geometry, from Shapely object
geom = ogr.CreateGeometryFromWkb(bounds.wkb)
feat.SetGeometry(geom)

layer.CreateFeature(feat)
feat = geom = None

# Save and close everything
ds = layer = feat = geom = None


########## pull out buildings shapefile from bounding box ##########

# Read in buildings to GeoDataFrame
OSM_buildings = gp.read_file(OSM_buildings)

# Set the bounds of the crop box
bounds = Polygon([(xmin,ymin), (xmin, ymax), (xmax, ymax), (xmax,ymin)])

# Crop all polygons and take the part inside the bounding box
OSM_buildings['geometry'] = OSM_buildings['geometry'].intersection(bounds)

# Export non-empty geometries to shp
OSM_buildings[OSM_buildings.geometry.area>0].to_file('./'+str(AOI)+'_artsurface/temp/gis_osm_buildings_EPSG3577_'+str(AOI)+'.shp', driver='ESRI Shapefile')

print('OSM buildings extracted for '+str(AOI))


########## pull out roads shapefile from bounding box and buffer 3m ##########

# Read in buildings to GeoDataFrame
OSM_roads = gp.read_file(OSM_roads)

# Set the bounds of the crop box
bounds = Polygon([(xmin,ymin), (xmin, ymax), (xmax, ymax), (xmax,ymin)])

# Crop all polygons and take the part inside the bounding box
OSM_roads['geometry'] = OSM_roads['geometry'].intersection(bounds)

# Export non-empty geometries to shp
OSM_roads[OSM_roads.geometry.length>0].to_file('./'+str(AOI)+'_artsurface/temp/gis_osm_roads_EPSG3577_'+str(AOI)+'.shp', driver='ESRI Shapefile')

# buffer roads by 3m
inputVector = './'+str(AOI)+'_artsurface/temp/gis_osm_roads_EPSG3577_'+str(AOI)+'.shp'
outputVector = './'+str(AOI)+'_artsurface/temp/gis_osm_roads_EPSG3577_'+str(AOI)+'_buf3m.shp'
bufferDist = 3
vectorutils.buffervector(inputVector, outputVector, bufferDist, True)

print('OSM roads extracted and buffered 3m for '+str(AOI))


########## pull out railways shapefile from bounding box and buffer 3m #########

# Read in buildings to GeoDataFrame
OSM_railways = gp.read_file(OSM_railways)

# Set the bounds of the crop box
bounds = Polygon([(xmin,ymin), (xmin, ymax), (xmax, ymax), (xmax,ymin)])

# Crop all polygons and take the part inside the bounding box
OSM_railways['geometry'] = OSM_railways['geometry'].intersection(bounds)

# Export non-empty geometries to shp
OSM_railways[OSM_railways.geometry.length>0].to_file('./'+str(AOI)+'_artsurface/temp/gis_osm_railways_EPSG3577_'+str(AOI)+'.shp', driver='ESRI Shapefile')

# buffer railways by 3m
inputVector = './'+str(AOI)+'_artsurface/temp/gis_osm_railways_EPSG3577_'+str(AOI)+'.shp'
outputVector = './'+str(AOI)+'_artsurface/temp/gis_osm_railways_EPSG3577_'+str(AOI)+'_buf3m.shp'
bufferDist = 3
vectorutils.buffervector(inputVector, outputVector, bufferDist, True)

print('OSM railways extracted and buffered 3m for '+str(AOI))


########## merge buildings and buffered roads and railways #########

outputMergefn = './'+str(AOI)+'_artsurface/temp/'+str(AOI)+'_merge.shp'
directory = './'+str(AOI)+'_artsurface/temp/'
fileStartsWith = 'gis_osm_buildings_EPSG3577_'+str(AOI), 'gis_osm_roads_EPSG3577_'+str(AOI)+'_buf3m', 'gis_osm_railways_EPSG3577_'+str(AOI)+'_buf3m'
fileEndsWith = '.shp'
driverName = 'ESRI Shapefile'
geometryType = ogr.wkbPolygon

out_driver = ogr.GetDriverByName(driverName)
if os.path.exists(outputMergefn):
    out_driver.DeleteDataSource(outputMergefn)
out_ds = out_driver.CreateDataSource(outputMergefn)
set_crs = ogr.osr.SpatialReference()
set_crs.ImportFromEPSG(EPSG)
out_layer = out_ds.CreateLayer(outputMergefn, set_crs, geom_type=geometryType)

fileList = os.listdir(directory)

for file in fileList:
    if file.startswith(fileStartsWith) and file.endswith(fileEndsWith):
        print(file)
        ds = ogr.Open(directory+file)
        lyr = ds.GetLayer()
        for feat in lyr:
            out_feat = ogr.Feature(out_layer.GetLayerDefn())
            out_feat.SetGeometry(feat.GetGeometryRef().Clone())
            out_layer.CreateFeature(out_feat)
            out_feat = None
            out_layer.SyncToDisk()
del out_ds, out_layer
print('OSM merged for '+str(AOI))


########## rasterize OSM AOI to 1m  ##########

# Define pixel_size and NoData value of new raster
pixel_size = 1
NoData_value = 0

# Filename of input OGR file
vector_fn = './'+str(AOI)+'_artsurface/temp/'+str(AOI)+'_merge.shp'

# Filename of the raster Tiff that will be created
raster_fn = './'+str(AOI)+'_artsurface/temp/'+str(AOI)+'_1m.tif'

# Open the data source and read in the extent
source_ds = ogr.Open(vector_fn)
source_layer = source_ds.GetLayer()
x_min, x_max, y_min, y_max = source_layer.GetExtent()

# Create the destination data source
x_res = int((x_max - x_min) / pixel_size)
y_res = int((y_max - y_min) / pixel_size)
target_ds = gdal.GetDriverByName('GTiff').Create(raster_fn, x_res, y_res, 1, gdal.GDT_Byte, options=['COMPRESS=DEFLATE'])
target_ds.SetGeoTransform((x_min, pixel_size, 0, y_max, 0, -pixel_size))
set_crs = ogr.osr.SpatialReference()
set_crs.ImportFromEPSG(EPSG)
target_ds.SetProjection(set_crs.ExportToWkt())
band = target_ds.GetRasterBand(1)
band.SetNoDataValue(NoData_value)

# Rasterize
gdal.RasterizeLayer(target_ds, [1], source_layer, burn_values=[1])
del target_ds, source_ds, source_layer

print('OSM rasterized to 1m for '+str(AOI))


########## create blank raster from OSM vector file coordinates ##########

invec = './'+str(AOI)+'_artsurface/temp/'+str(AOI)+'_AOI.shp'
image = './'+str(AOI)+'_artsurface/temp/'+str(AOI)+'_blank.tif'
imageutils.createBlankImgFromRefVector(invec, None, image, resolution, 1, 'GTiff', 3)


########## zonal stats for cover count from OSM to empty raster #########

outim = './'+str(AOI)+'_artsurface/temp/'+str(AOI)+'_zstats.tif'
statsim = './'+str(AOI)+'_artsurface/temp/'+str(AOI)+'_1m.tif'
imagecalc.getImgSumStatsInPxl(image, statsim, outim, 'GTiff', 3, [7], 1)


########## create % artificial surface #########

outputImage = './'+str(AOI)+'_artsurface/temp/'+str(AOI)+'_artsurfacepercent.tif'
expression = '(b1/625)*100'
imagecalc.imageMath(outim, outputImage, expression, 'GTiff', 5)


########## make % artificial surface blank.tif extent by mosaic #########

blankim = './'+str(AOI)+'_artsurface/temp/'+str(AOI)+'_blank.tif'
artsurface = './'+str(AOI)+'_artsurface/temp/'+str(AOI)+'_artsurfacepercent.tif'
inputList = blankim, artsurface
outImage = './'+str(AOI)+'_artsurface/'+str(AOI)+'_artsurfacecover.tif'
backgroundVal = 0.
skipVal = 0.
skipBand = 1
overlapBehaviour = 2
gdalformat = 'GTiff'
imageutils.createImageMosaic(inputList, outImage, backgroundVal, skipVal, skipBand, overlapBehaviour, 'GTiff', 5)

########## remove temporary files ##########

if RemoveTemp is True:
    shutil.move('./'+str(AOI)+'_artsurface/'+str(AOI)+'_artsurfacecover.tif', './'+str(AOI)+'_artsurfacecover.tif')
    shutil.rmtree('./'+str(AOI)+'_artsurface')
    print('Generated Artificial Surface Cover (%) for ' + str(AOI))
else:
    print('Generated Artificial Surface Cover (%) for '+str(AOI))