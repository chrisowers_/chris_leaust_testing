'''

current running script of level4

'''

import sys, os
import copy
import numpy
import xarray
import rasterio
from matplotlib import pyplot
import datacube
from datacube.storage import masking
from datacube.helpers import write_geotiff
from datacube.utils.geometry import CRS

import scipy
from scipy import stats
from skimage.segmentation import quickshift
from sklearn.impute import SimpleImputer

# must specific lccs_dev environment for MADs and mangrove datasets
dc = datacube.Datacube(env='lccs_dev', app='level4')

# import DEADataHandling for clear Fractional Cover Scenes
sys.path.append('../../../dea-notebooks/10_Scripts')
import DEADataHandling
import BandIndices
import TasseledCapTools


# import le_lccs modules
sys.path.append('../../../livingearth_lccs')
from le_lccs.le_ingest import gridded_ingest
from le_lccs.le_classification import lccs_l3
from le_lccs.le_classification import lccs_l4


os.makedirs('./level4', exist_ok=True)

res = (-25, 25)
time = ('2015-01-01', '2015-12-31')
crs = 'EPSG:3577'
sensor = 'ls8'

Ayr = {'AOI': 'Ayr', 'query': {'x':(1500000, 1600000), 'y':(-2200000, -2100000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1500000, 'target_max_x': 1600000, 'target_min_y':-2200000, 'target_max_y': -2100000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Diamantina = {'AOI': 'Diamantina', 'query': {'x':(800000, 900000), 'y':(-2800000, -2700000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 800000, 'target_max_x': 900000, 'target_min_y':-2800000, 'target_max_y': -2700000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Gwydir = {'AOI': 'Gwydir', 'query': {'x':(1600000, 1700000), 'y':(-3400000, -3300000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1600000, 'target_max_x': 1700000, 'target_min_y':-3400000, 'target_max_y': -3300000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Leichhardt = {'AOI': 'Leichhardt', 'query': {'x':(800000, 900000), 'y':(-2000000, -1900000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 800000, 'target_max_x': 900000, 'target_min_y':-2000000, 'target_max_y': -1900000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Kakadu = {'AOI': 'Kakadu', 'query': {'x':(0, 100000), 'y':(-1350000, -1250000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 0, 'target_max_x': 100000, 'target_min_y':-1350000, 'target_max_y': -1250000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Hobart = {'AOI': 'Hobart', 'query': {'x':(1200000, 1300000), 'y':(-4800000, -4700000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1200000, 'target_max_x': 1300000, 'target_min_y':-4800000, 'target_max_y': -4700000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Perth = {'AOI': 'Perth', 'query': {'x':(-1550000, -1450000), 'y':(-3650000, -3550000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': -1550000, 'target_max_x': -1450000, 'target_min_y':-3650000, 'target_max_y': -3550000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Murray_Valley = {'AOI': 'Murray_Valley', 'query': {'x':(1100000, 1200000), 'y':(-4000000, -3900000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1100000, 'target_max_x': 1200000, 'target_min_y':-4000000, 'target_max_y': -3900000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Adelaide = {'AOI': 'Adelaide', 'query': {'x':(550000, 650000), 'y':(-3850000, -3750000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 550000, 'target_max_x': 650000, 'target_min_y':-3850000, 'target_max_y': -3750000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Lake_Eyre = {'AOI': 'Lake_Eyre', 'query': {'x':(500000, 600000), 'y':(-3000000, -2900000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 500000, 'target_max_x': 600000, 'target_min_y':-3000000, 'target_max_y': -2900000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Blue_Mtns = {'AOI': 'Blue_Mtns', 'query': {'x':(1600000, 1700000), 'y':(-3900000, -3800000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1600000, 'target_max_x': 1700000, 'target_min_y':-3900000, 'target_max_y': -3800000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Aust_Alps = {'AOI': 'Aust_Alps', 'query': {'x':(1400000, 1500000), 'y':(-4100000, -4000000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 1400000, 'target_max_x': 1500000, 'target_min_y':-4100000, 'target_max_y': -4000000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Collier_Range = {'AOI': 'Collier_Range', 'query': {'x':(-1300000, -1200000), 'y':(-2700000, -2600000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': -1300000, 'target_max_x': -1200000, 'target_min_y':-2700000, 'target_max_y': -2600000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Coorong = {'AOI': 'Coorong', 'query': {'x':(600000, 700000), 'y':(-3950000, -3850000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x': 600000, 'target_max_x': 700000, 'target_min_y':-3950000, 'target_max_y': -3850000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Brisbane = {'AOI': 'Brisbane', 'query': {'x':(2000000, 2100000), 'y':(-3200000, -3100000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x':2000000, 'target_max_x': 2100000, 'target_min_y':-3200000, 'target_max_y': -3100000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}

Mt_Ney = {'AOI': 'Mt_Ney', 'query': {'x':(-1000000, -900000), 'y':(-3650000, -3550000), 'resolution': res, 'time': time, 'crs': crs}, 'query_geofabric': {'target_min_x':-1000000, 'target_max_x': -900000, 'target_min_y':-3650000, 'target_max_y': -3550000, 'target_pixel_size_x': res[1], 'target_pixel_size_y': res[0], 'target_crs': crs}}


###################################################################################################################    

# Normal
sites = [Ayr, Hobart, Perth, Murray_Valley, Adelaide, Blue_Mtns, Aust_Alps, Coorong, Brisbane]

for site in sites:
    ########## veg/non veg ##########
   
    # need to specify as pq not indexed in lccs_dev
    dc = datacube.Datacube(env='datacube', app="level3")
    
    mask_dict = {'cloud_acca': 'no_cloud',
                 'cloud_fmask': 'no_cloud',
                 'contiguous': True}
    # New vegetation classification
    FC = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                           sensors=['ls5','ls7','ls8'],
                                           product = 'fc',
                                           masked_prop=0.1,
                                           mask_dict=mask_dict)

    # Create a mask to show areas where total vegetation is greater than the baresoil fraction of a pixel for each scene
    tv_mask = FC['BS'] < (FC['PV'] + (FC['NPV'] < 90))
    tv = tv_mask.where(FC['BS'] > 0)
    # Calculate proportion of time where total vegetation is greater than the bare soil fraction of a pixel for the input year
    tv_summary = tv.mean(dim='time')
    tv_thres = tv_summary > 0.167
    
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], **site['query'])
    wofs_ann = masking.mask_invalid_data(wofs_ann)    
    aquatic_wat = 1-((wofs_ann["frequency"] >= 0.9))
    tv_thres = tv_thres * aquatic_wat

    vegetat_veg_cat_ds = tv_thres.to_dataset(name="vegetat_veg_cat").squeeze()
    
    # back to lccs_dev
    dc = datacube.Datacube(env='lccs_dev', app="level3")

    
    ########## aquatic/terrestrial ##########
    # Load data from datacube
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], **site['query'])
    wofs_ann = masking.mask_invalid_data(wofs_ann)

    item = dc.load(product="item_v2", measurements=["relative"], 
                         **site['query'])
    item = masking.mask_invalid_data(item)
    if bool(item) is True:
        item = item.squeeze().drop('time')
    else:
        print("item not available")

    mangrove = dc.load(product="mangrove_extent_cover_albers", measurements=["extent"], 
                         **site['query'])
    if bool(mangrove) is True:
        mangrove = masking.mask_invalid_data(mangrove)
    else:
        print("mangrove not available")

    # Create binary layer representing aquatic (1) and terrestrial (0)

    if bool(item) is True & bool(mangrove) is True:
        aquatic_wat = ((wofs_ann["frequency"] >= 0.2) | ((item["relative"] >= 2) & (item["relative"] <= 8)) | (mangrove["extent"] == 1))
    elif bool(item) is True:
        aquatic_wat = ((wofs_ann["frequency"] >= 0.2) | ((item["relative"] >= 2) & (item["relative"] <= 8)))
    elif bool(mangrove) is True:
        aquatic_wat = ((wofs_ann["frequency"] >= 0.2) | (mangrove["extent"] == 1))
    else:
        aquatic_wat = ((wofs_ann["frequency"] >= 0.2))

    # Convert to Dataset and add name
    aquatic_wat_cat_ds = aquatic_wat.to_dataset(name="aquatic_wat_cat").squeeze().drop('time')             

    
    ########## cultman/natural ##########
    tmad = dc.load(product=sensor +"_nbart_tmad_annual", **site['query'])
    tmad = tmad.isel(time=0)
    tmad = tmad.drop('time')
    
    # Impute missing values (0 and NaN)
    imp_0 = SimpleImputer(missing_values=0, strategy='mean')
    container = {}
    for key in tmad.data_vars:
        d = tmad[key].data.squeeze()
        d = numpy.nan_to_num(d)
        d = numpy.where(d < 0, 0, d)
        d = numpy.where(d == 1, 0, d)
        imp_0.fit(d)
        d = imp_0.transform(d)
        d = -numpy.log(d)
        container.update({key: d})
    tmad['edev'].data = container['edev']
    tmad['sdev'].data = container['sdev']
    tmad['bcdev'].data = container['bcdev']
    
    # Calculate the mean of all the tmad inputs
    tmad_mean = numpy.mean(numpy.stack([tmad.edev.data, tmad.sdev.data, tmad.bcdev.data], axis=-1), axis=-1)
    # Convert type to float64 (required for quickshift)
    tmad_mean = numpy.float64(tmad_mean)
    # Segment
    tmad_seg = quickshift(tmad_mean, kernel_size=2, convert2lab=False, max_dist=500, ratio=0.5)
    # Calculate the median for each segment
    tmad_median_seg = scipy.ndimage.median(input=tmad_mean, labels=tmad_seg, index=tmad_seg)
    # Set threshold as 10th percentile of mean TMAD
    thresh = numpy.percentile(tmad_mean.ravel(), 20)
    # Create boolean layer using threshold
    tmad_thresh = tmad_median_seg < thresh
    cultman_agr_cat_ds = xarray.Dataset({'cultman_agr_cat': (tmad.dims, tmad_thresh)}, coords=tmad.coords)
    
    ########## artsurface/natural ##########
    dc = datacube.Datacube(env='datacube', app="level3")  
    LS = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                       sensors=['ls5','ls7','ls8'],
                                       masked_prop=0.1,
                                       mask_dict=mask_dict)
    
    TCe = TasseledCapTools.pct_exceedance_tasseled_cap(LS, tc_bands=['greenness','brightness', 'wetness'],
                                               greenness_threshold=-500, 
                                               brightness_threshold=5000, 
                                               wetness_threshold=-2500, 
                                               drop=True, drop_tc_bands=True)
    TCe_thresG = TCe.greenness_pct_exceedance < 0.1
    TCe_thresB = TCe.brightness_pct_exceedance < 0.1
    TCe_thresW = TCe.wetness_pct_exceedance > 0.9
    TCE = TCe_thresG | TCe_thresB | TCe_thresW
    urban  = TCE > 0
    artific_urb_cat_ds = urban.to_dataset(name="artific_urb_cat").squeeze()
    

    ########## artwater/natural water ##########
    # Load data
    var_file = "/g/data/u46/wofs/confidence/geoFabric/geofabric.tif"
    var_name = "features"

    import_obj = gridded_ingest.LEIngestGDAL(**site['query_geofabric'])
    geofabric = import_obj.read_to_xarray(var_file, var_name)

    # Create binary layer representing artificial water (1) and natural water (0)
    artwatr_wat_cat = ((geofabric["features"] == 1) | (geofabric["features"] == 8))

    # Convert to Dataset and add name
    artwatr_wat_cat_ds = artwatr_wat_cat.to_dataset(name="artwatr_wat_cat")
    
    
    ########## LCCS classification ##########
    variables_xarray_list = []
    variables_xarray_list.append(vegetat_veg_cat_ds)
    variables_xarray_list.append(aquatic_wat_cat_ds)
    variables_xarray_list.append(cultman_agr_cat_ds)
    variables_xarray_list.append(artific_urb_cat_ds)
    variables_xarray_list.append(artwatr_wat_cat_ds)

    # Merge to a single dataframe
    classification_data = xarray.merge(variables_xarray_list)

    # Apply Level 3 classification using separate function. Works through in three stages
    level1, level2, level3 = lccs_l3.classify_lccs_level3(classification_data)

    # Save classification values back to xarray
    out_class_xarray = xarray.Dataset(
        {"level1" : (classification_data["vegetat_veg_cat"].dims, level1),
         "level2" : (classification_data["vegetat_veg_cat"].dims, level2),
         "level3" : (classification_data["vegetat_veg_cat"].dims, level3)})
    classification_data = xarray.merge([classification_data, out_class_xarray])

    # Convert level3 to Dataset and add name
    level3_ds = classification_data.level3.to_dataset(name="level3")

    print("Reading in data for level 4 classification")
    
    
    ########## Water State ##########
    waterstt_wat_cat_ds = aquatic_wat.to_dataset(name="waterstt_wat_cat").squeeze().drop('time')

    
    ########## Water Persistence ##########
    # Load data from datacube
    waterper = wofs_ann.frequency.squeeze().drop('time')

    '''
    WOfs does not provide hydroperiod in days, best alternative is frequency (wet/dry obs)
    which have been estimated based on months/year

    0.74 - 1    --> 1 : ("B1", "Perennial (> 9 months)"),
    0.58 - 0.74 --> 7 : ("B7", "Non-perennial (7 to 9 months)")
    0.3 - 0.58  --> 8 : ("B8", "Non-perennial (4 to 7 months)")
    0 - 0.3     --> 9 : ("B9", "Non-perennial (1 to 4 months)")

    '''
    waterper2 = numpy.zeros_like(waterper, dtype='float64')
    waterper2 = numpy.where((waterper > 0) & (waterper < 1), waterper*12*30, waterper2)
    
    waterper_reclass = xarray.DataArray(waterper2,
                                coords={'y': waterper['y'].values,
                                        'x': waterper['x'].values},
                                    dims=['y', 'x'])
    
    # Convert to Dataset and add name
    waterper_wat_cin_ds = waterper_reclass.to_dataset(name="waterper_wat_cin")

    ########## Lifeform ##########
    # Create Dataset
    fc_ann = dc.load(product="fc_percentile_albers_annual", measurements=["PV_PC_90"], **site['query'])
    fc_ann = masking.mask_invalid_data(fc_ann)
    lifeform = copy.deepcopy(fc_ann["PV_PC_90"])
    lifeform.values = numpy.zeros_like(fc_ann["PV_PC_90"], dtype='float64')
    lifeform.values = numpy.where((fc_ann["PV_PC_90"] < 40.) & (fc_ann["PV_PC_90"] >= 0.), 2, lifeform.values)
    lifeform.values = numpy.where((fc_ann["PV_PC_90"] >= 40.) & (fc_ann["PV_PC_90"] <= 100), 1, lifeform.values)

    # Convert to Dataset and add name
    lifeform_veg_cat_ds = lifeform.to_dataset(name="lifeform_veg_cat").squeeze().drop('time')

    
    ########## Canopy Cover ##########
    # Create Dataset
    canopycover = copy.deepcopy(fc_ann["PV_PC_90"])
    canopycover.values = numpy.zeros_like(fc_ann["PV_PC_90"], dtype='float64')
    canopycover.values = numpy.where((fc_ann["PV_PC_90"] <= 100.) & (fc_ann["PV_PC_90"] >= 1.), fc_ann["PV_PC_90"], canopycover.values)

    # Convert to Dataset and add name
    canopyco_veg_con_ds = canopycover.to_dataset(name="canopyco_veg_con").squeeze().drop('time')
    
    
    ########## Level 4 Classification ##########
    print("Applying level 4 classification")
    variables_xarray_list = []
    variables_xarray_list.append(level3_ds)
    variables_xarray_list.append(lifeform_veg_cat_ds)
    variables_xarray_list.append(canopyco_veg_con_ds)
    variables_xarray_list.append(waterper_wat_cin_ds)
    variables_xarray_list.append(waterstt_wat_cat_ds)

    # Merge to a single dataframe
    l4_classification_data = xarray.merge(variables_xarray_list).drop('time')

    # Apply Level 4 classification
    classification_array = lccs_l4.classify_lccs_level4(l4_classification_data)

    # Get Level 4 codes to apply colour scheme
    l4_classification_codes = lccs_l4.get_lccs_level4_code(classification_array)

    # To check the results for level 3 use colour_lccs_level3 to get the colour scheme.
    red, green, blue, alpha = lccs_l4.colour_lccs_level4(l4_classification_codes)
    
    
    # Write out
    output_rgb_file_name = 'level4/'+str(site['AOI'])+'_l4_'+str(time[0])+'.tif'
    out_file_transform = [site['query_geofabric']['target_pixel_size_x'], 0, site['query_geofabric']['target_min_x'], 0, site['query_geofabric']['target_pixel_size_y'], site['query_geofabric']['target_max_y']]
    output_x_size = int((site['query_geofabric']['target_max_x'] - site['query_geofabric']['target_min_x'])/site['query_geofabric']['target_pixel_size_x'])
    output_y_size = int((site['query_geofabric']['target_min_y'] - site['query_geofabric']['target_max_y'])/site['query_geofabric']['target_pixel_size_y'])

    # Write RGB colour scheme out
    rgb_dataset = rasterio.open(output_rgb_file_name, 'w', driver='GTiff',
                                height=output_y_size, width=output_x_size,
                                count=3, dtype=level3.dtype,
                                crs=crs, transform=out_file_transform)
    rgb_dataset.write(red, 1)
    rgb_dataset.write(green, 2)
    rgb_dataset.write(blue, 3)
    rgb_dataset.close()

    
###################################################################################################################    

# No artificial
sites = [Diamantina, Gwydir, Leichhardt, Kakadu, Lake_Eyre, Collier_Range, Mt_Ney]

for site in sites:
    ########## veg/non veg ##########
   
    # need to specify as pq not indexed in lccs_dev
    dc = datacube.Datacube(env='datacube', app="level3")
    
    mask_dict = {'cloud_acca': 'no_cloud',
                 'cloud_fmask': 'no_cloud',
                 'contiguous': True}
    # New vegetation classification
    FC = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                           sensors=['ls5','ls7','ls8'],
                                           product = 'fc',
                                           masked_prop=0.1,
                                           mask_dict=mask_dict)

    # Create a mask to show areas where total vegetation is greater than the baresoil fraction of a pixel for each scene
    tv_mask = FC['BS'] < (FC['PV'] + (FC['NPV'] < 90))
    tv = tv_mask.where(FC['BS'] > 0)
    # Calculate proportion of time where total vegetation is greater than the bare soil fraction of a pixel for the input year
    tv_summary = tv.mean(dim='time')
    tv_thres = tv_summary > 0.167
    
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], **site['query'])
    wofs_ann = masking.mask_invalid_data(wofs_ann)    
    aquatic_wat = 1-((wofs_ann["frequency"] >= 0.9))
    tv_thres = tv_thres * aquatic_wat

    vegetat_veg_cat_ds = tv_thres.to_dataset(name="vegetat_veg_cat").squeeze()
    
    # back to lccs_dev
    dc = datacube.Datacube(env='lccs_dev', app="level3")

    
    ########## aquatic/terrestrial ##########
    # Load data from datacube
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], **site['query'])
    wofs_ann = masking.mask_invalid_data(wofs_ann)

    item = dc.load(product="item_v2", measurements=["relative"], 
                         **site['query'])
    item = masking.mask_invalid_data(item)
    if bool(item) is True:
        item = item.squeeze().drop('time')
    else:
        print("item not available")

    mangrove = dc.load(product="mangrove_extent_cover_albers", measurements=["extent"], 
                         **site['query'])
    if bool(mangrove) is True:
        mangrove = masking.mask_invalid_data(mangrove)
    else:
        print("mangrove not available")

    # Create binary layer representing aquatic (1) and terrestrial (0)

    if bool(item) is True & bool(mangrove) is True:
        aquatic_wat = ((wofs_ann["frequency"] >= 0.2) | ((item["relative"] >= 2) & (item["relative"] <= 8)) | (mangrove["extent"] == 1))
    elif bool(item) is True:
        aquatic_wat = ((wofs_ann["frequency"] >= 0.2) | ((item["relative"] >= 2) & (item["relative"] <= 8)))
    elif bool(mangrove) is True:
        aquatic_wat = ((wofs_ann["frequency"] >= 0.2) | (mangrove["extent"] == 1))
    else:
        aquatic_wat = ((wofs_ann["frequency"] >= 0.2))

    # Convert to Dataset and add name
    aquatic_wat_cat_ds = aquatic_wat.to_dataset(name="aquatic_wat_cat").squeeze().drop('time')             

    
    ########## cultman/natural ##########
    tmad = dc.load(product=sensor +"_nbart_tmad_annual", **site['query'])
    tmad = tmad.isel(time=0)
    tmad = tmad.drop('time')
    
    # Impute missing values (0 and NaN)
    imp_0 = SimpleImputer(missing_values=0, strategy='mean')
    container = {}
    for key in tmad.data_vars:
        d = tmad[key].data.squeeze()
        d = numpy.nan_to_num(d)
        d = numpy.where(d < 0, 0, d)
        d = numpy.where(d == 1, 0, d)
        imp_0.fit(d)
        d = imp_0.transform(d)
        d = -numpy.log(d)
        container.update({key: d})
    tmad['edev'].data = container['edev']
    tmad['sdev'].data = container['sdev']
    tmad['bcdev'].data = container['bcdev']
    
    # Calculate the mean of all the tmad inputs
    tmad_mean = numpy.mean(numpy.stack([tmad.edev.data, tmad.sdev.data, tmad.bcdev.data], axis=-1), axis=-1)
    # Convert type to float64 (required for quickshift)
    tmad_mean = numpy.float64(tmad_mean)
    # Segment
    tmad_seg = quickshift(tmad_mean, kernel_size=2, convert2lab=False, max_dist=500, ratio=0.5)
    # Calculate the median for each segment
    tmad_median_seg = scipy.ndimage.median(input=tmad_mean, labels=tmad_seg, index=tmad_seg)
    # Set threshold as 10th percentile of mean TMAD
    thresh = numpy.percentile(tmad_mean.ravel(), 20)
    # Create boolean layer using threshold
    tmad_thresh = tmad_median_seg < thresh
    cultman_agr_cat_ds = xarray.Dataset({'cultman_agr_cat': (tmad.dims, tmad_thresh)}, coords=tmad.coords)
    
    ########## artsurface/natural ##########
    dc = datacube.Datacube(env='datacube', app="level3")  
    LS = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], 
                                       sensors=['ls5','ls7','ls8'],
                                       masked_prop=0.1,
                                       mask_dict=mask_dict)
    
    TCe = TasseledCapTools.pct_exceedance_tasseled_cap(LS, tc_bands=['greenness','brightness', 'wetness'],
                                               greenness_threshold=-500, 
                                               brightness_threshold=5000, 
                                               wetness_threshold=-2500, 
                                               drop=True, drop_tc_bands=True)
    TCe_thresG = TCe.greenness_pct_exceedance < 0.1
    TCe_thresB = TCe.brightness_pct_exceedance < 0.1
    TCe_thresW = TCe.wetness_pct_exceedance > 0.9
    TCE = TCe_thresG | TCe_thresB | TCe_thresW
    urban  = TCE * 0
    artific_urb_cat_ds = urban.to_dataset(name="artific_urb_cat").squeeze()
    

    ########## artwater/natural water ##########
    # Load data
    var_file = "/g/data/u46/wofs/confidence/geoFabric/geofabric.tif"
    var_name = "features"

    import_obj = gridded_ingest.LEIngestGDAL(**site['query_geofabric'])
    geofabric = import_obj.read_to_xarray(var_file, var_name)

    # Create binary layer representing artificial water (1) and natural water (0)
    artwatr_wat_cat = ((geofabric["features"] == 1) | (geofabric["features"] == 8))

    # Convert to Dataset and add name
    artwatr_wat_cat_ds = artwatr_wat_cat.to_dataset(name="artwatr_wat_cat")
    
    
    ########## LCCS classification ##########
    variables_xarray_list = []
    variables_xarray_list.append(vegetat_veg_cat_ds)
    variables_xarray_list.append(aquatic_wat_cat_ds)
    variables_xarray_list.append(cultman_agr_cat_ds)
    variables_xarray_list.append(artific_urb_cat_ds)
    variables_xarray_list.append(artwatr_wat_cat_ds)

    # Merge to a single dataframe
    classification_data = xarray.merge(variables_xarray_list)

    # Apply Level 3 classification using separate function. Works through in three stages
    level1, level2, level3 = lccs_l3.classify_lccs_level3(classification_data)

    # Save classification values back to xarray
    out_class_xarray = xarray.Dataset(
        {"level1" : (classification_data["vegetat_veg_cat"].dims, level1),
         "level2" : (classification_data["vegetat_veg_cat"].dims, level2),
         "level3" : (classification_data["vegetat_veg_cat"].dims, level3)})
    classification_data = xarray.merge([classification_data, out_class_xarray])

    # Convert level3 to Dataset and add name
    level3_ds = classification_data.level3.to_dataset(name="level3")

    print("Reading in data for level 4 classification")
    
    
    ########## Water State ##########
    waterstt_wat_cat_ds = aquatic_wat.to_dataset(name="waterstt_wat_cat").squeeze().drop('time')

    
    ########## Water Persistence ##########
    # Load data from datacube
    waterper = wofs_ann.frequency.squeeze().drop('time')

    '''
    WOfs does not provide hydroperiod in days, best alternative is frequency (wet/dry obs)
    which have been estimated based on months/year

    0.74 - 1    --> 1 : ("B1", "Perennial (> 9 months)"),
    0.58 - 0.74 --> 7 : ("B7", "Non-perennial (7 to 9 months)")
    0.3 - 0.58  --> 8 : ("B8", "Non-perennial (4 to 7 months)")
    0 - 0.3     --> 9 : ("B9", "Non-perennial (1 to 4 months)")

    '''
    waterper2 = numpy.zeros_like(waterper, dtype='float64')
    waterper2 = numpy.where((waterper > 0) & (waterper < 1), waterper*12*30, waterper2)
    
    waterper_reclass = xarray.DataArray(waterper2,
                                coords={'y': waterper['y'].values,
                                        'x': waterper['x'].values},
                                    dims=['y', 'x'])
    
    # Convert to Dataset and add name
    waterper_wat_cin_ds = waterper_reclass.to_dataset(name="waterper_wat_cin")

    ########## Lifeform ##########
    # Create Dataset
    fc_ann = dc.load(product="fc_percentile_albers_annual", measurements=["PV_PC_90"], **site['query'])
    fc_ann = masking.mask_invalid_data(fc_ann)
    lifeform = copy.deepcopy(fc_ann["PV_PC_90"])
    lifeform.values = numpy.zeros_like(fc_ann["PV_PC_90"], dtype='float64')
    lifeform.values = numpy.where((fc_ann["PV_PC_90"] < 40.) & (fc_ann["PV_PC_90"] >= 0.), 2, lifeform.values)
    lifeform.values = numpy.where((fc_ann["PV_PC_90"] >= 40.) & (fc_ann["PV_PC_90"] <= 100), 1, lifeform.values)

    # Convert to Dataset and add name
    lifeform_veg_cat_ds = lifeform.to_dataset(name="lifeform_veg_cat").squeeze().drop('time')

    
    ########## Canopy Cover ##########
    # Create Dataset
    canopycover = copy.deepcopy(fc_ann["PV_PC_90"])
    canopycover.values = numpy.zeros_like(fc_ann["PV_PC_90"], dtype='float64')
    canopycover.values = numpy.where((fc_ann["PV_PC_90"] <= 100.) & (fc_ann["PV_PC_90"] >= 1.), fc_ann["PV_PC_90"], canopycover.values)

    # Convert to Dataset and add name
    canopyco_veg_con_ds = canopycover.to_dataset(name="canopyco_veg_con").squeeze().drop('time')
    
    
    ########## Level 4 Classification ##########
    print("Applying level 4 classification")
    variables_xarray_list = []
    variables_xarray_list.append(level3_ds)
    variables_xarray_list.append(lifeform_veg_cat_ds)
    variables_xarray_list.append(canopyco_veg_con_ds)
    variables_xarray_list.append(waterper_wat_cin_ds)
    variables_xarray_list.append(waterstt_wat_cat_ds)

    # Merge to a single dataframe
    l4_classification_data = xarray.merge(variables_xarray_list).drop('time')

    # Apply Level 4 classification
    classification_array = lccs_l4.classify_lccs_level4(l4_classification_data)

    # Get Level 4 codes to apply colour scheme
    l4_classification_codes = lccs_l4.get_lccs_level4_code(classification_array)

    # To check the results for level 3 use colour_lccs_level3 to get the colour scheme.
    red, green, blue, alpha = lccs_l4.colour_lccs_level4(l4_classification_codes)
    
    
    # Write out
    output_rgb_file_name = 'level4/'+str(site['AOI'])+'_l4_'+str(time[0])+'.tif'
    out_file_transform = [site['query_geofabric']['target_pixel_size_x'], 0, site['query_geofabric']['target_min_x'], 0, site['query_geofabric']['target_pixel_size_y'], site['query_geofabric']['target_max_y']]
    output_x_size = int((site['query_geofabric']['target_max_x'] - site['query_geofabric']['target_min_x'])/site['query_geofabric']['target_pixel_size_x'])
    output_y_size = int((site['query_geofabric']['target_min_y'] - site['query_geofabric']['target_max_y'])/site['query_geofabric']['target_pixel_size_y'])

    # Write RGB colour scheme out
    rgb_dataset = rasterio.open(output_rgb_file_name, 'w', driver='GTiff',
                                height=output_y_size, width=output_x_size,
                                count=3, dtype=level3.dtype,
                                crs=crs, transform=out_file_transform)
    rgb_dataset.write(red, 1)
    rgb_dataset.write(green, 2)
    rgb_dataset.write(blue, 3)
    rgb_dataset.close()
