#!/bin/bash

#PBS -N level4
#PBS -P r78
#PBS -q normalbw
#PBS -M cho18@aber.ac.uk
#PBS -m abe
#PBS -l walltime=10:00:00
#PBS -l mem=256GB
#PBS -l ncpus=16
#PBS -l wd

module use /g/data/v10/public/modules/modulefiles
module load dea
DATACUBE_CONFIG_PATH="/home/574/co6850/lccs_dev.conf"
datacube -E lccs_dev 
python level4.py