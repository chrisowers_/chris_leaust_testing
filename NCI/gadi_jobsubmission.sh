#!/bin/bash

#PBS -N l4_vp_template_gadi
#PBS -o l4_vp_template_gadi.out
#PBS -e l4_vp_template_gadi.err
#PBS -P r78
#PBS -q express
#PBS -M cho18@aber.ac.uk
#PBS -m abe
#PBS -l storage=gdata/v10+gdata/r78+gdata/u46+gdata/rs0+gdata/fk4+gdata/r78
#PBS -l walltime=00:20:00
#PBS -l mem=16GB
#PBS -l ncpus=1
#PBS -l wd

export LE_LCCS_PLUGINS_PATH=/g/data/r78/LCCS_Aberystwyth/co6850/livingearth_australia/le_plugins
export PYTHONPATH=/g/data/r78/LCCS_Aberystwyth/co6850/dea-notebooks/Scripts:/g/data/r78/LCCS_Aberystwyth/co6850/livingearth_australia:$PYTHONPATH

module use /g/data/v10/public/modules/modulefiles
module load dea

cd /g/data/r78/LCCS_Aberystwyth/co6850/livingearth_lccs/bin
python3 le_lccs_odc.py ../../livingearth_australia/templates/l4_vp_template_gadi.yaml
