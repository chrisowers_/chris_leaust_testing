import os, sys, copy
import yaml
import numpy as np
import xarray as xr
import pickle

from itertools import groupby
from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec

import datacube
from datacube.storage import masking
from datacube.virtual import catalog_from_file
from datacube.utils.geometry import CRS
from datacube.helpers import write_geotiff

dc = datacube.Datacube()

# AWS or Gadi
service = '/home/jovyan/development/'

sys.path.append(str(service)+"dea-notebooks/Scripts")
from dea_classificationtools import sklearn_unflatten
from dea_classificationtools import sklearn_flatten

sys.path.append(str(service)+"livingearth_lccs")
from le_lccs.le_ingest import gridded_ingest

sys.path.append(os.path.abspath(str(service)+"livingearth_australia/le_plugins"))

# loading extents from yaml Dan prepared, just change site_name to tile interested in from our 16 test sites
yaml_sites_file = os.path.abspath(str(service)+"livingearth_australia/templates/au_test_sites.yaml")

with open(yaml_sites_file, "r") as f:
    site_config = yaml.safe_load(f)

site_name = "Collier Range"
extent_x = [site_config[site_name]["min_x"],
            site_config[site_name]["max_x"]]
extent_y = [site_config[site_name]["min_y"],
            site_config[site_name]["max_y"]]

crs = "EPSG:3577"
res = (-25, 25)
time = ("2015-01-01", "2015-12-31")
query =({'time': time,
             'x':extent_x,
            'y':extent_y,
            'crs':crs,
            'resolution':res})

# datacube functions to load in a virtual product from recipe
catalog = catalog_from_file(service+'livingearth_australia/le_plugins/virtual_product_cat.yaml')

import importlib
from datacube.virtual import DEFAULT_RESOLVER

sys.path.append(str(service)+"livingearth_australia")
import le_plugins

# FC_summary
# Get location of transformation
transformation = "le_plugins.FC_summary"
trans_loc = importlib.import_module(transformation)
trans_class = transformation.split('.')[-1]
DEFAULT_RESOLVER.register('transform', trans_class, getattr(trans_loc, trans_class))

product = catalog['fc_veg']
fc_veg = product.load(dc, **query)

print(fc_veg)

print('unique values for '+str(site_name), np.unique(fc_veg.fc_veg))