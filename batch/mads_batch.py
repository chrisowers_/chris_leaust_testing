import os
import datacube
from datacube.storage import masking
from datacube.helpers import write_geotiff
dc = datacube.Datacube(env="lccs_dev", app="fc")

os.makedirs('./mads_outs', exist_ok=True)

# Ayr 2010
x = (1500000, 1600000)
y = (-2200000, -2100000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Ayr_mads_2010.tif', dataset=mads)
print("Ayr_mads_2010.tif")

# Ayr 2015
x = (1500000, 1600000)
y = (-2200000, -2100000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Ayr_mads_2015.tif', dataset=mads)
print("Ayr_mads_2015.tif")

###############################

# Diamentina 2010
x = (800000, 900000)
y = (-2800000, -2700000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Diamentina_mads_2010.tif', dataset=mads)
print("Diamentina_mads_2010.tif")

# Diamentina 2015
x = (800000, 900000)
y = (-2800000, -2700000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Diamentina_mads_2015.tif', dataset=mads)
print("Diamentina_mads_2015.tif")

###############################

# Gwydir 2010
x = (1600000, 1700000)
y = (-3400000, -3300000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Gwydir_mads_2010.tif', dataset=mads)
print("Gwydir_mads_2010.tif")

# Gwydir 2015
x = (1600000, 1700000)
y = (-3400000, -3300000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Gwydir_mads_2015.tif', dataset=mads)
print("Gwydir_mads_2015.tif")

###############################

# Leichhardt 2010
x = (800000, 900000)
y = (-2000000, -1900000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Leichhardt_mads_2010.tif', dataset=mads)
print("Leichhardt_mads_2010.tif")

# Leichhardt 2015
x = (800000, 900000)
y = (-2000000, -1900000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Leichhardt_mads_2015.tif', dataset=mads)
print("Leichhardt_mads_2015.tif")

###############################

# Kakadu 2010
x = (0, 100000)
y = (-1350000, -1250000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Kakadu_mads_2010.tif', dataset=mads)
print("Kakadu_mads_2010.tif")

# Kakadu 2015
x = (0, 100000)
y = (-1350000, -1250000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Kakadu_mads_2015.tif', dataset=mads)
print("Kakadu_mads_2015.tif")

###############################

# Hobart 2010
x = (1200000, 1300000)
y = (-4800000, -4700000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Hobart_mads_2010.tif', dataset=mads)
print("Hobart_mads_2010.tif")

# Hobart 2015
x = (1200000, 1300000)
y = (-4800000, -4700000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Hobart_mads_2015.tif', dataset=mads)
print("Hobart_mads_2015.tif")

###############################

# Perth 2010
x = (-1550000, -1450000)
y = (-3650000, -3550000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Perth_mads_2010.tif', dataset=mads)
print("Perth_mads_2010.tif")

# Perth 2015
x = (-1550000, -1450000)
y = (-3650000, -3550000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Perth_mads_2015.tif', dataset=mads)
print("Perth_mads_2015.tif")

###############################

# Murray_Valley 2010
x = (1100000, 1200000)
y = (-4000000, -3900000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Murray_Valley_mads_2010.tif', dataset=mads)
print("Murray_Valley_mads_2010.tif")

# Murray Valley 2015
x = (1100000, 1200000)
y = (-4000000, -3900000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Murray_Valley_mads_2015.tif', dataset=mads)
print("Murray_Valley_mads_2015.tif")

###############################

# Adelaide 2010
x = (550000, 650000)
y = (-3850000, -3750000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Adelaide_mads_2010.tif', dataset=mads)
print("Adelaide_mads_2010.tif")

# Adelaide 2015
x = (550000, 650000)
y = (-3850000, -3750000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Adelaide_mads_2015.tif', dataset=mads)
print("Adelaide_mads_2015.tif")

###############################

# Lake_Eyre 2010
x = (500000, 600000)
y = (-3000000, -2900000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Lake_Eyre_mads_2010.tif', dataset=mads)
print("Lake_Eyre_mads_2010.tif")

# Lake_Eyre 2015
x = (500000, 600000)
y = (-3000000, -2900000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Lake_Eyre_mads_2015.tif', dataset=mads)
print("Lake_Eyre_mads_2015.tif")

###############################

# Blue_Mtns 2010
x = (1600000, 1700000)
y = (-3900000, -3800000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Blue_Mtns_mads_2010.tif', dataset=mads)
print("Blue_Mtns_mads_2010.tif")

# Blue_Mtns 2015
x = (1600000, 1700000)
y = (-3900000, -3800000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Blue_Mtns_mads_2015.tif', dataset=mads)
print("Blue_Mtns_mads_2015.tif")

###############################

# Aust_Alps 2010
x = (1400000, 1500000)
y = (-4100000, -4000000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Aust_Alps_mads_2010.tif', dataset=mads)
print("Aust_Alps_mads_2010.tif")

# Aust_Alps 2015
x = (1400000, 1500000)
y = (-4100000, -4000000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Aust_Alps_mads_2015.tif', dataset=mads)
print("Aust_Alps_mads_2015.tif")

###############################

# Collier_Range 2010
x = (-1300000, -1200000)
y = (-2700000, -2600000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Collier_Range_mads_2010.tif', dataset=mads)
print("Collier_Range_mads_2010.tif")

# Collier_Range 2015
x = (-1300000, -1200000)
y = (-2700000, -2600000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Collier_Range_mads_2015.tif', dataset=mads)
print("Collier_Range_mads_2015.tif")

###############################

# Coorong 2010
x = (600000, 700000)
y = (-3950000, -3850000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Coorong_mads_2010.tif', dataset=mads)
print("Coorong_mads_2010.tif")

# Coorong 2015
x = (600000, 700000)
y = (-3950000, -3850000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Coorong_mads_2015.tif', dataset=mads)
print("Coorong_mads_2015.tif")

###############################

# Brisbane 2010
x = (2000000, 2100000)
y = (-3200000, -3100000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Brisbane_mads_2010.tif', dataset=mads)
print("Brisbane_mads_2010.tif")

# Brisbane 2015
x = (2000000, 2100000)
y = (-3200000, -3100000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Brisbane_mads_2015.tif', dataset=mads)
print("Brisbane_mads_2015.tif")

###############################

# Mt_Ney 2010
x = (-1000000, -900000)
y = (-3650000, -3550000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Mt_Ney_mads_2010.tif', dataset=mads)
print("Mt_Ney_mads_2010.tif")

# Mt_Ney 2015
x = (-1000000, -900000)
y = (-3650000, -3550000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')
write_geotiff(filename='mads_outs/Mt_Ney_mads_2015.tif', dataset=mads)
print("Mt_Ney_mads_2015.tif")
