#!/usr/bin/env python3
"""
Simple script to run LCCS classification for multiple sites
with sites defined using a yaml file.

Dan Clewley, edited by Chris Owers based on Belle Tissot work
2020-06-04


must be executed from home/jovyan/development/livingearth_lccs/bin/
example$ python3 run_test_sites_l4.py -o test_level4 --year 2015 --level 4

"""

import argparse
import datetime
import logging
import os
import sys
import subprocess
import yaml

# current working directory for outputs
output_dir = os.getcwd()

# Set up default input if not specifed in kwargs
YAML_SITES_FILE = os.path.abspath("/home/jovyan/development/livingearth_lccs_development_tests/notebooks/cultivated_ml/au_test_sites.yaml")

LCCS_CONFIG_FILE_L3 = os.path.abspath("/home/jovyan/development/livingearth_australia/templates/l3_vp_template_.yaml")
LCCS_CONFIG_FILE_L4 = os.path.abspath("/home/jovyan/development/livingearth_australia/templates/l4_vp_template.yaml")

LE_RUN = os.path.abspath("/home/jovyan/development/livingearth_lccs/bin/le_lccs_odc.py")

OUTPUT_FOLDER_NAME = "test"

year = "2015"


def update_config_yaml_l3(year, config_template, out_config):
    '''
    using the original config template to adjust the year as required
    
    year: kwargs
    config_template: original LCCS config file (using as a template for new config)
    out_config: new config file for LCCS tests
    
    '''
    with open(config_template, "r") as f:
        new_config = yaml.safe_load(f)    

    # update start and end times
    start = f"{year}-01-01"
    end = f"{year}-12-31"

    # list of l3 layers which need start & end dates
    layers = ['fc_veg', 'wcf_mask', 'cultman_agr_cat', 'tf_urban_classification', 
              'wofs_mask', 'item_v2_mask', 'mangrove']

    for layer in layers:
        new_config['L3layers'][layer]['start_time'] = start
        new_config['L3layers'][layer]['end_time'] = end

    # write out new config yaml
    with open(out_config, 'w') as outfile:
        outfile.write(yaml.dump(new_config))
        outfile.flush()
        outfile.close()

def update_config_yaml_l4(year, config_template, out_config):
    '''
    using the original config template to adjust the year as required
    
    year: kwargs
    config_template: original LCCS config file (using as a template for new config)
    out_config: new config file for LCCS tests
    
    '''
    with open(config_template, "r") as f:
        new_config = yaml.safe_load(f)    

    # update start and end times
    start = f"{year}-01-01"
    end = f"{year}-12-31"

    # list of l3 layers which need start & end dates
    layers = ['fc_veg', 'wcf_mask', 'cultman_agr_cat', 'tf_urban_classification', 
              'wofs_mask', 'item_v2_mask', 'mangrove']

    for layer in layers:
        new_config['L3layers'][layer]['start_time'] = start
        new_config['L3layers'][layer]['end_time'] = end

    # list of l4 layers which need start & end dates
    layers = ['lifeform_veg_cat', 'canopyco_veg_con', 'wofs_mask', 
              'item_v2_mask', 'waterper_wat_cin', 'watersea_veg_cat']
    
    for layer in layers:
        new_config['L4layers'][layer]['start_time'] = start
        new_config['L4layers'][layer]['end_time'] = end

    # write out new config yaml
    with open(out_config, 'w') as outfile:
        outfile.write(yaml.dump(new_config))
        outfile.flush()
        outfile.close()


def run_all_sites_l3(year, output_folder_name, output_folder, config_file, yaml_sites_file): 
    '''
    run through all sites from a yaml specifying the sites and extents.
    requires a yaml file as a foundation, created in the function update_config_yaml
    
    year: kwargs
    output_folder_name: kwargs
    output_folder: basename + output_folder_name
    config_file: yaml file created from function update_config_yaml
    yaml_sites_file: yaml specifying the sites and extents
    '''
    # Read in site extents
    with open(yaml_sites_file, "r") as f:
        sites_config = yaml.safe_load(f)

    # Run for all sites
    print("*** {} ***".format(year))
    for i, site_name in enumerate(sites_config.keys()):

        print("[{:02}/{:02}] {}".format(i+1, len(sites_config.keys()),site_name))

        site_extent = [sites_config[site_name]["min_x"],
                       sites_config[site_name]["min_y"],
                       sites_config[site_name]["max_x"],
                       sites_config[site_name]["max_y"]]

        today_str = datetime.datetime.strftime(datetime.datetime.now(), "%Y%m%d")

        out_name_base = "lccs_tests_{}_{}_{}_{}".format(today_str, output_folder_name, site_name.replace(" ","_"), year)
        output_l3_file = os.path.join(output_folder, "{}_level3.tif".format(out_name_base))
        output_l3_rgb_file = os.path.join(output_folder, "{}_level3_rgb.tif".format(out_name_base))

        # If output file exists, skip
        if os.path.isfile(output_l3_rgb_file):
            continue

        run_cmd = ["python3", LE_RUN,
                   "--log", logfile,
                   "--output_l3_file_name", output_l3_file,
                   "--output_l3_rgb_file_name", output_l3_rgb_file,
                   "--extent"]
        run_cmd.extend([str(i) for i in site_extent])
        run_cmd.append(config_file)
        print(" ".join(run_cmd))
        subprocess.call(run_cmd)

def run_all_sites_l4(year, output_folder_name, output_folder, config_file, yaml_sites_file): 
    '''
    run through all sites from a yaml specifying the sites and extents.
    requires a yaml file as a foundation, created in the function update_config_yaml
    
    year: kwargs
    output_folder_name: kwargs
    output_folder: basename + output_folder_name
    config_file: yaml file created from function update_config_yaml
    yaml_sites_file: yaml specifying the sites and extents
    '''
    # Read in site extents
    with open(yaml_sites_file, "r") as f:
        sites_config = yaml.safe_load(f)

    # Run for all sites
    print("*** {} ***".format(year))
    for i, site_name in enumerate(sites_config.keys()):

        print("[{:02}/{:02}] {}".format(i+1, len(sites_config.keys()),site_name))

        site_extent = [sites_config[site_name]["min_x"],
                       sites_config[site_name]["min_y"],
                       sites_config[site_name]["max_x"],
                       sites_config[site_name]["max_y"]]

        today_str = datetime.datetime.strftime(datetime.datetime.now(), "%Y%m%d")

        out_name_base = "lccs_tests_{}_{}_{}_{}".format(today_str, output_folder_name, site_name.replace(" ","_"), year)
        output_l3_file = os.path.join(output_folder, "{}_level3.tif".format(out_name_base))
        output_l3_rgb_file = os.path.join(output_folder, "{}_level3_rgb.tif".format(out_name_base))
        output_l4_file = os.path.join(output_folder, "{}_level4.tif".format(out_name_base))
        output_l4_rgb_file = os.path.join(output_folder, "{}_level4_rgb.tif".format(out_name_base))

        # If output file exists, skip
        if os.path.isfile(output_l4_rgb_file):
            continue

        run_cmd = ["python3", LE_RUN,
                   "--log", logfile,
                   "--output_l3_file_name", output_l3_file,
                   "--output_l3_rgb_file_name", output_l3_rgb_file,
                   "--output_l4_file_name", output_l4_file,
                   "--output_l4_rgb_file_name", output_l4_rgb_file,
                   "--extent"]
        run_cmd.extend([str(i) for i in site_extent])
        run_cmd.append(config_file)
        print(" ".join(run_cmd))
        subprocess.call(run_cmd)        

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Run LE LCCS Classification "
                                                 "for multiple sites",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-o", "--outdir",
                        type=str,
                        required=True,
                        help="Directory for output files, name something appropriate for current test")
    parser.add_argument("--year",
                        type=str,
                        required=True,
                        help="Year of analysis (i.e. 2015)")
    parser.add_argument("-l", "--level",
                        type=str,
                        required=True,
                        help="Level 3 or Level 4 classification? Just enter number.")
    parser.add_argument("--config",
                        type=str,
                        required=False,
                        help="Config file template for LCCS classification, ensure this matches the selected LCCS level classifcation.")
    parser.add_argument("--sites_config",
                        type=str,
                        required=False,
                        default=YAML_SITES_FILE,
                        help="Yaml file containing site extents")
    args = parser.parse_args()

    # base folder/file names if not specified in kwargs
    today_str = datetime.datetime.strftime(datetime.datetime.now(), "%Y%m%d")
    out_base_name = "lccs_{}_{}_{}".format(today_str, args.outdir, year)

    # create output folder for results
    OUTPUT_FOLDER = os.path.join(output_dir, out_base_name)
    os.makedirs(OUTPUT_FOLDER, exist_ok=True)

    if args.level == "3":
        # out for config yaml
        OUT_CONFIG = OUTPUT_FOLDER+"/"+out_base_name+"l3_config.yaml"

        # Set up logging
        logging.basicConfig(stream=sys.stdout, level=logging.INFO)
        # out for logging txt file
        logfile = OUTPUT_FOLDER+"/"+out_base_name+"l3_log.txt"

        # if config isn't specified, use default level 3 yaml template
        if args.config is None:
            update_config_yaml_l3(year=args.year, config_template=LCCS_CONFIG_FILE_L3, 
                       out_config=OUT_CONFIG)
            
            run_all_sites_l3(year=args.year, output_folder_name=args.outdir,
                  output_folder=OUTPUT_FOLDER, config_file=OUT_CONFIG,
                  yaml_sites_file=args.sites_config)
        
        else:
            update_config_yaml_l3(year=args.year, config_template=args.config, 
                       out_config=OUT_CONFIG)
            
            run_all_sites_l3(year=args.year, output_folder_name=args.outdir,
                  output_folder=OUTPUT_FOLDER, config_file=OUT_CONFIG,
                  yaml_sites_file=args.sites_config)          

    if args.level == "4":
        # out for config yaml
        OUT_CONFIG = OUTPUT_FOLDER+"/"+out_base_name+"l4_config.yaml"

        # Set up logging
        logging.basicConfig(stream=sys.stdout, level=logging.INFO)
        # out for logging txt file
        logfile = OUTPUT_FOLDER+"/"+out_base_name+"l4_log.txt"

        # if config isn't specified, use default level 4 yaml template
        if args.config is None:
            update_config_yaml_l4(year=args.year, config_template=LCCS_CONFIG_FILE_L4, 
                       out_config=OUT_CONFIG)
            
            run_all_sites_l4(year=args.year, output_folder_name=args.outdir,
                  output_folder=OUTPUT_FOLDER, config_file=OUT_CONFIG,
                  yaml_sites_file=args.sites_config)
        
        else:
            update_config_yaml_l4(year=args.year, config_template=args.config, 
                       out_config=OUT_CONFIG)
            
            run_all_sites_l4(year=args.year, output_folder_name=args.outdir,
                  output_folder=OUTPUT_FOLDER, config_file=OUT_CONFIG,
                  yaml_sites_file=args.sites_config)            
