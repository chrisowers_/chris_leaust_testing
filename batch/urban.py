""" script to output derived urban indices to geotiff
"""

import os
import numpy as np
import datacube
from datacube.storage import masking
from datacube.helpers import write_geotiff
from datacube.utils.geometry import CRS
dc = datacube.Datacube(env="lccs_dev", app="urban")

os.makedirs('./urban', exist_ok=True)


###############################
# # Ayr 2010
# x = (1500000, 1600000)
# y = (-2200000, -2100000)

# # Diamantina 2010
# x = (800000, 900000)
# y = (-2800000, -2700000)

# # Gwydir 2010
# x = (1600000, 1700000)
# y = (-3400000, -3300000)

# # Leichhardt 2010
# x = (800000, 900000)
# y = (-2000000, -1900000)

# # Kakadu 2010
# x = (0, 100000)
# y = (-1350000, -1250000)

# # Hobart 2010
# x = (1200000, 1300000)
# y = (-4800000, -4700000)

# # Perth 2015
# x = (-1550000, -1450000)
# y = (-3650000, -3550000)

# # Murray_Valley 2010
# x = (1100000, 1200000)
# y = (-4000000, -3900000)

# Adelaide 2015
x = (550000, 650000)
y = (-3850000, -3750000)

# # Lake_Eyre 2010
# x = (500000, 600000)
# y = (-3000000, -2900000)

# # Blue_Mtns 2010
# x = (1600000, 1700000)
# y = (-3900000, -3800000)

# # Aust_Alps 2010
# x = (1400000, 1500000)
# y = (-4100000, -4000000)

# # Collier_Range 2010
# x = (-1300000, -1200000)
# y = (-2700000, -2600000)

# # Coorong 2010
# x = (600000, 700000)
# y = (-3950000, -3850000)

# # Brisbane 2010
# x = (2000000, 2100000)
# y = (-3200000, -3100000)

# # Mt_Ney 2010
# x = (-1000000, -900000)
# y = (-3650000, -3550000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

# MAD_edev
# mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
#                time=time, **query)
# mads = masking.mask_invalid_data(mads).squeeze().drop('time')

# edev=(mads["edev"])
# log_edev = np.log(1/edev)
# log_edev = log_edev.to_dataset(name="array")
# log_edev.attrs['crs'] = CRS(mads.crs)
# write_geotiff(filename='urban/MAD_edev_normalised.tif', dataset=log_edev)
# print("MAD_edev_normalised.tif")

# sdev=(mads["sdev"])
# log_sdev = np.log(1/sdev)
# log_sdev = log_sdev.to_dataset(name="array")
# log_sdev.attrs['crs'] = CRS(mads.crs)
# write_geotiff(filename='urban/MAD_sdev_normalised.tif', dataset=log_sdev)
# print("MAD_sdev_normalised.tif")

# bcdev=(mads["bcdev"])
# log_bcdev = np.log(1/bcdev)
# log_bcdev = log_bcdev.to_dataset(name="array")
# log_bcdev.attrs['crs'] = CRS(mads.crs)
# write_geotiff(filename='urban/MAD_bcdev_normalised.tif', dataset=log_bcdev)
# print("MAD_bcdev_normalised.tif")

geomedian = dc.load(product=sensor + "_nbart_geomedian_annual", time=time, **query)
geomedian = masking.mask_invalid_data(geomedian).squeeze().drop('time')

write_geotiff(filename='urban/Adelaide_geomedian_2015.tif', dataset=geomedian)
print("Adelaide_geomedian_2015.tif")

# # Normalised difference built-up index
# NDBI = ((geomedian.swir1 - geomedian.nir) / (geomedian.swir1 + geomedian.nir))
# # write to geotiff - change XX.to_dataset
# NDBI = NDBI.to_dataset(name="array")
# NDBI.attrs['crs'] = CRS(geomedian.crs)
# write_geotiff(filename='urban/NDBI.tif', dataset=NDBI)
# print("NDBI.tif")

# # Tassel cap brightness
# TCB = ((0.30378*geomedian.blue) + (0.2793*geomedian.green) + (0.4743*geomedian.red) + (0.5585*geomedian.nir) + (0.5082*geomedian.swir1) + (0.1863*geomedian.swir2))
# # write to geotiff - change XX.to_dataset
# TCB = TCB.to_dataset(name="array")
# TCB.attrs['crs'] = CRS(geomedian.crs)
# write_geotiff(filename='urban/TCB.tif', dataset=TCB)
# print("TCB.tif")

# # BRGI - Richards suggestion
# BRGI = ((geomedian.red) + (geomedian.blue) + (geomedian.green) + (geomedian.nir))/4
# # write to geotiff - change XX.to_dataset
# BRGI = BRGI.to_dataset(name="array")
# BRGI.attrs['crs'] = CRS(geomedian.crs)
# write_geotiff(filename='urban/BRGI.tif', dataset=BRGI)
# print("BRGI.tif")

# # Modified normalised difference water index
# MNDWI = (geomedian.green - geomedian.swir1) / (geomedian.green + geomedian.swir1)
# # write to geotiff - change XX.to_dataset
# MNDWI = MNDWI.to_dataset(name="array")
# MNDWI.attrs['crs'] = CRS(geomedian.crs)
# write_geotiff(filename='urban/MNDWI.tif', dataset=MNDWI)
# print("MNDWI.tif")


