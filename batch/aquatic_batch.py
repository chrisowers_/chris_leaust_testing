import os
import datacube
from datacube.storage import masking
from datacube.helpers import write_geotiff
dc = datacube.Datacube(app="aquatic")

os.makedirs('./aquatic_outs', exist_ok=True)

# Ayr 2010
x = (1500000, 1600000)
y = (-2200000, -2100000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Ayr_wofs_2010.tif', dataset=wofs_ann)
print("Ayr_wofs_2010.tif")

# Ayr 2015
x = (1500000, 1600000)
y = (-2200000, -2100000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Ayr_wofs_2015.tif', dataset=wofs_ann)
print("Ayr_wofs_2015.tif")

# Ayr ITEMs
item = dc.load(product="item_v2", measurements=["relative"], 
                     time=time, **query)
item = item.squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Ayr_ITEM.tif', dataset=item)
print("Ayr_ITEM.tif")

###############################

# Diamentina 2010
x = (800000, 900000)
y = (-2800000, -2700000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Diamentina_wofs_2010.tif', dataset=wofs_ann)
print("Diamentina_wofs_2010.tif")

# Diamentina 2015
x = (800000, 900000)
y = (-2800000, -2700000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Diamentina_wofs_2015.tif', dataset=wofs_ann)
print("Diamentina_wofs_2015.tif")

# # Diamentina ITEMs
# item = dc.load(product="item_v2", measurements=["relative"], 
#                      time=time, **query)
# item = item.squeeze().drop('time')
# write_geotiff(filename='aquatic_outs/Diamentina_ITEM.tif', dataset=item)
# print("Diamentina_ITEM.tif")

###############################

# Gwydir 2010
x = (1600000, 1700000)
y = (-3400000, -3300000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Gwydir_wofs_2010.tif', dataset=wofs_ann)
print("Gwydir_wofs_2010.tif")

# Gwydir 2015
x = (1600000, 1700000)
y = (-3400000, -3300000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Gwydir_wofs_2015.tif', dataset=wofs_ann)
print("Gwydir_wofs_2015.tif")

# # Gwydir ITEMs
# item = dc.load(product="item_v2", measurements=["relative"], 
#                      time=time, **query)
# item = item.squeeze().drop('time')
# write_geotiff(filename='aquatic_outs/Gwydir_ITEM.tif', dataset=item)
# print("Gwydir_ITEM.tif")

###############################

# Leichhardt 2010
x = (800000, 900000)
y = (-2000000, -1900000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Leichhardt_wofs_2010.tif', dataset=wofs_ann)
print("Leichhardt_wofs_2010.tif")

# Leichhardt 2015
x = (800000, 900000)
y = (-2000000, -1900000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Leichhardt_wofs_2015.tif', dataset=wofs_ann)
print("Leichhardt_wofs_2015.tif")

# Leichhardt ITEMs
item = dc.load(product="item_v2", measurements=["relative"], 
                     time=time, **query)
item = item.squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Leichhardt_ITEM.tif', dataset=item)
print("Leichhardt_ITEM.tif")

###############################

# Kakadu 2010
x = (0, 100000)
y = (-1350000, -1250000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Kakadu_wofs_2010.tif', dataset=wofs_ann)
print("Kakadu_wofs_2010.tif")

# Kakadu 2015
x = (0, 100000)
y = (-1350000, -1250000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Kakadu_wofs_2015.tif', dataset=wofs_ann)
print("Kakadu_wofs_2015.tif")

# Kakadu ITEMs
item = dc.load(product="item_v2", measurements=["relative"], 
                     time=time, **query)
item = item.squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Kakadu_ITEM.tif', dataset=item)
print("Kakadu_ITEM.tif")

###############################

# Hobart 2010
x = (1200000, 1300000)
y = (-4800000, -4700000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})


wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Hobart_wofs_2010.tif', dataset=wofs_ann)
print("Hobart_wofs_2010.tif")

# Hobart 2015
x = (1200000, 1300000)
y = (-4800000, -4700000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Hobart_wofs_2015.tif', dataset=wofs_ann)
print("Hobart_wofs_2015.tif")

# Hobart ITEMs
item = dc.load(product="item_v2", measurements=["relative"], 
                     time=time, **query)
item = item.squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Hobart_ITEM.tif', dataset=item)
print("Hobart_ITEM.tif")

###############################

# Perth 2010
x = (-1550000, -1450000)
y = (-3650000, -3550000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Perth_wofs_2010.tif', dataset=wofs_ann)
print("Perth_wofs_2010.tif")

# Perth 2015
x = (-1550000, -1450000)
y = (-3650000, -3550000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Perth_wofs_2015.tif', dataset=wofs_ann)
print("Perth_wofs_2015.tif")

# Perth ITEMs
item = dc.load(product="item_v2", measurements=["relative"], 
                     time=time, **query)
item = item.squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Perth_ITEM.tif', dataset=item)
print("Perth_ITEM.tif")

###############################

# Murray_Valley 2010
x = (1100000, 1200000)
y = (-4000000, -3900000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Murray_Valley_wofs_2010.tif', dataset=wofs_ann)
print("Murray_Valley_wofs_2010.tif")

# Murray_Valley 2015
x = (1100000, 1200000)
y = (-4000000, -3900000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Murray_Valley_wofs_2015.tif', dataset=wofs_ann)
print("Murray_Valley_wofs_2015.tif")

# # Murray_Valley ITEMs
# item = dc.load(product="item_v2", measurements=["relative"], 
#                      time=time, **query)
# item = item.squeeze().drop('time')
# write_geotiff(filename='aquatic_outs/Murray_Valley_ITEM.tif', dataset=item)
# print("Murray_Valley_ITEM.tif")

###############################

# Adelaide 2010
x = (550000, 650000)
y = (-3850000, -3750000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Adelaide_wofs_2010.tif', dataset=wofs_ann)
print("Adelaide_wofs_2010.tif")

# Adelaide 2015
x = (550000, 650000)
y = (-3850000, -3750000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Adelaide_wofs_2015.tif', dataset=wofs_ann)
print("Adelaide_wofs_2015.tif")

# Adelaide ITEMs
item = dc.load(product="item_v2", measurements=["relative"], 
                     time=time, **query)
item = item.squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Adelaide_ITEM.tif', dataset=item)
print("Adelaide_ITEM.tif")

###############################

# Lake_Eyre 2010
x = (500000, 600000)
y = (-3000000, -2900000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Lake_Eyre_wofs_2010.tif', dataset=wofs_ann)
print("Lake_Eyre_wofs_2010.tif")

# Lake_Eyre 2015
x = (500000, 600000)
y = (-3000000, -2900000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Lake_Eyre_wofs_2015.tif', dataset=wofs_ann)
print("Lake_Eyre_wofs_2015.tif")

# # Lake_Eyre ITEMs
# item = dc.load(product="item_v2", measurements=["relative"], 
#                      time=time, **query)
# item = item.squeeze().drop('time')
# write_geotiff(filename='aquatic_outs/Lake_Eyre_ITEM.tif', dataset=item)
# print("Lake_Eyre_ITEM.tif")

###############################

# Blue_Mtns 2010
x = (1600000, 1700000)
y = (-3900000, -3800000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Blue_Mtns_wofs_2010.tif', dataset=wofs_ann)
print("Blue_Mtns_wofs_2010.tif")

# Blue_Mtns 2015
x = (1600000, 1700000)
y = (-3900000, -3800000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Blue_Mtns_wofs_2015.tif', dataset=wofs_ann)
print("Blue_Mtns_wofs_2015.tif")

# # Blue_Mtns ITEMs
# item = dc.load(product="item_v2", measurements=["relative"], 
#                      time=time, **query)
# item = item.squeeze().drop('time')
# write_geotiff(filename='aquatic_outs/Blue_Mtns_ITEM.tif', dataset=item)
# print("Blue_Mtns_ITEM.tif")

###############################

# Aust_Alps 2010
x = (1400000, 1500000)
y = (-4100000, -4000000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Aust_Alps_wofs_2010.tif', dataset=wofs_ann)
print("Aust_Alps_wofs_2010.tif")

# Aust_Alps 2015
x = (1400000, 1500000)
y = (-4100000, -4000000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Aust_Alps_wofs_2015.tif', dataset=wofs_ann)
print("Aust_Alps_wofs_2015.tif")

# # Aust_Alps ITEMs
# item = dc.load(product="item_v2", measurements=["relative"], 
#                      time=time, **query)
# item = item.squeeze().drop('time')
# write_geotiff(filename='aquatic_outs/Aust_Alps_ITEM.tif', dataset=item)
# print("Aust_Alps_ITEM.tif")

###############################

# Collier_Range 2010
x = (-1300000, -1200000)
y = (-2700000, -2600000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Collier_Range_wofs_2010.tif', dataset=wofs_ann)
print("Collier_Range_wofs_2010.tif")

# Collier_Range 2015
x = (-1300000, -1200000)
y = (-2700000, -2600000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Collier_Range_wofs_2015.tif', dataset=wofs_ann)
print("Collier_Range_wofs_2015.tif")

# # Collier_Range ITEMs
# item = dc.load(product="item_v2", measurements=["relative"], 
#                      time=time, **query)
# item = item.squeeze().drop('time')
# write_geotiff(filename='aquatic_outs/Collier_Range_ITEM.tif', dataset=item)
# print("Collier_Range_ITEM.tif")

###############################

# Coorong 2010
x = (600000, 700000)
y = (-3950000, -3850000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Coorong_wofs_2010.tif', dataset=wofs_ann)
print("Coorong_wofs_2010.tif")

# Coorong 2015
x = (600000, 700000)
y = (-3950000, -3850000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Coorong_wofs_2015.tif', dataset=wofs_ann)
print("Coorong_wofs_2015.tif")

# Coorong ITEMs
item = dc.load(product="item_v2", measurements=["relative"], 
                     time=time, **query)
item = item.squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Coorong_ITEM.tif', dataset=item)
print("Coorong_ITEM.tif")

###############################

# Brisbane 2010
x = (2000000, 2100000)
y = (-3200000, -3100000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Brisbane_wofs_2010.tif', dataset=wofs_ann)
print("Brisbane_wofs_2010.tif")

# Brisbane 2015
x = (2000000, 2100000)
y = (-3200000, -3100000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Brisbane_wofs_2015.tif', dataset=wofs_ann)
print("Brisbane_wofs_2015.tif")

# Brisbane ITEMs
item = dc.load(product="item_v2", measurements=["relative"], 
                     time=time, **query)
item = item.squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Brisbane_ITEM.tif', dataset=item)
print("Brisbane_ITEM.tif")

###############################

# Mt_Ney 2010
x = (-1000000, -900000)
y = (-3650000, -3550000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2010-01-01", "2010-12-31")
sensor = 'ls5'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Mt_Ney_wofs_2010.tif', dataset=wofs_ann)
print("Mt_Ney_wofs_2010.tif")

# Mt_Ney 2015
x = (-1000000, -900000)
y = (-3650000, -3550000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls7'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"], 
                     time=time, **query)
wofs_ann = masking.mask_invalid_data(wofs_ann).squeeze().drop('time')
write_geotiff(filename='aquatic_outs/Mt_Ney_wofs_2015.tif', dataset=wofs_ann)
print("Mt_Ney_wofs_2015.tif")

# # Mt_Ney ITEMs
# item = dc.load(product="item_v2", measurements=["relative"],
#                      time=time, **query)
# item = item.squeeze().drop('time')
# write_geotiff(filename='aquatic_outs/Mt_Ney_ITEM.tif', dataset=item)
# print("Mt_Ney_ITEM.tif")
