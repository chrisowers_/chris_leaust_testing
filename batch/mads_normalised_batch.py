import os
import xarray
import numpy as np
import datacube
from datacube.storage import masking
from datacube.helpers import write_geotiff
from datacube.utils.geometry import CRS
dc = datacube.Datacube(env="lccs_dev", app="fc")

os.makedirs('./mads_normalised_outs', exist_ok=True)

# Ayr 2015
x = (1500000, 1600000)
y = (-2200000, -2100000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Ayr_mads_normalised.tif', dataset=log_mads)
print("Ayr_mads_normalised.tif")

###############################

# Diamentina 2015
x = (800000, 900000)
y = (-2800000, -2700000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Diamentina_mads_normalised.tif', dataset=log_mads)
print("Diamentina_mads_normalised.tif")

###############################

# Gwydir 2015
x = (1600000, 1700000)
y = (-3400000, -3300000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Gwydir_mads_normalised.tif', dataset=log_mads)
print("Gwydir_mads_normalised.tif")

###############################

# Leichhardt 2015
x = (800000, 900000)
y = (-2000000, -1900000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Leichhardt_mads_normalised.tif', dataset=log_mads)
print("Leichhardt_mads_normalised.tif")

###############################

# Kakadu 2015
x = (0, 100000)
y = (-1350000, -1250000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Kakadu_mads_normalised.tif', dataset=log_mads)
print("Kakadu_mads_normalised.tif")

###############################

# Hobart 2015
x = (1200000, 1300000)
y = (-4800000, -4700000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Hobart_mads_normalised.tif', dataset=log_mads)
print("Hobart_mads_normalised.tif")

###############################

# Perth 2015
x = (-1550000, -1450000)
y = (-3650000, -3550000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Perth_mads_normalised.tif', dataset=log_mads)
print("Perth_mads_normalised.tif")

###############################

# Murray Valley 2015
x = (1100000, 1200000)
y = (-4000000, -3900000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Murray_Valley_mads_normalised.tif', dataset=log_mads)
print("Murray_Valley_mads_normalised.tif")

###############################

# Adelaide 2015
x = (550000, 650000)
y = (-3850000, -3750000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Adelaide_mads_normalised.tif', dataset=log_mads)
print("Adelaide_mads_normalised.tif")

###############################

# Lake_Eyre 2015
x = (500000, 600000)
y = (-3000000, -2900000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Lake_Eyre_mads_normalised.tif', dataset=log_mads)
print("Lake_Eyre_mads_normalised.tif")

###############################

# Blue_Mtns 2015
x = (1600000, 1700000)
y = (-3900000, -3800000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Blue_Mtns_mads_normalised.tif', dataset=log_mads)
print("Blue_Mtns_mads_normalised.tif")

###############################

# Aust_Alps 2015
x = (1400000, 1500000)
y = (-4100000, -4000000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Aust_Alps_mads_normalised.tif', dataset=log_mads)
print("Aust_Alps_mads_normalised.tif")

###############################

# Collier_Range 2015
x = (-1300000, -1200000)
y = (-2700000, -2600000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Collier_Range_mads_normalised.tif', dataset=log_mads)
print("Collier_Range_mads_normalised.tif")

###############################

# Coorong 2015
x = (600000, 700000)
y = (-3950000, -3850000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Coorong_mads_normalised.tif', dataset=log_mads)
print("Coorong_mads_normalised.tif")

###############################

# Brisbane 2015
x = (2000000, 2100000)
y = (-3200000, -3100000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Brisbane_mads_normalised.tif', dataset=log_mads)
print("Brisbane_mads_normalised.tif")

###############################

# Dundas 2015
x = (-1000000, -900000)
y = (-3650000, -3550000)

res = (-25, 25)
crs = "EPSG:3577"
time = ("2015-01-01", "2015-12-31")
sensor = 'ls8'
query =({'x':x,
        'y':y,
        'crs':crs,
        'resolution':res})

mads = dc.load(product=sensor +"_nbart_tmad_annual", measurements=["edev", "sdev", "bcdev"],
               time=time, **query)

mads = masking.mask_invalid_data(mads).squeeze().drop('time')

edev=(mads["edev"])
sdev=(mads["sdev"])
bcdev=(mads["bcdev"])
log_edev = np.log(1/edev)
log_sdev = np.log(1/sdev)
log_bcdev = np.log(1/bcdev)
log_mads = xarray.merge([log_edev, log_sdev, log_bcdev])

log_mads.attrs['crs'] = CRS(mads.crs)
write_geotiff(filename='mads_normalised_outs/Dundas_mads_normalised.tif', dataset=log_mads)
print("Dundas_mads_normalised.tif")
