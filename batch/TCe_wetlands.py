import sys, os
import datacube
from datacube.helpers import write_geotiff
from datacube.utils.geometry import CRS
dc = datacube.Datacube(app="tasseled cap")
sys.path.append('../../dea-notebooks/10_Scripts')
import DEADataHandling
import TasseledCapTools


os.makedirs('./wetlands', exist_ok=True)

res = (-25, 25)
time = ('2015-01-01', '2015-12-31')
crs = 'EPSG:3577'

Ayr = {'AOI': 'Ayr', 'query': {'x':(1500000, 1600000), 'y':(-2200000, -2100000), 'resolution': res, 'time': time, 'crs': crs}}

Diamantina = {'AOI': 'Diamantina', 'query': {'x':(800000, 900000), 'y':(-2800000, -2700000), 'resolution': res, 'time': time, 'crs': crs}}

Gwydir = {'AOI': 'Gwydir', 'query': {'x':(1600000, 1700000), 'y':(-3400000, -3300000), 'resolution': res, 'time': time, 'crs': crs}}

Leichhardt = {'AOI': 'Leichhardt', 'query': {'x':(800000, 900000), 'y':(-2000000, -1900000), 'resolution': res, 'time': time, 'crs': crs}}

Kakadu = {'AOI': 'Kakadu', 'query': {'x':(0, 100000), 'y':(-1350000, -1250000), 'resolution': res, 'time': time, 'crs': crs}}

Hobart = {'AOI': 'Hobart', 'query': {'x':(1200000, 1300000), 'y':(-4800000, -4700000), 'resolution': res, 'time': time, 'crs': crs}}

Perth = {'AOI': 'Perth', 'query': {'x':(-1550000, -1450000), 'y':(-3650000, -3550000), 'resolution': res, 'time': time, 'crs': crs}}

Murray_Valley = {'AOI': 'Murray_Valley', 'query': {'x':(1100000, 1200000), 'y':(-4000000, -3900000), 'resolution': res, 'time': time, 'crs': crs}}

Adelaide = {'AOI': 'Adelaide', 'query': {'x':(550000, 650000), 'y':(-3850000, -3750000), 'resolution': res, 'time': time, 'crs': crs}}

Lake_Eyre = {'AOI': 'Lake_Eyre', 'query': {'x':(500000, 600000), 'y':(-3000000, -2900000), 'resolution': res, 'time': time, 'crs': crs}}

Blue_Mtns = {'AOI': 'Blue_Mtns', 'query': {'x':(1600000, 1700000), 'y':(-3900000, -3800000), 'resolution': res, 'time': time, 'crs': crs}}

Aust_Alps = {'AOI': 'Aust_Alps', 'query': {'x':(1400000, 1500000), 'y':(-4100000, -4000000), 'resolution': res, 'time': time, 'crs': crs}}

Collier_Range = {'AOI': 'Collier_Range', 'query': {'x':(-1300000, -1200000), 'y':(-2700000, -2600000), 'resolution': res, 'time': time, 'crs': crs}}

Coorong = {'AOI': 'Coorong', 'query': {'x':(600000, 700000), 'y':(-3950000, -3850000), 'resolution': res, 'time': time, 'crs': crs}}

Brisbane = {'AOI': 'Brisbane', 'query': {'x':(2000000, 2100000), 'y':(-3200000, -3100000), 'resolution': res, 'time': time, 'crs': crs}}

Mt_Ney = {'AOI': 'Mt_Ney', 'query': {'x':(-1000000, -900000), 'y':(-3650000, -3550000), 'resolution': res, 'time': time, 'crs': crs}}

# sites = [Ayr, Diamantina, Gwydir, Leichhardt, Kakadu, Hobart, Perth, Murray_Valley, Adelaide, Lake_Eyre, Blue_Mtns, Aust_Alps, Collier_Range, Coorong, Brisbane, Mt_Ney]

sites = [Kakadu]


for site in sites:
    ds = DEADataHandling.load_clearlandsat(dc=dc, query=site['query'], sensors=['ls5','ls7','ls8'], 
                                           masked_prop=0.75, mask_pixel_quality=False, 
                                           mask_invalid_data=True)
    TCe = TasseledCapTools.pct_exceedance_tasseled_cap(ds, tc_bands=['greenness', 'brightness', 'wetness'], 
                                                       greenness_threshold=700,brightness_threshold=4000, 
                                                       wetness_threshold=-600, drop=True, drop_tc_bands=True)
    
    TCGe = ds['greenness_pct_exceedance']
    TCBe = ds['brightness_pct_exceedance']
    TCWe = ds['wetness_pct_exceedance']
    wetlands = TCGe.where(TCWe > 0.70)
    wetlands = wetlands.to_dataset(name='array')
    wetlands.attrs['crs'] = CRS('EPSG:3577')
    write_geotiff(filename='wetlands/'+str(site['AOI'])+'_TCe_wetlands_2015.tif', dataset=wetlands)
    print(site['AOI'])

